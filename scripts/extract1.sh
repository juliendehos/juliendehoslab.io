#!/bin/sh

if [ "$#" -ne 2 ] ; then
    echo "usage: $0 time file" >&2
    echo "example: $0 01:42 foobar.mp4" >&2
    exit 1
fi

TIME=$1
INPUT=$2
OUTPUT="${INPUT%.*}.png"

if [ -f "$OUTPUT" ] ; then
    read -p "$OUTPUT already exists. Overwrite (y/n) ? " CONFIRM
    if [ "$CONFIRM" != "y" ] ; then
        echo "$OUTPUT skipped"
        exit 1
    fi
fi

ffmpeg -ss "$TIME" -i "$INPUT" -vframes 1 "$OUTPUT" && echo "$OUTPUT written"

# ffmpeg -ss 00:01 -i in.mp4 -vframes 1 out.png

