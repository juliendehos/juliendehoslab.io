let
  #url = "https://github.com/NixOS/nixpkgs/archive/5272327.tar.gz"; # 20.03
  #url = "https://github.com/NixOS/nixpkgs/archive/4d2b37a.tar.gz"; # 22.11
  url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/24.05.tar.gz";
  pkgs = import (fetchTarball url) {};
in pkgs.callPackage ./default.nix {}

