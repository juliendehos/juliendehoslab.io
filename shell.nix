let

  #config = {
  #  allowBroken = true;
  #  packageOverrides = pkgz: rec {
  #    haskellPackages = pkgz.haskellPackages.override {
  #      overrides = hpNew: hpOld: rec {
  #        hakyll = hpOld.hakyll.overrideAttrs(old: {
  #          #configureFlags = "-f watchServer -f previewServer";
  #          preConfigure = "sed -i \"s/warp            >= 3.2   && < 3.3/warp/\" hakyll.cabal";
  #        });
  #      };
  #    };
  #  };
  #};

  #url = "https://github.com/NixOS/nixpkgs/archive/5272327.tar.gz"; # 20.03
  #pkgs = import (fetchTarball url) { inherit config; };
  #pkgs = import <nixpkgs> { inherit config; };
  #pkgs = import <nixpkgs> { inherit config; };

  pkgs = import <nixpkgs> {};

  #url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.11.tar.gz";
  #pkgs = import (fetchTarball url) {};

in import ./default.nix { inherit pkgs; }

