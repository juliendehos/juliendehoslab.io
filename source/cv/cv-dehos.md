---
title: CV Julien Dehos
---

# Contacts

- e-mail : `dehos@univ-littoral.fr`
- site-web : <https://juliendehos.gitlab.io>
- ORCID : <https://orcid.org/0000-0002-4049-2551>


# État civil

- nationalité : française
- année de naissance : 1983


# Formation

- 2010 : Doctorat Informatique, Université du Littoral Côte d’Opale
- 2007 : Master Sciences et Technologies, Université Bordeaux 1
- 2007 : Ingénieur Informatique, ENSEIRB
- 2004 : DUT Informatique, IUT Clermont 1


# Expérience professionnelle

- depuis 2011 : maître de conférence, Université du Littoral Côte d’Opale
- 2019-2020 : formateur occasionnel, Epitech Lille
- 2010-2011 : attaché temporaire d’enseignement et de recherche, Université Clermont 1
- 2007-2010 : allocataire de recherche en informatique graphique, ULCO
- 2007-2009 : enseignant vacataire, Université Clermont 1
- 2007 : stagiaire, R&D en informatique graphique, Université Clermont 1
- 2006 : stagiaire, développement informatique (jeux vidéo), Chronophage Games
- 2005 : stagiaire, développement web, CDOS du Finistère
- 2004 : stagiaire, développement informatique (topologie digitale), Université Clermont 1
- 2003 : stagiaire, développement web, brgm


# Publications scientifiques

## Journaux

#. Tristan Cazenave, Yen-Chi Chen, Guan-Wei Chen, Shi-Yu Chen, Xian-Dong Chiu, et al., *Polygames: Improved Zero Learning*, International Computer Games Association Journal (ICGA), vol. 42, num. 4, pp. 244-256, 2020
#. Amaury Dubois, Julien Dehos and Fabien Teytaud., *Upper Confidence Tree for planning restart strategies in Multi-Modal Optimization*, Soft Computing, 2020
#. Hervé Lobbes, Julien Dehos, Bruno Pereira, Guillaume Le Guenno, Laurent Sarry et al., *Computed and Subjective Blue Scleral Color Analysis as a Diagnostic Tool for Iron Deficiency: A Pilot Study*, Journal of Clinical Medicine, vol. 8, num. 11, pp. 1876, 2019
#. Fabien Teytaud and Julien Dehos, *On the tactical and strategic behaviour of MCTS when biasing random simulations*, ICGA Journal, vol. 38, num. 2, pp. 67-80, 2015
#. Julien Dehos, Éric Zeghers, Laurent Sarry, François Rousselle and Christophe Renaud, *Immersive front-projection analysis using a radiosity-based simulation method*, Virtual Reality (VIRE), vol.18, num. 2, pp. 1117-128, 2014
#. Julien Dehos, Éric Zeghers, Laurent Sarry, François Rousselle and Christophe Renaud, *Pratical photoquantity measurement using a camera*, IET Image Processing (IET-IPR), vol. 6, num. 4, pp.301-443, 2012
#. Christophe Lohou and Julien Dehos, *Automatic correction of Ma and Sonka’s thinning algorithm using P-simple points*, IEEE Transactions on Pattern Analysis and Machine Intelligence (TPAMI),vol. 32, num. 6, pp. 1148-1152, 2010
#. Christophe Lohou and Julien Dehos, *An automatic correction of Ma’s thinning algorithm based on P-simple points*, Journal of Mathematical Imaging and Vision (JMIV), vol. 36, num. 1, pp. 54-62,2010


## Conférences

#. Amaury Dubois, Julien Dehos, Fabien Teytaud, *Improving Multi-Modal Optimization Restart Strategy Through Multi-Armed Bandit*, 17thIEEE International Conference On Machine Learning and Applications, (ICMLA 2018)
#. Florian Leprêtre, Fabien Teytaud, Julien Dehos, *Multi-armed bandit for stratified sampling : Application to numerical integration*, The 22nd Conference on Technologies and Applications of Artificial Intelligence, (TAAI 2017)
#. Joris Duguépéroux, Ahmad Mazyad, Fabien Teytaud, Julien Dehos, *Pruning playouts in Monte-Carlo Tree Search for the game of Havannah*, The 9th International Conference on Computers and Games (CG2016)
#. Julien Dehos, Éric Zeghers, Christophe Renaud, François Rousselle and Laurent Sarry, *Radiometric compensation for a low-cost immersive projection system*, Proceedings of the 2008 ACM Symposiumon Virtual Reality Software and Technology (VRST)


## Chapitres de livres

#. André Bigand, Julien Dehos, Christophe Renaud and Joseph Constantin, *Image Quality Assessmentof Computer-generated Images, Based on Machine Learning and Soft Computing* (Chapter 2 and Chapter 3), ISBN : 9783319735436, Springer International Publishing, 2018


## Journaux francophones

#. Julien Dehos, Pierre-Alexandre Hébert and Christophe Renaud, *Image multi-scène par intégra-tion d’un autostéréogramme dans une scène 3D*, Revue Electronique Francophone d’Informatique Graphique (REFIG), vol. 8, num. 2, pp. 67–78, 2014
#. Julien Dehos, Éric Zéghers, Laurent Sarry, François Rousselle and Christophe Renaud, *Analyse ensimulation de projection immersive par l’avant*, Revue Electronique Francophone d’Informatique Graphique (REFIG), vol. 5, num. 2, pp. 15–26, 2011
#. Julien Dehos, Éric Zeghers, François Rousselle, Christophe Renaud and Laurent Sarry, *Compensation radiométrique d’un système de projection immersive grand-public*, Revue Electronique Francophone d’Informatique Graphique (REFIG), vol. 3, num. 3, pp. 11-21, 2009


## Conférences francophones

#. Julien Dehos, Éric Zeghers, Laurent Sarry, François Rousselle and Christophe Renaud, *Analyse ensimulation de projection immersive par l’avant*, Journées de l’Association Française d’Informatique Graphique 2010 (AFIG), second best paper award
#. Julien Dehos, Éric Zeghers, François Rousselle, Christophe Renaud and Laurent Sarry, *Compensation radiométrique d’un système de projection immersive grand-public*, Journées de l’Association Française d’Informatique Graphique 2008 (AFIG)


## Communications

#. Facebook AI, *Open-sourcing Polygames, a new framework for training AI bots through self-play*, 2020
#. Julien Dehos, *Calculer des images de synthèse*, 3e Conférence du Pôle STS, June 7, 2016, Université du Littoral Côte d’Opale
#. Julien Dehos, Pierre-Alexandre Hébert and Christophe Renaud, *Multi-scene-images using single-image-stereograms*, Groupe de Travail Rendu du GDR IG, June 12, 2014, Telecom ParisTech
#. Julien Dehos, *Introduction au calcul parallèle avec OpenCL*, Séminaires du LISIC, January 5, 2017, Université du Littoral Côte d’Opale
#. Julien Dehos, *Intérêt pédagogique et pluridisciplinaire du lancer de rayon pour l’enseignement de l’informatique*, Journées de l’Association Française d’Informatique Graphique 2011 (AFIG)
#. Julien Dehos, *Etude radiométrique d’un système de projection immersive grand-public pour desapplications de réalité mixte*, PhD thesis, December 2, 2010, Université du Littoral Côte d’Opale
#. Julien Dehos, Éric Zeghers, Laurent Sarry, François Rousselle and Christophe Renaud, *Analyse ensimulation des réflexions multiples lors d’une projection immersive*, Groupe de Travail Rendu et Visualisation, October 8, 2010, Telecom ParisTech
#. Julien Dehos, *Étude radiométrique d’un système de projection immersive grand-public pour desapplications de réalités mélangées*, Nouveau Chapitre de la Thèse, Association Bernard Grégory, 2010
#. Julien Dehos, Pierre Pontier, Benjamin Astre, Frédéric Brachfogel, Marc Kervarec, François Rous-selle, Benjamin Albouy, Éric Zeghers, Christophe Renaud and Laurent Sarry, *Catopsys Project : aversatile low-cost immersive projection system for VR applications*, Laval Virtual Revolution 2009
#. Julien Dehos, *Vers la réalité virtuelle réaliste*, Doctoriales franco-belges, 2009
#. Julien Dehos, Éric Zeghers, François Rousselle and Christophe Renaud, *Compensation radiomé-trique d’un système de projection immersive, Groupe de Travail Rendu et Visualisation*, September 5, 2008, Université Marne-la-Vallée
#. Julien Dehos, *Lancer de rayons et acquisition radiométrique spécifiques au système Catopsys*, Master thesis, 2007, Université Bordeaux 1


# Autres activités scientifiques

- relectures d’article :

    - Computer Vision and Image Understanding
    - Journal of Virtual Reality and Broadcasting
    - Revue Électronique Francophone d’Informatique Graphique
    - IET Image Processing
    - The Visual Computer

- jury du meilleur papier des Journées de l’AFIG (2011-2017)


# Enseignements

- 2020/2021 à 2022/2023 : Prog. fonctionnelle avancée, M1 Info, ULCO
- 2020/2021 à 2022/2023 : Prog. fonctionnelle, L3 Info, ULCO
- 2020/2021 à 2022/2023 : Génie Logiciel 1 & 2, L3 Info, ULCO
- 2017/2018 à 2020/2021 : Prog. fonctionnelle pour le web, M2 Info, ULCO
- 2016/2017 à 2019/2020 : Génie logiciel, L3 Info, ULCO
- 2015/2016 à 2019/2020 : Programmation fonctionnelle, M1 Info, ULCO
- 2014/2015 à 2019/2020 : Initiation au HPC, M1/M2 Info, ULCO
- 2011/2012 à 2019/2020 : C++, L3 Info, ULCO
- 2014/2015 à 2016/2017 : Projets informatiques, L3 Info, ULCO
- 2013/2014 à 2015/2016 : Réalité virtuelle, ING3 EILCO, ULCO
- 2011/2012 à 2014/2015 : Initiation à l’algorithmique 2, L1 MSPI, ULCO
- 2014/2015 : Initiation à l’algorithmique 1, L1 MSPI, ULCO
- 2013/2014 : Mise à niveau Linux, M1 Info Apprentissage, ULCO
- 2012/2013 : Réseaux, L3 Info, ULCO
- 2011/2012 à 2012/2013 : Formats de fichier 3D, M2 IGC, ULCO
- 2011/2012 à 2012/2013 : Rendu temps-réel, M2 IGC, ULCO
- 2011/2012 à 2012/2013 : Système d’exploitation, L2 Info, ULCO
- 2011/2012 : Bases de données, L3 Info, ULCO
- 2011/2012 : Informatique graphique, L3 Info, ULCO
- 2011/2012 : Programmation C++, M1 IGC, ULCO
- 2010/2011 : OpenGL, DUT2 Info, Univ. Clermont 1
- 2009/2010 à 2010/2011 : Développement Web, DUT2 Info, Univ. Clermont 1
- 2009/2010 à 2010/2011 : Traitement d’images, DUT2 Info, Univ. Clermont 1
- 2009/2010 à 2010/2011 : Synthèse d’images, DUT2 Info, Univ. Clermont 1
- 2007/2008 à 2010/2011 : Génie logiciel, L3 Pro Info, Univ. Clermont 1
- 2007/2008 à 2009/2010 : Rendu par photon-mapping, L3 Pro Info, Univ. Clermont 1
- 2007/2008 à 2009/2010 : Initiation au calcul scientifique, L3 Pro Info, Univ. Clermont 1
- 2007/2008 à 2009/2010 : Initiation aux probabilités, L3 Pro Info, Univ. Clermont 1


# Responsabilités administratives

- 2012/2013 à 2013/2014 : responsable des stages et projets L3/M1/M2, dpt-info, ULCO


# Contributions en informatique

## Projets open-source

- projets personnels sur gitlab: <https://gitlab.com/users/juliendehos/projects>
- contributions à des projets github: <https://github.com/juliendehos>

## Livres

- *La programmation fonctionnelle - Introduction et application en Haskell à l’usage de l’étudiant et du développeur*, ISBN : 9782340028777, Ellipses, 2019

## Articles de vulgarisation

- *[Introduction aux combinateurs de parseurs monadiques ou comment écrire des compilateurs en Haskell](https://juliendehos.developpez.com/tutoriels/haskell/comment-ecrire-compilateurs-en-haskell/)*, Developpez.com, 2020.
- *[Tutoriel pour apprendre à coder un streamer vidéo basique en Haskell et à le déployer sur Heroku](https://juliendehos.developpez.com/tutoriel/haskell/coder-streamer-video-heroku/)*, Developpez.com, 2020.
- *[Apprendre comment implémenter une IA de jeu en Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-ia-jeu-en-Haskell)*, Developpez.com, 2019.
- *[Apprendre comment implémenter un serveur de blog avec le langage Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-serveur-blog)*, Developpez.com, 2019.

## Présentations

- *[2D animations in Haskell using gloss, lens and state](https://fosdem.org/2023/schedule/event/haskell_2d_animations/)*, FOSDEM, 2023
- *[A quick overview of the Haskell tooling](https://fosdem.org/2023/schedule/event/haskell_tooling_overview/)*, FOSDEM, 2023
- *[Coder un streamer video en 135 lignes de Haskell et en 1 week-end](https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19)*, Lambda Lille (remote), 2020
- *[An overview of the Nix ecosystem](https://juliendehos.gitlab.io/lillefp-2019-nix)*, LilleFP 14, 2019
- *[Isomorphic web apps in Haskell](https://juliendehos.gitlab.io/lillefp-2019-isomorphic)*, LilleFP 12, 2019
- *[L’écosystème Nix pour développer en Python, et au delà...](https://juliendehos.gitlab.io/pyconfr-2018-nix/)*, PyconFR, 2018


