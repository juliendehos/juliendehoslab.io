---
title: Julien Dehos' webpage
notoc: notoc
---

I am an assistant professor in computer science and this is my webpage.

## Affiliations & contact

- [Université du Littoral Côte d'Opale](http://www.univ-littoral.fr)
- E-mail: dehos at univ-littoral dot fr
- [CV](cv/cv-dehos.pdf)

[//]: # (TODO ORCID: [0000-0002-4049-2551](https://orcid.org/0000-0002-4049-2551))


## Teaching

- [Environnement de travail](posts/env/index.html)
- [Génie logiciel 1 (L3 Info)](posts/gl1/index.html)
- [Génie logiciel 2 (L3 Info)](posts/gl2/index.html)
- [Programmation fonctionnelle (L3 Info)](posts/pf/index.html)
- [Prog fonctionnelle avancée (M1 Info)](posts/pfa/index.html)


## Research

- [HAL](https://hal.archives-ouvertes.fr/search/index/q/*/authFullName_s/Julien+Dehos/sort/producedDate_tdate+desc/)


## Software development

- [GitLab](https://gitlab.com/users/juliendehos/projects)
- [GitHub](https://github.com/juliendehos)

