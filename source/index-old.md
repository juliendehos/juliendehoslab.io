---
title: Julien Dehos' webpage
notoc: notoc
---

I am an assistant professor in computer science and this is my webpage.

## Affiliations & contact

- [Université du Littoral Côte d'Opale](http://www.univ-littoral.fr)
- E-mail: dehos at univ-littoral dot fr
- [CV](cv/cv-dehos.pdf)

[//]: # (TODO ORCID: [0000-0002-4049-2551](https://orcid.org/0000-0002-4049-2551))


## Teaching

- [Environnement de travail](posts/env/index.html)
- [Génie logiciel 1 (L3 Info)](posts/gl1/index.html)
- [Génie logiciel 2 (L3 Info)](posts/gl2/index.html)
- [Programmation fonctionnelle (L3 Info)](posts/pf/index.html)
- [Prog fonctionnelle avancée (M1 Info)](posts/pfa/index.html)

## Research

Publications:

- [HAL](https://hal.archives-ouvertes.fr/search/index/q/*/authFullName_s/Julien+Dehos/sort/producedDate_tdate+desc/)

[//]: # (TODO [arXiv](https://arxiv.org/search/cs?searchtype=author&query=Dehos%2C+J))


Other contributions & communications:

- [Open-sourcing Polygames, a new framework for training AI bots through self-play](https://ai.facebook.com/blog/open-sourcing-polygames-a-new-framework-for-training-ai-bots-through-self-play/), 2020
- [Calculer des images de synthèse](https://gitlab.com/juliendehos/conf_sts_2016_se), 2016

## Software development

Repositories:

- [personal projects on gitlab](https://gitlab.com/users/juliendehos/projects)
- [contributions to github projects](https://github.com/juliendehos)

Communications:

- Coder un streamer video en 135 lignes de Haskell et en 1 week-end, [Lambda Lille (remote)](https://www.meetup.com/fr-FR/LambdaLille/events/269852942/), 2020 :
  [slides](https://juliendehos.gitlab.io/talk-2020-lambdalille-covideo19/index.pdf),
  [dépot de code](https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19),
  [video](https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19/-/raw/master/files/talk-2020-lambdalille-covideo19.mp4),
  [video du meetup](https://www.youtube.com/watch?v=6CO98XBsNiY).
- An overview of the Nix ecosystem, [LilleFP14](https://www.meetup.com/fr-FR/Lille-FP/events/260541114/), 2019 :
  [slides](https://juliendehos.gitlab.io/talk-2019-lillefp-nix),
  [dépot de code](https://gitlab.com/juliendehos/talk-2019-lillefp-nix).
- Isomorphic web apps in Haskell, [LilleFP12](https://www.meetup.com/fr-FR/Lille-FP/events/258682124/), 2019 :
  [slides](https://juliendehos.gitlab.io/talk-2019-lillefp-miso),
  [dépot de code](https://gitlab.com/juliendehos/talk-2019-lillefp-miso).
- L'écosystème Nix pour développer en Python, et au delà…
  [PyConFR 2018](https://www.pycon.fr/2018/) :
  [vidéo](https://www.youtube.com/watch?v=rmRy9QZLp-g),
  [slides](https://juliendehos.gitlab.io/talk-2018-pyconfr-nix/),
  [dépôt de code](https://gitlab.com/juliendehos/talk-2018-pyconfr-nix).

Publications:

- [Introduction aux combinateurs de parseurs monadiques ou comment écrire des compilateurs en Haskell](https://juliendehos.developpez.com/tutoriels/haskell/comment-ecrire-compilateurs-en-haskell/), Developpez.com, 2020.
- [Tutoriel pour apprendre à coder un streamer vidéo basique en Haskell et à le déployer sur Heroku](https://juliendehos.developpez.com/tutoriel/haskell/coder-streamer-video-heroku/), Developpez.com, 2020.
- [Apprendre comment implémenter une IA de jeu en Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-ia-jeu-en-Haskell),
  Developpez.com, 2019.
- [Apprendre comment implémenter un serveur de blog avec le langage Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-serveur-blog),
  Developpez.com, 2019.
- La programmation fonctionnelle - Introduction et application en
  Haskell à l'usage de l'étudiant et du développeur, Ellipses, 2019 :
  [page de l'éditeur](https://www.editions-ellipses.fr/programmation-fonctionnelle-introduction-applications-haskell-lusage-ltudiant-dveloppeur-p-13083.html),
  [dépôt de code](https://gitlab.com/juliendehos/codes-livre-haskell).

[![livre haskell](files/livre-haskell-jd-small.jpg)](files/livre-haskell-jd.jpg)

Selected projects:

- [super-manu-bros-19](https://juliendehos.gitlab.io/super-manu-bros-19/), A Super-Mario-like game
- [covideo19](https://gitlab.com/juliendehos/covideo19), Quick & dirty screencast program
- [autoquizer](https://gitlab.com/juliendehos/autoquizer), A simple web app for running quizzes + automatic scoring
- [pongvaders](https://gitlab.com/juliendehos/pongvaders), Pong and Spaceinvaders in a single game
- [haskell-invaders](https://gitlab.com/juliendehos/haskell-invaders), A space-invaders-like video game, in Haskell (using Gloss or Shine)
- [miso-bmxtricks](https://gitlab.com/juliendehos/miso-bmxtricks), A not so minimalistic example of Miso's isomorphic feature
- [miso-samegame](https://gitlab.com/juliendehos/miso-samegame), Samegame implemented with Haskell & Miso
- [herve](https://gitlab.com/juliendehos/herve), C++ virtual reality library
- [petanqulator](https://gitlab.com/juliendehos/petanqulator), PETANQue simULATOR

[//]: # (- [haskell-cheats](https://gitlab.com/juliendehos/haskell-cheats), Cheat sheet about the Haskell programming language and its ecosystem (WIP))

[//]: # (This may be the most platform independent comment)

