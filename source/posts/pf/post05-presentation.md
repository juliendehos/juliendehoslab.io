---
title: PF - Présentation du module
date: 2023-08-21
---

## Objectifs du module

- avoir un peu de culture générale sur la programmation fonctionnelle
- utiliser un langage fonctionnel (Haskell)
- programmer selon une approche fonctionnelle

## Pré-requis 

- notions d'algorithmique

## Organisation

- 3h de CM
- 24h de TP

## Évaluation

- session 1 : contrôle continu 
- session 2 : devoir sur feuille (aucun document autorisé)
- voir la section [Remarques sur le contrôle continu](../env/post40-cc.html)

## Travaux pratiques

- VM fournie (voir la section [Environnement de travail, Machine Virtuelle](../env/post10-vm.html))
- [dépôt gitlab](https://gitlab.dpt-info.univ-littoral.fr/julien.dehos/ulco-pf-etudiant) à forker (voir la section [Environnement de travail, GIT](../env/post20-git.html))
- [flash-cards](https://gitlab.com/juliendehos/juliendehos.gitlab.io/-/raw/master/source/posts/pf/files/PF.apkg?ref_type=heads&inline=false) au format [Anki](https://apps.ankiweb.net/)

