---
title: PF TP7/8 - Mini-projets
date: 2020-02-25
---

## Todocli

En utilisant le projet fourni `projets/todocli`, implémentez une application
permettant de gérer une "todolist". Votre application doit permettre de :

- lire la todolist dans un fichier donné en paramètre
- afficher toutes les tâches, les tâches à faire, les tâches faites
- ajouter et supprimer une tâche
- marquer une tâche comme "faite" ou "à faire"
- quitter en sauvegardant automatiquement la todolist dans le fichier

* * *

<video preload="metadata" controls>
<source src="files/todocli.mp4" type="video/mp4" />
![](files/todocli.png){width="50%"}

</video>


## Maze

En utilisant le projet fourni `projets/maze`, implémentez un jeu de labyrinthe
en mode texte. Indication : stockez les données du labyrinthe dans un Vector
(tableau à une dimension).

* * *

<video preload="metadata" controls>
<source src="files/maze.mp4" type="video/mp4" />
![](files/maze.png){width="50%"}

</video>


## Inventaire

On veut implémenter un programme de calcul d'inventaire : on a un fichier
indiquant des produits en stock et le programme doit calculer la quantité
totale de chaque produit.

Dans le projet `projets/inventaire`, terminez l'implémentation des modules
`Produit`, `Stock` et `Inventaire`, puis le programme principal. Testez
progressivement avec les tests unitaires fournis.

* * *

```text
[nix-shell]$ cabal run inventaire 
usage: <filename>

[nix-shell]$ cabal run inventaire -- data/stock1.txt 

*** stock ***
Orange Pur Jus 1l (6)
Chimay 0.75l (6)
Orange Pur Jus 1l (3)

*** inventaire ***
Chimay 0.75l (6)
Orange Pur Jus 1l (9)
```


## HS Paint

Implémentez un logiciel de dessin basique. Votre logiciel doit permettre de
dessiner à la souris en cliquant sur le bouton gauche et de nettoyer la zone de
dessin en cliquant sur le bouton droit.  S'il vous reste du temps, vous pouvez
ajouter un réglage de la brosse, avec aperçu (couleur et taille).
Utilisez le projet `projets/hspaint` fourni.

* * *

<video preload="metadata" controls>
<source src="files/hs-paint.mp4" type="video/mp4" />
![](files/hs-paint.png){width="50%"}

</video>


## Todoweb

Le projet `projets/todoweb` implémente un serveur web, pour gérer une
"todolist". Complétez le code fourni (Haskell et HTML/JavaScript) de façon à
pouvoir ajouter et supprimer des tâches.

* * *

<video preload="metadata" controls>
<source src="files/todoweb.mp4" type="video/mp4" />
![](files/todoweb.png){width="50%"}

</video>


## Hangman

En utilisant le projet fourni `projets/hangman`, implémentez un [jeu du
pendu](https://fr.wikipedia.org/wiki/Le_Pendu_%28jeu%29) avec une interface
texte. Votre jeu doit :

- sélectionner le mot à deviner aléatoirement parmi une liste prédéfinie,
- afficher le mot à deviner avec les lettres déjà trouvée,
- afficher les lettres déjà saisies,
- afficher l'état du pendu,
- dérouler une partie de jeu (saisir et jouer une lettre, détecter les fins de partie...)

* * *

<video preload="metadata" controls>
<source src="files/hangman.mp4" type="video/mp4" />
![](files/hangman.png){width="70%"}

</video>


* * *

Pour dessiner le pendu, vous pouvez utiliser le texte suivant :

```text
      _______     
     |/      |    
     |      (_)   
     |      \|/   
     |       |    
     |      / \   
 ____|___________ 
                  
```

* * *

Pour définir du texte multi-ligne en Haskell, vous pouvez utiliser le code suivant :


```haskell
main = putStrLn "+-------+\n\
                \| hello |\n\
                \+-------+"
```

* * *

Pour les mots à deviner, vous pouvez utiliser :

```text
function
functional programming
haskell
list
pattern matching
recursion
tuple
type system
```

* * *

Bonus : ajoutez un exécutable à votre projet et implémentez-y une interface graphique.

<video preload="metadata" controls>
<source src="files/hangman-gtk.mp4" type="video/mp4" />
![](files/hangman-gtk.png){width="70%"}

</video>

