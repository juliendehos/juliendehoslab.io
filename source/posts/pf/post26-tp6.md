---
title: PF TP6 - Fonctions d'ordres supérieurs
date: 2023-08-21
---


# Exercices

- Pour ces exercices, travaillez dans le dossier `ulco-pf-etudiant/tp6/exos/`.

## Forme curryfiée

- Écrivez une fonction `myRangeTuple1` qui prend un tuple `(x0, x1)` et
  retourne la liste des entiers entre `x0` et `x1`.

```text
*Main> myRangeTuple1 (0, 9)
[0,1,2,3,4,5,6,7,8,9]
```

- Écrivez une fonction `myRangeCurry1` équivalente mais qui prend deux
  paramètres au lieu d'un tuple.

```text
*Main> myRangeCurry1 0 9
[0,1,2,3,4,5,6,7,8,9]
```

* * *

- Écrivez une fonction `myRangeTuple2` qui un prend `x1` et retourne la liste
  des entiers jusqu'à `x1`. Pour cela, utilisez la fonction `myRangeTuple1`.

```text
*Main> myRangeTuple2 9
[0,1,2,3,4,5,6,7,8,9]
```

- Écrivez une fonction `myRangeCurry2` équivalente mais utilisant
  `myRangeCurry1`.

```text
*Main> myRangeCurry2 9
[0,1,2,3,4,5,6,7,8,9]
```

* * *

- Écrivez une fonction `myCurry` qui transforme une fonction non-curryfiée en
  fonction curryfiée.

- Testez sur `myRangeTuple1`.


## Évaluation partielle

- Quel est le type de la fonction `take` ?

- Écrivez une fonction `myTake2` qui retourne les deux premiers éléments d'une
  liste. Utilisez l'évaluation partielle.

```text
*Main> myTake2 [1..4]
[1,2]
```

* * *

- Quel est le type de `(!!)` ? Et celui de `flip (!!)` ?

- Écrivez une fonction `myGet2` qui retourne le troisième élément d'une liste
  (on considère que la liste a suffisamment d'éléments).

```text
*Main> myGet2 [1..4]
3
```

* * *

- Écrivez une expression `map` qui multiplie par 2 chaque entier de la liste
  `[1..4]`.

- Écrivez une expression `map` qui divise par 2 chaque entier de la liste
  `[1..4]`.


## Lambda

- En utilisant un `map` et une lambda, écrivez une expression qui multiplie 2
  par chaque entier de la liste `[1..4]`.

- En utilisant un `map` et une lambda, écrivez une expression qui divise par 2
  chaque entier de la liste `[1..4]`.

- En utilisant `map`, `zip` et une lambda, construisez la liste `["a-1", "b-2",
  ... , "z-26"]`.

- Idem mais en utilisant `zipWith` et une lambda.


## Composition

- Écrivez une fonction `plus42Positif` qui teste si un entier incrémenté de 42
  est positif, en utilisant une composition de fonctions.

```text
*Main> plus42Positif 2
True

*Main> plus42Positif (-84)
False
```

- Écrivez une fonction `mul2PlusUn` qui calcule $2x+1$, en utilisant une
  composition de fonctions.

```text
*Main> mul2PlusUn 10
21
```

* * *

- Écrivez une fonction `mul2MoinsUn` qui calcule $2x-1$, en utilisant une
  composition de fonctions.

```text
*Main> mul2MoinsUn 10
19
```

- Écrivez une fonction `applyTwice` qui applique deux fois une fonction. 

```text
*Main> applyTwice mul2PlusUn 10
43
```


# Mini-projets

Pour les exercices suivants, travaillez dans les dossiers correspondants et
utilisez la configuration de projet Cabal + Nix (voir l'explication dans la
section [environnement de travail](../env/post30-vscode.html)). 


## Parcours en spirale d'une matrice 

On veut parcourir une matrice en spirale, du coin en haut à gauche jusqu'au
centre et dans le sens horaire. Par exemple, avec la matrice 

$$\left[
\begin{matrix}
a & b & c & d \\
e & f & g & h \\
i & j & k & l \\
m & n & o & p \\
\end{matrix}
\right]$$

le parcours en spirale donne la liste `[a,b,c,d,h,l,p,o,n,m,i,e,f,g,k,j]`.

Ce genre de parcours est utilisé, par exemple, en synthèse d'images pour
commencer le calcul au centre de l'image (zone la plus intéressante) et ainsi
afficher cette zone plus rapidement.

* * *

Vous aurez bien sûr remarqué que le parcours en spirale peut être implémenté en
répétant les opérations suivantes :

- prendre la première ligne, ici `[a,b,c,d]`, matrice résultat : 

$$\left[
\begin{matrix}
e & f & g & h \\
i & j & k & l \\
m & n & o & p \\
\end{matrix}
\right]$$

- prendre la dernière colonne, ici `[h,l,p]`, matrice résultat : 

$$\left[
\begin{matrix}
e & f & g \\
i & j & k \\
m & n & o \\
\end{matrix}
\right]$$

* * *

- prendre la dernière ligne dans l'ordre inverse, ici `[o,n,m]`, matrice
  résultat : 

$$\left[
\begin{matrix}
e & f & g \\
i & j & k \\
\end{matrix}
\right]$$

- prendre la première colonne dans l'ordre inverse, ici `[i,e]`, matrice
  résultat : 

$$\left[
\begin{matrix}
f & g \\
j & k \\
\end{matrix}
\right]$$

- etc...

* * *

En Haskell, on peut représenter une matrice comme la liste de ses lignes,
chaque ligne étant la liste des colonnes correspondantes. Par exemple, notre
matrice d'exemple peut s'écrire :

```haskell
m = [['a','b','c','d'],
     ['e','f','g','h'],
     ['i','j','k','l'],
     ['m','n','o','p']]
```

Ainsi, on récupère une ligne très facilement, par exemple la première ligne de
`m` est `head m`.  Pour récupérer une colonne, il suffit de passer par la
matrice transposée.

* * *

Dans le projet `tp6/spiral`, écrivez un programme en Haskell qui calcule le
parcours en spirale d'une matrice.  Pour simplifier, vous pouvez écrire la
matrice en dur dans le code mais on doit pouvoir la remplacer par une autre.
Indication : vous pouvez utiliser les fonctions `transpose`, `splitAt` et
`cycle` du module `Data.List`. 


## Interface graphique avec Gi-Gtk

- Implémentez une interface graphique permettant de saisir une liste de tâches
  et de la vider. Pour cela, utilisez
[gi-gtk](https://hackage.haskell.org/package/gi-gtk-3.0.41) (faites attention
de lire la documentation à la bonne version) et travaillez dans le projet
`tp6/todolist` (un code d'exemple est fourni).

<video preload="metadata" controls>
<source src="files/2020-pf-todolist.mp4" type="video/mp4" />
![](files/2020-pf-todolist.png){width="50%"}

</video>


## IORef

- Écrivez un serveur web qui permet de lister des noms (url `/`) et d'en
  ajouter (url `/add/...`). Pour cela, utilisez
  [IORef](https://hackage.haskell.org/package/base/docs/Data-IORef.html),
  [liftIO](https://hackage.haskell.org/package/base/docs/Control-Monad-IO-Class.html)
  et travaillez dans le projet `tp6/webapp`.

<video preload="metadata" controls>
<source src="files/2020-pf-webapp.mp4" type="video/mp4" />
![](files/2020-pf-webapp.png){width="90%"}

</video>

