---
title: PF TP3 - Listes, tuples
date: 2020-02-25
---

# Listes

## Opérations de base

- Dans `ghci`, exécutez les expressions suivantes (1) :

```haskell
[1, 2, 3, 4]
[1..4]
1:2:3:4:[]
[0, 10 .. 100]
13 : [37, 42]
[13] ++ [37, 42]
length  [1, 2, 3, 4]
null  [1, 2, 3, 4]
['f', 'o', 'o', 'b', 'a', 'r']
```

* * *

- Dans `ghci`, exécutez les expressions suivantes (2) :

```haskell
head  [1, 2, 3, 4]
tail [1, 2, 3, 4]
last [1, 2, 3, 4]
init [1, 2, 3, 4]
take 2 [1, 2, 3, 4]
drop 2 [1, 2, 3, 4]
splitAt 2 [1, 2, 3, 4]
[1, 2, 3, 4] !! 1
```

* * *

- Dans `ghci`, exécutez les expressions suivantes (3) :

```haskell
reverse [42, 13, 37]
words "foo bar"
unwords ["foo","bar"]
concat ["foo","bar"]
lines "foo\nbar"
unlines ["foo","bar"]
12 `elem` [42, 13, 37]
13 `elem` [42, 13, 37]
```

* * *

- Dans `ghci`, exécutez les expressions suivantes (4) :

```haskell
sum [12, 21, 9]
product [1, 2, 3]
maximum [1, 2, 3]
all even [1, 2]
any even [1, 2]
replicate 3 42
take 10 (cycle [0, 1])
```

* * *

- Dans `ghci`, exécutez les expressions suivantes (5) :

```haskell
import Data.List
sort [37, 42, 13]
nub [1, 2, 1, 2, 3]
[1, 2, 3] \\ [2, 3]
intercalate " - " ["foo", "bar", "baz"]
[1, 2, 3] `union` [2, 3]
"foo" `isPrefixOf` "foobar"
"bar" `isPrefixOf` "foobar"
```

## Palindrome

- Écrivez une fonction qui teste si un texte est un palindrome.

```haskell
$ ghci palindrome.hs 

*Main> palindrome "radar"
True

*Main> palindrome "foobar"
False
```

## Répéter une liste

- Écrivez une fonction `twice` qui prend une liste et retourne cette liste répétée deux fois.

```haskell
$ ghci twice.hs 

*Main> twice [1, 2]
[1,2,1,2]

*Main> twice "to"
"toto"
```

* * *

- Écrivez une fonction `sym` qui prend une liste et retourne cette liste répétée à l'endroit et à l'envers.

```haskell
$ ghci twice.hs 

*Main> sym [1, 2]
[1,2,2,1]

*Main> sym "to"
"toot"
```

## Pattern matching de listes (1)

- Écrivez une fonction `fuzzyLength` qui retourne un message selon la taille de
  la liste :

```haskell
$ ghci fuzzyLength.hs 

*Main> fuzzyLength []
"empty"

*Main> fuzzyLength [1]
"one"

*Main> fuzzyLength [1,2]
"two"

*Main> fuzzyLength [1..4]
"many"
```

## Pattern matching de listes (2)

- Écrivez des fonctions `safeHead` et `safeTail`, qui retournent la tête et la
  queue d'une liste, dans un `Maybe`.

```haskell
$ ghci head-tail.hs 

*Main> safeHead ""
Nothing

*Main> safeHead "foobar"
Just 'f'

*Main> safeTail ""
Nothing

*Main> safeTail "foobar"
Just "oobar"
```


# Tuples

## Opérations de base

- Dans `ghci`, exécutez les expressions suivantes :

```haskell
splitAt 2 [1..10]
fst (1, "foobar")
snd (1, "foobar")
zip [1..] "foobar"
unzip [(1,'f'),(2,'o'),(3,'o')]
zip3 [1..] "foo" "bar"
zipWith (+) [1, 2, 1] [1, 2, 3]
```

## Pattern matching de tuples

- Écrivez une fonction `myFst` équivalente à `fst`.
- Écrivez une fonction `mySnd` équivalente à `snd`.
- Écrivez une fonction `myFst3` qui retourne le premier élément d'un triplet.

```haskell
$ ghci fst-snd.hs 

*Main> myFst ("foo", 1)
"foo"

*Main> mySnd ("foo", 1)
1

*Main> myFst3 ("foo", 1, 'a')
"foo"
```


# Récapitulatif

## Cowsay

- Écrivez un `cowsay` simplifié, où le message apparait sur une seule ligne.

```text
$ runghc cowsay.hs Hello World!
  ____________
< Hello World! >
  ------------
        \   ^__^ 
         \  (oo)\_______ 
            (__)\       )\/\ 
                ||----w | 
                ||     || 
```

## Statistiques d'un fichier texte

- Écrivez un programme qui lit un fichier dont le nom est passé en argument de
  ligne de commande et affiche son nombre de lignes, mots, etc. Indication :
  `readFile`.

```text
$ runghc wc.hs 
usage: <file>

$ runghc wc.hs wc.hs 
lines: 26
words: 108
chars: 757
non-whitespace chars: 559

$ wc wc.hs 
 26 108 757 wc.hs
```

## Calcul de norme

- Écrivez une fonction `dot` qui calcule le produit scalaire de deux listes de `Double`.

Rappel : $\mathbf{u} \cdot \mathbf{v} = \sum_i u_i v_i$

```haskell
$ ghci norm.hs 

*Main> dot [3, 4] [1, 2]
11.0

*Main> dot [3, 4] [3, 4]
25.0
```

* * *

- Écrivez une fonction `norm` qui calcule la norme d'une liste de `Double`.

Rappel : $||\mathbf{u}|| = \sqrt{\mathbf{u} \cdot \mathbf{u}}$

```haskell
$ ghci norm.hs 

*Main> norm [3, 4]
5.0
```

## Recherche dans une liste

- Écrivez une fonction `findIndPositive :: [Double] -> Maybe Int` qui retourne
  l'indice du premier nombre positif de la liste, s'il existe.  Indication :
  écrivez une fonction `findFstPositive :: [(Int, Double)] -> Maybe Int` et
  numérotez les nombres de la liste initiale. Bien-sûr, vous n'avez pas le
  droit à la fonction `findIndex`.

```haskell
$ ghci recherche-liste.hs 

*Main> findIndPositive [-1.3, 3.7, 4.2] 
Just 1

*Main> findIndPositive [-1.3, -3.7, -4.2] 
Nothing
```

## Cowsay multiligne

- Écrivez un `cowsay` qui découpe les lignes trop longues. Pour simplifier, on
  considère qu'une ligne doit faire 5 mots maximum. Vous pouvez utiliser les
fonctions `concat`, `map` ou `concatMap`.

```text
$ runghc cowsay-multi.hs A profunctor is a ...
  _____________________________________
/ A profunctor is a bifunctor           \
| where the first argument is           |
| contravariant and the second argument |
\ is covariant.                         /
  -------------------------------------
        \   ^__^ 
         \  (oo)\_______ 
            (__)\       )\/\ 
                ||----w | 
                ||     || 
```

