---
title: PF TP5 - Traitements de liste, listes en compréhension
date: 2020-02-25
---

# Map

## Doubler des entiers

- Écrivez une fonction récursive `mapDoubler1` qui multiplie par 2 tous les
  entiers d'une liste donnée.

- Écrivez une fonction `mapDoubler2` équivalente mais utilisant `map`.

```text
$ ghci mapDoubler.hs 

*Main> mapDoubler1 [1..4]
[2,4,6,8]

*Main> mapDoubler2 [1..4]
[2,4,6,8]
```

## Réimplémenter map

- Écrivez une fonction récursive `mymap` équivalente à `map`.

- Testez votre fonction en doublant une liste d'entiers et en mettant en
  majuscule un texte.


# Filter

## Filter des entiers pairs

- Écrivez une fonction récursive `filterEven1` qui retient les entiers pairs
  d'une liste.

- Écrivez une fonction `filterEven2` équivalente mais utilisant `filter`.

```text
$ ghci filterEven.hs 

*Main> filterEven1 [1..4]
[2,4]

*Main> filterEven2 [1..4]
[2,4]
```

## Réimplémenter filter

- Écrivez une fonction récursive `myfilter` équivalente à `filter`.

- Testez votre fonction en filtrant les entiers pairs d'une liste et en
  filtrant les lettres d'un texte.


# Fold

## Calculer une somme

- Écrivez une fonction récursive `foldSum1` qui calcule la somme des entiers 
  d'une liste.

- Écrivez une fonction `foldSum2` équivalente mais utilisant `foldr`.

```text
$ ghci foldSum.hs 

*Main> foldSum1 [1..4]
10

*Main> foldSum2 [1..4]
10
```

## Réimplémenter foldr

- Écrivez une fonction récursive `myfoldr` équivalente à `foldr`.

- Testez votre fonction en calculant la somme d'une liste d'entiers et en
  calculant la lettre d'un texte la plus loin dans l'alphabet (par exemple, 'r'
  pour "barfoo").


# Listes en compréhension

## Équivalent de map ou filter

- Écrivez une fonction `doubler` qui multiplie par 2 les entiers d'une liste,
  en utilisant une liste en compréhension.

- Écrivez une fonction `pairs` qui retient les entiers pairs d'une liste,
  en utilisant une liste en compréhension.


## Réimplémenter map et filter

- Implémenter `map` et `filter` en utilisant des listes en compréhension.

- Testez en doublant les entiers d'une liste et en sélectionnant les entiers
  pairs d'une liste.

## Listes plus complexes

- Écrivez une fonction `multiples` qui prend un entier et retourne la liste de
  ses multiples. Utilisez une liste en compréhension.

```text
*Main> multiples 35
[1,5,7,35]
```

* * *

- Écrivez une fonction `combinaisons` qui prend deux listes et retourne la
  liste de ses combinaisons. 

```text
*Main> combinaisons ["pomme", "poire"] [13, 37, 42]
[("pomme",13),("pomme",37),("pomme",42),("poire",13),("poire",37),("poire",42)]
```

* * *

- Écrivez une fonction `tripletsPyth` qui prend un entier et retourne la liste
  des triplets pythagoriciens inférieurs à cet entier. Rappel : $(x,y,z)$ est
  pythagoricien si $x^2+y^2 = z^2$.

```text
*Main> tripletsPyth 13
[(3,4,5),(5,12,13),(6,8,10)]
```


# Récapitulatif

## Liste

- Réimplémentez `concat`. Écrivez une version "traitement de liste" et une
  version "liste en compréhension".

```text
*Main> myConcat1 ["foo", "bar"]
"foobar"
```

* * *

- Écrivez une fonction qui prend une liste de liste et retourne la liste de
  leur têtes. Écrivez une version "traitement de liste" et une version "liste
  en compréhension".

```text
*Main> getHeads1 ["foo", "bar"]
"fb"
```

* * *

- Écrivez une fonction équivalente mais gérant les listes vides. Écrivez une
  version "traitement de liste" et une version "liste en compréhension".

```text
*Main> getSafeHeads1 ["foo", "", "bar"]
"fb"
```


## Casser le chiffrement de César

Pour rappel, le chiffrement de César consiste à remplacer chaque lettre d'un
message  par la lettre à distance $n$ (modulo 26), dans l'ordre de
l'alphabet.  Par exemple, "hal" est le chiffrement de "ibm" avec $n=25$.
Déchiffrer revient à chiffrer par $-n$.

* * *

Pour casser le chiffrement de César, il suffit de trouver la valeur de
$n$ du message chiffré.  Une méthode brute-force classique consiste à
générer les 26 (dé)chiffrements possibles et à retenir celui dont la
distribution des fréquences d'apparition des différentes lettres est la plus
proche d'une distribution prédéterminée (par exemple, à partir d'un texte de
référence écrit dans la même langue).  Pour trouver la distribution $\hat
f$ la plus proche de la distribution de référence $f$, on cherche à
minimiser la statistique du $\chi^2$, c'est-à-dire à calculer : 

$$\underset{\hat f}{\arg\min} \quad \sum_i
\frac{(\hat f_i - f_i)^2}{f_i}$$

* * *

- Implémentez le chiffrement de César en Haskell.  Pour cela, commencez par
  écrire les deux fonctions suivantes.

```haskell
decaler :: Int -> Char -> Char
...
chiffrerCesar :: Int -> String -> String
...
```

> Indications : 
> 
>   - fonction `fromIntegral`
>   - fonctions `isLower`, `ord` et `chr` du module `Data.Char`
>   - on suppose que toutes les lettres sont en minuscule

* * *

- Implémentez l'attaque décrite précédemment, en écrivant les fonctions
  suivantes.

```haskell
compterOccurences :: Char -> String -> Int
...
compterLettres :: String -> Int
...
frequencesUk :: [Float]
frequencesUk = 
    [ 0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07
    , 0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06
    , 0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
    ]

calculerFrequences :: String -> [Float]
...
calculerKhi2 :: [Float] -> [Float] -> Float
...
casserCesar :: String -> [Float] -> Int
...
```

* * *

- Déchiffrez `sh cpjavpyl lza iypsshual, s'ljolj lza tha`.

