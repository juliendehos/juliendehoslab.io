---
title: PF TP4 - Fonctions récursives
date: 2020-02-25
---

# Fonctions récursives simples

## Factorielle

- Écrivez une fonction qui calcule la factorielle.

```text
$ ghci fact.hs

*Main> fact 2
2

*Main> fact 5
120
```

## PGCD

- Écrivez une fonction qui calcule le pgcd. Rappel : 

$$P(a,b) = \left\{ \begin{array}{l} a \text{ si } b = 0 \\ P(b, \text{reste de } a \text{ par } b ) \text{ sinon } \end{array} \right.$$


```text
$ ghci pgcd.hs

*Main> pgcd 49 35
7
```

## LoopEcho

- Écrivez une fonction `loopEcho :: Int -> IO String` qui saisit une ligne et
  l'affiche. Cette fonction boucle jusqu'à ce que la saisie soit vide ou que le
  nombre de répétitions (en paramètre) soit atteint. La fonction retourne un
  texte indiquant quel cas met fin à la fonction.

- Écrivez un programme qui appelle `loopEcho` pour 3 répétitions maximum, puis
  affiche la valeur retournée. 

* * *

```text
$ runghc loop-echo.hs 
> foobar
foobar
> 
empty line

$ runghc loop-echo.hs 
> foobar
foobar
> foo
foo
> bar
bar
loop terminated
```

# Récursivité terminale

## Factorielle TCO

- Écrivez une fonction `factTco`, récursive terminale, qui calcule la
  factorielle.

## Fibonacci

- Le fichier `fibo.hs` contient une fonction récursive terminale qui calcule le
  $n^\text{ième}$ terme de la suite de Fibonacci.

- Écrivez une fonction `fiboNaive` équivalente mais récursive non-terminale.

- Écrivez un programme qui permet de calculer Fibonacci avec l'une ou l'autre
  des fonctions précédentes. Regardez les temps de calculs.

* * *

```text
$ time runghc fibo.hs 
usage: <naive|tco> <n>

$ time runghc fibo.hs naive 35
9227465

real	0m14,015s
user	0m13,960s
sys	0m0,026s

$ time runghc fibo.hs tco 35
9227465

real	0m0,177s
user	0m0,150s
sys	0m0,025s
```


# Récursivité sur des listes

## Exercice 1

- Écrivez une fonction récursive `mylength` qui retourne la taille d'une liste.

- Écrivez une fonction récursive `toUpperString` qui met en majuscule un `String`.
  Indication : utilisez la fonction `toUpper` du module `Data.Char`.

- Écrivez une fonction récursive `onlyLetters` qui garde uniquement les lettres
  d'un `String`.  Indication : utilisez la fonction `isLetter` du module
  `Data.Char`.

* * *

```text
$ ghci liste1.hs 

*Main> mylength "foobar"
6

*Main> toUpperString "foo Bar"
"FOO BAR"

*Main> onlyLetters "foo Bar"
"fooBar"
```

## Exercice 2

- Écrivez une fonction récursive `mulListe` qui multiplie par un entier tous
  les éléments d'une liste.

- Écrivez une fonction récursive `selectListe :: (Int, Int) -> [Int] -> [Int]`
  qui retient les entiers d'une liste compris dans un intervalle donné.

- Écrivez une fonction récursive `sumListe` qui calcule la somme d'une liste
  d'entiers.

- Modifiez les signatures de fonctions de façon à les rendre génériques. 

* * *

```text
$ ghci liste2.hs 
*Main> mulListe 3 [3, 4, 1]
[9,12,3]

*Main> selectListe (2, 3) [3, 4, 1]
[3]

*Main> sumListe [3, 4, 1]
8

*Main> sumListe [3, 4, 1.0]
8.0
```

## Réimplémenter des fonctions

- Réimplémentez `last`, `init`, `replicate`, `drop` et `take`.

```text
$ ghci liste3.hs 

*Main> mylast "foobar"
'r'

*Main> myinit "foobar"
"fooba"

*Main> myreplicate 4 'f'
"ffff"

*Main> mydrop 3 "foobar"
"bar"

*Main> mytake 3 "foobar"
"foo"
```


# Récapitulatif

## Base de contacts

On veut implémenter une base de contacts (nom, adresse mail), à partir du code
suivant. 

```haskell
type Contact = (String, String)     -- (name, email)
type Base = [Contact]

newBase :: Base
newBase = []
```

* * *

- Écrivez une fonction `search :: String -> Base -> Bool` et une fonction
  `insert :: Contact -> Base -> Base`.

```text
*Main> b0 = insert ("toto","toto@ulco.fr") newBase

*Main> b0
[("toto","toto@ulco.fr")]

*Main> search "toto" b0
True

*Main> search "tata" b0
False

*Main> b1 = insert ("tata","tata@ulca.fr") b0

*Main> b1
[("tata","tata@ulca.fr"),("toto","toto@ulco.fr")]

*Main> insert ("tata","tata@ulca.fr") b1
[("tata","tata@ulca.fr"),("toto","toto@ulco.fr")]
```

* * *

- Écrivez une fonction `update :: Contact -> Base -> Base` et une fonction
  `remove :: String -> Base -> Base`

```text
*Main> update ("tata","tata@nimpe.org") b0
[("toto","toto@ulco.fr")]

*Main> update ("tata","tata@nimpe.org") b1
[("tata","tata@nimpe.org"),("toto","toto@ulco.fr")]

*Main> remove "tata" b0
[("toto","toto@ulco.fr")]

*Main> remove "tata" b1
[("toto","toto@ulco.fr")]
```


## Tri fusion

- Écrivez une fonction `triFusion` qui trie une liste selon l’algorithme de tri
  fusion.  Indication : écrivez une fonction `fusion` qui fusionne deux listes
  triées et utilisez la sur les deux moitiés (triées) de la liste à trier.

```text
$ ghci triFusion.hs 

*Main> triFusion [13, 42, 37, 2, 23, 2]
[2,2,13,23,37,42]

*Main> triFusion "foobar"
"abfoor"
```


## Calculatrice

- Écrivez une calculatrice gérant les additions et multiplications d’entiers,
  en notation préfixée. La calculatrice doit, en boucle, saisir une expression
  au clavier et afficher le résultat. Le programme termine quand on saisit une
  ligne vide. Indications : écrivez une fonction
  `calcu :: [String] -> (Int, [String])`.

```text
$ runghc calcu.hs 
> + 2 * 3 4
14
> + 2 3 4
5
(not parsed: 4)
> + 2
calcu.hs: parse error
CallStack (from HasCallStack):
  error, called at calcu.hs:5:12 in main:Main
```


