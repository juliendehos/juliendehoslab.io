---
title: Git
date: 2023-08-16
---


# Compte GitLab

Pour réaliser les TP et pour votre évaluation, on utilisera le serveur GitLab
de la fac.

- Vérifiez que vous avez un compte et que vous arrivez à vous connecter à
  l'[interface web](https://gitlab.dpt-info.univ-littoral.fr).


# Configuration initiale

- Ouvrez le fichier de configuration avec la commande :

    ```sh
    code ~/.config/home-manager/git.nix
    ```

- Remplacez les `TODO` par vos prénom/nom et e-mail. Enregistrez le fichier et
  quittez VSCode.

- Lancez la commande suivante pour mettre à jour la configuration :

    ```text
    home-manager switch
    ```


# Forker un dépôt existant

Pour chaque module, un dépôt de base vous est fourni. Vous devrez le forker
pour pouvoir travailler avec votre propre dépôt :

- Ouvrez un navigateur et connectez-vous au [GitLab de la
  fac](https://gitlab.dpt-info.univ-littoral.fr).

- Allez sur la page GitLab du dépôt fourni (cf la page de présentation du
  module).

- Cliquez sur le bouton "Fork".

* * *

- Choisissez le `namespace` correspondant à votre `username` et la visibilité
  `private`, puis cliquez sur le bouton "Fork project".

- Allez dans le menu "Manage > Members", cliquez sur le bouton "Invite members"
  et ajoutez votre encadrant de TP avec le rôle `reporter`.

- Dans un terminal, clonez votre nouveau dépôt : 

    ```sh
    git clone https://gitlab.dpt-info.univ-littoral.fr/<mon-login>/<mon-depot>.git

    # ou : git -c http.sslVerify=false clone ...
    ```

* * *

- Allez dans votre dépôt et ouvrez le fichier `README.md` :

    ```sh
    cd <mon-depot>
    
    code README.md
    ```

- Remplacez les `TODO` par vos nom/prénom/groupe. Attention, ce fichier sera
  analysé automatiquement donc n'y changez rien d'autre. Enregistrez le fichier
  et quittez VSCode.

- Validez votre modification et envoyez-la sur GitLab avec les commandes
  suivantes :

    ```sh
    git commit -am "maj du readme"

    git push
    ```

