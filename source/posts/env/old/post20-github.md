---
title: Git
date: 2024-01-12
---

Pour réaliser les TP et pour votre évaluation, on utilisera git et GitHub.


# Configuration GitHub

## Compte GitHub


- Créez-vous un compte sur [GitHub](https://github.com/) et connectez-vous.

## Clés SSH

Les clés SSH permettent à GitHub d'authentifier votre machine. Le principe est
de créer une paire clé publique + clé privée sur votre machine locale puis
d'enregistrer la clé publique sur votre compte GitHub. 

- Pour [générer une paire de
  clés](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent),
entrez la commande suivante dans un terminal, en indiquant votre adresse
e-mail :

    ```sh
    ssh-keygen -t ed25519 -C "votre adresse e-mail"
    ```

- Affichez votre clé publique avec la commande suivante et copiez-la :

    ```sh
    cat ~/.ssh/id_ed25519.pub
    ```

* * *

- Pour [enregistrer votre clé publique sur
  GitHub](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account),
allez dans les "Settings" **de votre compte** puis "SSH and GPG keys", "New SSH
key". Indiquez un titre (par exemple "108 mesange" si vous êtes dans cette
salle et sur cette machine) et collez votre clé publique.

* * *

> Attention : si vous utilisez plusieurs machines, vous devrez configurer des
> clés SSH pour chacune d'entre elles.


# Forker un dépôt existant

Pour chaque module, un dépôt de base vous est fourni. Vous devrez le forker
pour pouvoir travailler avec votre propre dépôt :

- Ouvrez un navigateur et connectez-vous à GitHub.

- Allez sur la page GitHub du dépôt fourni (cf la page de présentation du
  module).

- Cliquez sur le bouton "Fork" pour forker le projet sur votre compte.

- Allez dans les "Settings" **du projet** puis "Collaborators" puis "Add people"
  et ajoutez votre encadrant de TP.

* * *

- Dans un terminal, clonez votre nouveau dépôt : 

    ```sh
    git clone git@github.com:<mon-login>/<mon-depot>.git
    ```

- Allez dans votre dépôt et ouvrez le fichier `README.md` :

    ```sh
    cd <mon-depot>
    
    code README.md
    ```

- Remplacez les `TODO` par vos nom/prénom/groupe. Attention, ce fichier sera
  analysé automatiquement donc n'y changez rien d'autre. Enregistrez le fichier
  et quittez VSCode.

- Validez votre modification et envoyez-la sur GitHub avec les commandes
  suivantes :

    ```sh
    git commit -am "maj du readme"

    git push
    ```

