<map version="1.0.1">
    <node ID="ID_1" TEXT="Environnement de travail">
        <node ID="ID_2" POSITION="right" STYLE="fork" TEXT="Machine Virtuelle">
            <node ID="ID_8" POSITION="right" STYLE="fork" TEXT="Ordinateur perso"/>
            <node ID="ID_9" POSITION="right" STYLE="fork" TEXT="Ordinateur de la fac"/>
            <node ID="ID_10" POSITION="right" STYLE="fork" TEXT="NixOS"/>
        </node>
        <node ID="ID_3" POSITION="right" STYLE="fork" TEXT="Git et GitLab">
            <node ID="ID_16" POSITION="right" STYLE="fork" TEXT="Configurer Git">
                <arrowlink DESTINATION="ID_10" STARTARROW="Default"/>
            </node>
            <node ID="ID_29" POSITION="right" STYLE="fork" TEXT="Compte GitLab"/>
            <node ID="ID_17" POSITION="right" STYLE="fork" TEXT="Forker un d&#233;p&#244;t"/>
            <node ID="ID_18" POSITION="right" STYLE="fork" TEXT="Mettre &#224; jour un d&#233;p&#244;t"/>
        </node>
        <node ID="ID_5" POSITION="right" STYLE="fork" TEXT="VSCode">
            <node ID="ID_12" POSITION="right" STYLE="fork" TEXT="Editer un fichier"/>
            <node ID="ID_13" POSITION="right" STYLE="fork" TEXT="Travailler sur un projet"/>
            <node ID="ID_24" POSITION="right" STYLE="fork" TEXT="Configurations (C++, Haskell...)">
                <arrowlink DESTINATION="ID_10" STARTARROW="Default"/>
            </node>
        </node>
        <node ID="ID_4" POSITION="right" STYLE="fork" TEXT="Evaluation">
            <node ID="ID_20" POSITION="right" STYLE="fork" TEXT="Contr&#244;le continu (projets)">
                <arrowlink DESTINATION="ID_18" STARTARROW="Default"/>
            </node>
            <node ID="ID_21" POSITION="right" STYLE="fork" TEXT="Plagiat"/>
        </node>
        <node ID="ID_30" POSITION="right" STYLE="fork" TEXT="M&#233;thodes d'apprentissage"/>
    </node>
</map>