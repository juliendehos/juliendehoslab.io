<map version="1.0.1">
    <node ID="ID_1" TEXT="M&#233;thodes d'apprentissage">
        <node ID="ID_2" POSITION="right" STYLE="fork" TEXT="L'apprentissage">
            <node ID="ID_25" POSITION="right" STYLE="fork" TEXT="Compr&#233;hension / m&#233;morisation"/>
            <node ID="ID_13" POSITION="right" STYLE="fork" TEXT="M&#233;moire &#224; court / long terme"/>
            <node ID="ID_14" POSITION="right" STYLE="fork" TEXT="Oubli et consolidation"/>
            <node ID="ID_15" POSITION="right" STYLE="fork" TEXT="Fonctionnement">
                <node ID="ID_3" POSITION="right" STYLE="fork" TEXT="Encodage"/>
                <node ID="ID_4" POSITION="right" STYLE="fork" TEXT="Sch&#233;mas mentaux"/>
                <node ID="ID_5" POSITION="right" STYLE="fork" TEXT="Amorces"/>
                <node ID="ID_12" POSITION="right" STYLE="fork" TEXT="R&#233;cup&#233;ration"/>
            </node>
        </node>
        <node ID="ID_16" POSITION="right" STYLE="fork" TEXT="Strat&#233;gie &#224; long terme">
            <node ID="ID_21" POSITION="right" STYLE="fork" TEXT="Travail actif (r&#233;fl&#233;chir plut&#244;t que lire)"/>
            <node ID="ID_17" POSITION="right" STYLE="fork" TEXT="Diversifier les sujets abord&#233;s"/>
            <node ID="ID_18" POSITION="right" STYLE="fork" TEXT="R&#233;p&#233;ter le travail dans le temps"/>
        </node>
        <node ID="ID_19" POSITION="right" STYLE="fork" TEXT="Quelques m&#233;thodes">
            <node ID="ID_20" POSITION="right" STYLE="fork" TEXT="Tests (questions, QCM, flash-cards)"/>
            <node ID="ID_22" POSITION="right" STYLE="fork" TEXT="Apprentissage g&#233;n&#233;ratif (exercices, rappel libre)"/>
            <node ID="ID_23" POSITION="right" STYLE="fork" TEXT="Cartes mentales"/>
            <node ID="ID_24" POSITION="right" STYLE="fork" TEXT="Enseignement, rubberducking"/>
            <node ID="ID_26" POSITION="right" STYLE="fork" TEXT="&#201;crire du code, faire des projets..."/>
        </node>
    </node>
</map>