---
title: VSCode
date: 2023-08-16
---

La VM fournie contient un éditeur de code (VSCode) à peu près configuré pour
les modules de Génie Logiciel et de Programmation Fonctionnelle.


# Généralités

## Lancement

> Au lancement de VSCode sur les machines de la fac, vous aurez peut-être des
> fenêtres d'authentification. Il suffit de les fermer sans rien remplir.

* * *

- VSCode permet d'éditer n'importe quel fichier texte. Pour cela, lancez VSCode
  et faites `Open file...`, ou lancez VSCode depuis un terminal en indiquant le
fichier :

    ```sh
    code README.md
    ```

* * *

![](files/vscode-fichier.png){width="90%"}

* * *


- VSCode permet également de travailler sur des projets plus conséquents, avec
  une configuration dédiée (CMake, Cabal...). Pour cela, lancez VSCode et
faites `Open folder...`, ou lancez VSCode depuis un terminal en indiquant le
dossier :

    ```sh
    code .
    ```

* * *

![](files/vscode-projet.png){width="90%"}


## Créer des projets à partir de templates

Pour les TP, des projets de base sont généralement fournis. Sinon, vous pouvez
créer des projets préconfigurés avec
[kickstart](https://github.com/Keats/kickstart) et [ces
templates](https://gitlab.com/juliendehos/kickstart-templates). Par exemple :

```sh
kickstart https://gitlab.com/juliendehos/kickstart-templates -s cpp-simple
...
```


## Gérer les dépendances

La VM fournie permet de gérer les dépendances d'un projet via
[Nix](https://nixos.org/) et d'intégrer cette gestion dans VSCode.

- Si votre projet contient un fichier `shell.nix` ou `default.nix`, 
  fait un `Ctrl-Shift-p` puis tapez la commande `Nix-Env: Select environment`
et sélectionnez ce fichier `nix`. VSCode va construire la configuration
logicielle nécessaire puis vous proposera un bouton `Reload` pour la charger.

Cette manipulation n'est à faire qu'à la première ouverture du projet avec
VSCode, ou si vous modifiez le fichier `nix`. Elle permet de prendre en compte
la configuration Nix au niveau de l'éditeur de code, mais pas du terminal de
VSCode. Pour cela, il faut passer par un `nix-shell` (voir ci-dessous).


# C++

## Sans configuration de projet

Pour du code simple, compilé via `gcc` ou `make`, vous pouvez éditer les
fichiers dans VSCode et compiler dans le terminal VSCode. S'il y a une
configuration Nix, lancez la commande `nix-shell` puis lancez-y les commandes
de compilation.

* * *

![](files/vscode-cpp.png){width="90%"}


## Projet CMake

VSCode fournit quelques fonctionnalités pour gérer les projets CMake
(configuration, compilation, exécution...).

- Normalement VSCode charge automatiquement la configuration Nix. Si ce n'est
  pas le cas et que vous constatez un problème de dépendances non satisfaites,
suivez les instructions de la section précédente sur la gestion des
dépendances.

- Lancez la compilation via le bouton `build` dans la barre du bas. A la
  première compilation, VSCode demande la version du compilateur à utiliser :
sélectionnez le compilateur "Non spécifié" :

* * *

![](files/vscode-cmake1.png){width="90%"}

* * *

La barre du bas de VScode permet de sélectionner la configuration CMake, de
compiler, d'exécuter un programme compilé, etc.

* * *

![](files/vscode-cmake2.png){width="90%"}


# Haskell

## Sans configuration de projet

La VM fournie contient déjà GHC et quelques paquets Haskell classiques.
Normalement, vous pouvez donc exécuter directement vos codes Haskell :

* * *

![](files/vscode-haskell.png){width="90%"}

* * *

Pour certains projets spécifiques, un fichier `shell.nix` est fourni et il
faudra donc d'abord lancer un `nix-shell`.


## Projet Cabal + Nix

- Le terminal VSCode s'utilise comme un terminal classique : lancez-y un
  `nix-shell` puis utilisez les commandes Cabal classiques (`build`, `run`,
`clean`...).

- Si vous ajoutez des dépendances externes dans le fichier `.cabal` (section
  `build-depends`), vous aurez besoin de relancer VSCode pour que celui-ci les
  prenne en compte.

* * *

<video preload="metadata" controls>
<source src="files/env-haskell-vscode.mp4" type="video/mp4" />
![](files/env-haskell-vscode.png){width="70%"}

</video>


# Python

Pour les projets Python, il vaut mieux lancer chaque commande dans un
`nix-shell` spécifique.

- Pour exécuter un script (par exemple `scripts/main.py`), allez dans le terminal et lancez :

    ```sh
    nix-shell --run "python scripts/main.py"
    ```

* * *

![](files/vscode-python.png){width="90%"}

