---
title: Git
date: 2024-07-10
---


# Compte GitLab

Pour réaliser les TP et pour votre évaluation, on utilisera le serveur GitLab
de la fac.

- Vérifiez que vous avez un compte et que vous arrivez à vous connecter à
  l'[interface web](https://gitlab.dpt-info.univ-littoral.fr).


# Configurer le client git

- Au premier lancement de la VM, pensez à configurer le client git (nom, email) :

    ```sh
    git config --global user.name "Prénom Nom"
    git config --global user.email "mon email"
    ```


# Forker un dépôt existant

Pour chaque module, un dépôt de base vous est fourni. Vous devrez le forker
pour pouvoir travailler avec votre propre dépôt :

- Ouvrez un navigateur et connectez-vous au [GitLab de la
  fac](https://gitlab.dpt-info.univ-littoral.fr).

- Allez sur la page GitLab du dépôt fourni (cf la page de présentation du
  module).

- Cliquez sur le bouton "Fork".

* * *

- Choisissez le `namespace` correspondant à votre `username` et la visibilité
  `private`, puis cliquez sur le bouton "Fork project".

- Allez dans le menu "Manage > Members", cliquez sur le bouton "Invite members"
  et ajoutez votre encadrant de TP avec le rôle `Reporter`.

- Dans un terminal, clonez votre nouveau dépôt : 

    ```sh
    git clone https://gitlab.dpt-info.univ-littoral.fr/<mon-login>/<mon-depot>.git
    ```

* * *

- Allez dans votre dépôt et ouvrez le fichier `README.md` :

    ```sh
    cd <mon-depot>
    
    code README.md
    ```

- Remplacez les `TODO` par vos nom/prénom/groupe. Attention, ce fichier sera
  analysé automatiquement donc n'y changez rien d'autre. Enregistrez le fichier
  et quittez VSCode.

- Validez votre modification et envoyez-la sur GitLab avec les commandes
  suivantes :

    ```sh
    git commit -am "maj du readme"

    git push
    ```



