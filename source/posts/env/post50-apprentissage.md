---
title: Méthodes d'apprentissage
date: 2023-09-10
---


> Ci-dessous quelques explications et conseils sans prétention, qui peuvent
> vous aider pour vos études et qu'on essaiera d'utiliser en cours/TP.


# Principe de l'apprentissage

- comprendre, mémoriser

- mémoire de travail, mémoire à long terme

- oubli, consolidation

* * *

![Source : [ScienceEtonnante](https://www.youtube.com/watch?v=RVB3PBPxMWg)](files/env-apprentissage-schema.png){width="90%"}


# Stratégie d'apprentissage

- travailler activement (réfléchir/produire plutôt que juste lire)

- réviser plusieurs sujets à la fois (plutôt que passer des heures sur une seule notion)

- réviser régulièrement (plutôt qu'intensément juste avant un examen)


# Quelques méthodes

- tester ses connaissances : questions/réponses, QCM, flash-cards

- "apprentissage génératif" : exercices/projets, rappels libres, écrire du
  code, [cartes mentales](https://fr.wikipedia.org/wiki/Carte_heuristique) 

- enseigner/expliquer à quelqu'un,
  [rubberducking](https://fr.wikipedia.org/wiki/M%C3%A9thode_du_canard_en_plastique)


# Ce qu'on mettra en pratique

- exercices/projets en TP

- participation active en cours/TP

- rappels libres en début de cours/TP

- quelques flash-cards fournies, au format [Anki](https://apps.ankiweb.net/)


