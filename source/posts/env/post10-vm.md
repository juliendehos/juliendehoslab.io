---
title: Machine virtuelle
date: 2023-08-16
---

Pour les TP de Génie Logiciel et de Programmation Fonctionnelle, une machine
virtuelle vous est fournie. Vous devrez utiliser cette VM (soit sur une
machine de la fac, soit sur votre machine perso) en association avec vos dépôts
Gitlab. Attention : la VM est mise à jour chaque année, donc ne reprenez pas
une ancienne VM.


# Installer la VM

## Sur une machine de la fac

> Utilisez toujours la même machine de la salle TP (l'installation est
> locale donc si vous changez de machine, vous devrez installer la VM sur cette
> machine également).

- Ouvrez un terminal et lancez la commande suivante :

    ```sh
    sh /usr/local/VM/reset-vm.sh
    ```


## Sur votre machine perso

> Vérifiez que vous avez au moins 4 Go de RAM et 35 Go d'espace disque
> disponible.

- Installez VirtualBox.

- Téléchargez l'[image fournie](https://drive.google.com/file/d/1q08DA9Ul_4pr67D18v99T2rmT-r5EyTd)
  (environ 7 Go, [configuration](https://gitlab.com/juliendehos/ulco-nixos)).

- Lancez VirtualBox et importez l'image téléchargée.


# Utilisation de la VM

- Pour lancer la VM, ouvrez VirtualBox, sélectionnez la machine `nixos` et cliquez sur démarrer.

- Au premier lancement, [configurez le client Git](post20-git.html).

> Lors des TP, pensez à mettre à jour vos dépôts Gitlab (add/commit/push)
> régulièrement, pour ne pas perdre votre travail en cas de problème avec la VM
> ou de changement de salle TP.

