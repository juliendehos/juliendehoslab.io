---
title: Remarques sur le contrôle continu
date: 2023-08-16
---

- Le contrôle continu porte sur les TP et les mini-projets abordés en TP. Tous
  les exercices ne sont pas notés.

* * * 

- L'évaluation est faite à partir de vos dépôts GitLab. Vous devez donc :

    - préparer les dépôts correctement (forker les dépôts fournis en privé,
      ajouter l'encadrant de TP comme membre `Reporter`, mettre à
      jour le `README.md`);

    - mettre à jour régulièrement vos dépôts GitLab (add/commit/push à la fin
      de chaque issue/exercice/TP).

* * * 

- Pour rappel, le plagiat est interdit. Sauf instructions contraires de votre
  encadrant de TP, vous ne devez pas recopier une solution toute faite
  d'Internet, d'un camarade ou d'une IA.

* * * 

[//]: # ( 
- En L3, vous pouvez travailler seul ou en binôme. Si vous travaillez en
  binôme, mettez vos deux noms dans le README et mettez vos deux comptes GitLab
  comme membres du dépôt (avec le rôle `Maintainer`). 
- En M1, vous devez travailler seul.
)
