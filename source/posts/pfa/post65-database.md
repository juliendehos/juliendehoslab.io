---
title: Bases de données
date: 2021-08-23
---

# Généralités

## Backends

- Haskell a des libs pour beaucoup de SGBD

- SGBDR/SQL : sqlite, postgresql, mysql...

- NoSQL : mongodb, redis...


## Principaux types de libs

- clients mid-level :

    - via des requêtes SQL
    - et des fonctions pour échanger des données
    - et des fonctions d'accès au SGBD

- DSL :
    - $\approx$ ORM en POO
    - un peu de boiler-plate pour définir les types/tables
    - puis DSL classique (type-safe et composable)

- [Which type-safe database library should you use?](https://williamyaoh.com/posts/2019-12-14-typesafe-db-libraries.html)


## Pour notre module PFA

- backend sqlite

- libs : sqlite-simple et selda-sqlite

- en vrai, pour du web :

    - plutôt postgresql (car performances, transactions, etc)
    - quasi pareil côté Haskell, mais plus compliqué côté BD

- pour la BD d'exemple : `sqlite3 country.db < country.sql`

* * *

```sql
CREATE TABLE country (
    country_id INTEGER PRIMARY KEY AUTOINCREMENT,
    country_name TEXT
);

CREATE TABLE city (
    city_id INTEGER PRIMARY KEY AUTOINCREMENT,
    city_country INTEGER,
    city_name TEXT,
    FOREIGN KEY(city_country) REFERENCES country(country_id)
);

INSERT INTO country VALUES(1, 'BRETAGNE');
INSERT INTO country VALUES(2, 'france');
INSERT INTO country VALUES(3, 'Uruguay');
INSERT INTO city VALUES(1, 3, 'Montevideo');
INSERT INTO city VALUES(2, 1, 'Rennes');
INSERT INTO city VALUES(3, 2, 'Blavozy');
```



# Sqlite-simple

## Généralités

- lib mid-level pour sqlite (inspiré de postgresql-simple, mysql-simple...)

- principe d'utilisation :

    - connexion à la BD
    - fonctions d'accès via des requêtes SQL

- gestions des requêtes SQL :

    - SQL classique
    - gestion des paramètres (injections SQL)
    - gestion des résultats (ADT...)


## Exemple de base

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllCountries :: Connection -> IO [(Int,Text)]
selectAllCountries conn = query_ conn "SELECT * FROM country"

main :: IO ()
main = withConnection "country.db" selectAllCountries >>= mapM_ print
```

* * *

- connexion/déconnexion manuelle :

```haskell
main :: IO ()
main = do
    conn <- open "country.db"

    execute_ conn
        "INSERT INTO country(country_name) VALUES ('Australie')"

    close conn
```

## Utilisation des requêtes

- modes d'exécution :

    - `execute_` : sans résultat, sans paramètre
    - `execute` : sans résultat, avec paramètres
    - `query_` : avec résultats, sans paramètre
    - `query` : avec résultats, avec paramètres

- expliciter le type des données si ambiguité

- `Only` si un seul paramètre

- `[[a]]` si le résultat est un type `a` élémentaire (e.g., `Text`)

- compatible avec les ADT (via `FromRow`)


* * *

```haskell
    execute conn
        "INSERT INTO country VALUES (?, ?)"
        (420::Int, "Zimbabwe"::Text)

    execute conn
        "INSERT INTO country(country_name) VALUES (?)"
        (Only "Foobar" :: Only Text)

    res1 <- query_ conn
        "SELECT country_name FROM country"
        :: IO [[Text]]
    mapM_ print res1

    res2 <- query conn
        "SELECT * FROM country WHERE lower(country_name) LIKE ?"
        (Only $ pack "b%")
        :: IO [(Int, Text)]
    mapM_ print res2
```


* * *

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple

data MyData = MyData 
    { _city    :: Text
    , _country :: Text
    } deriving (Show)

instance FromRow MyData where
    fromRow = MyData <$> field <*> field 

selectAll :: Connection -> IO [MyData]
selectAll conn = query_ conn 
    "SELECT city_name, country_name FROM city \
    \INNER JOIN country ON city_country = country_id"
```


# Selda

## Généralités

- DSL en "Haskell simple"

- principe d'utilisation :

    - écrire des ADT correspondant aux tables
    - faire des requêtes via le DSL et les ADT

- inconvénients/avantages :

    - pas de SQL mais le DSL + typage
    - ADT à écrire mais peut aussi servir pour générer la BD

- **type-safe**


## Exemple de base

```haskell
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

data Country = Country
  { country_id   :: ID Country
  , country_name :: Text
  } deriving (Generic, Show)

instance SqlRow Country

country_table :: Table Country
country_table = table "country" [#country_id :- autoPrimary]
```

* * *

```haskell
selectAllCountries :: SeldaT SQLite IO [Country]
selectAllCountries = query $ select country_table

main :: IO ()
main = withSQLite "country.db" selectAllCountries >>= mapM_ print
```

## DSL

```haskell
selectAllCountryNames :: SeldaT SQLite IO [Text]
selectAllCountryNames = query $ do
    c <- select country_table
    return (c ! #country_name)

selectAllCountries2 :: SeldaT SQLite IO [Text :*: ID Country]
selectAllCountries2 = query $ do
    c <- select country_table
    return (c ! #country_name :*: c ! #country_id)
```

## "Jointures"

```haskell
data City = City
  { city_id      :: ID City
  , city_country :: ID Country
  , city_name    :: Text
  } deriving (Generic, Show)

instance SqlRow City

city_table :: Table City
city_table = table "city"
    [ #city_id :- autoPrimary
    , #city_country :- foreignKey country_table #country_id ]
```

* * *

```haskell
selectAllData :: SeldaT SQLite IO [City :*: Country]
selectAllData = query $ do
    cities <- select city_table
    countries <- select country_table
    restrict (cities ! #city_country .== countries ! #country_id)
    return (cities :*: countries)
```



# Travaux pratiques

## music

- Dans `tp-database/music` et en utilisant `sqlite-simple`, écrivez un programme
  `sqlite-simple1.hs` qui lit la base de données et affiche chaque titre, avec
son interprète. Faites au plus simple.

```text
[nix-shell]$ runghc -Wall sqlite-simple1.hs 

("Radiohead","Paranoid android")
("Radiohead","Just")
("Rage against the machine","Take the power back")
("Rage against the machine","How I could just kill a man")
("Ibrahim Maalouf","La porte bonheur")
```

* * *

- Écrivez un programme `sqlite-simple2.hs` qui affiche le contenu de la table
  `artist`, en utilisant un ADT.

```text
[nix-shell]$ runghc -Wall sqlite-simple2.hs 

Artist {artist_id = 1, artist_name = "Radiohead"}
Artist {artist_id = 2, artist_name = "Rage against the machine"}
Artist {artist_id = 3, artist_name = "Ibrahim Maalouf"}
```

* * *

- Écrivez un programme `selda-sqlite2.hs` équivalent à `sqlite-simple2.hs` mais
  utilisant `selda`.

```text
[nix-shell]$ runghc -Wall selda-sqlite2.hs 

Artist {artist_id = 1, artist_name = "Radiohead"}
Artist {artist_id = 2, artist_name = "Rage against the machine"}
Artist {artist_id = 3, artist_name = "Ibrahim Maalouf"}
```

* * *

- Idem pour un programme `selda-sqlite1.hs`.

```text
[nix-shell]$ runghc -Wall selda-sqlite1.hs 

"Radiohead" :*: "Paranoid android"
"Radiohead" :*: "Just"
"Rage against the machine" :*: "Take the power back"
"Rage against the machine" :*: "How I could just kill a man"
"Ibrahim Maalouf" :*: "La porte bonheur"
```


## movie-simple

- Regardez la base de données fournie.

- En complétant les fichiers `Movie1.hs` et `main-test.hs`, implémentez et
  testez les requêtes suivantes. Utilisez `sqlite-simple`, sans définir d'ADT.

```text
Movie1.dbSelectAllMovies
(1,"Bernie",1996)
...

Movie1.dbSelectAllProds
("Bernie",1996,"Albert Dupontel","R\233alisateur")
...

Movie1.dbSelectMoviesFromPersonId 1
["Citizen Kane"]
...

```

* * *

- Idem mais en utilisant des ADT, dans `Movie2.hs`.

```text
Movie2.dbSelectAllMovies
Movie {movie_id = 1, movie_title = "Bernie", movie_year = 1996}
...

Movie2.dbSelectAllProds
ProdInfo {_movie = "Bernie", _year = 1996, _person = "Albert Dupontel", _role = "R\233alisateur"}
...

Movie2.dbSelectMoviesFromPersonId 1
Movie {movie_id = 4, movie_title = "Citizen Kane", movie_year = 1941}
...

```

* * *

- Écrivez un serveur web `main-serve.hs`, qui présente le contenu de la base de
  données dans une page web. Indication : utilisez `liftIO` pour "faire une IO
dans scotty".

![](files/pfa-movie-simple.png){width="60%"}



## movie-selda

L'objectif de cet exercice est d'implémenter un projet équivalent à
`movie-simple` mais en utilisant `selda` à la place de `sqlite-simple`.

- Regardez le code fourni. Comment est créée la base de données ?

* * *

- Complétez `Movie.hs` et `main-test.hs`.

```text
dbSelectAllMovies
Movie {movie_id = 1, movie_title = "Bernie", movie_year = 1996}
...

dbSelectAllProds
Movie {movie_id = 1, movie_title = "Bernie", movie_year = 1996} :*: Person {person_id = 3, person_name = "Albert Dupontel"} :*: Role {role_id = 1, role_name = "R\233alisateur"}
...

dbSelectMoviesFromPersonId 1
Movie {movie_id = 4, movie_title = "Citizen Kane", movie_year = 1941}
...
```

- Implémentez un serveur web.


## webmusic

L'objectif de cet exercice est d'implémenter un projet dont on peut changer
facilement de backend/lib de base de données.

* * *

- Regardez et testez le code fourni.

<video preload="metadata" controls>
<source src="files/pfa-webmusic.mp4" type="video/mp4" />
![](files/pfa-webmusichello.png){width="60%"}

</video>

* * *

- Implémentez et testez le module `DbSqliteSimple.hs`, de façon à utiliser
  `sqlite-simple` à la place de `selda-sqlite`.

* * *

- Idem avec `DbPostgresqlSimple.hs` et `DbSeldaPostgresql.hs`. Indications pour Postgresql :

    - provisionner la base à partir d'un script :

        ```text
        $ psql -U postgres -f music-pg.sql
        ```

    - tester la base dans une console SQL :

        ```text
        $ psql "host='localhost' port=5432 dbname=music_db user=music_user"

        music_db=> SELECT * FROM artist;
         artist_id |       artist_name        
        -----------+--------------------------
                 1 | Radiohead
                 2 | Rage against the machine
                 3 | Ibrahim Maalouf
        ```

    - supprimer la base :

        ```text
        sudo -u postgres dropdb music_db
        ```


