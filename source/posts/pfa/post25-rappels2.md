---
title: Rappels (système de types, fonctions d'ordres supérieurs)
date: 2021-08-23
---


# Système de types

## Typage statique

- en Haskell, toute expression est typée statiquement :

```haskell
Prelude> :type True
True :: Bool

Prelude> :type "foo"
"foo" :: [Char]

Prelude> :type "foo" ++ show True
"foo" ++ show True :: [Char]
```

## Inférence de types

- les types sont déterminés automatiquement, par inférence

- mais, en bonne pratique, on les écrit quand même :

```haskell
myConstant :: Int
myConstant = 42

main :: IO ()
main = print myConstant 
```

## Types paramétrés

- les types peuvent être paramétrés (polymorphisme paramétrique) :

```haskell
Prelude> :type (++)
(++) :: [a] -> [a] -> [a]

Prelude> :type "foo" ++ "bar"
"foo" ++ "bar" :: [Char]

Prelude> :type [True] ++ [True,False]
[True] ++ [True,False] :: [Bool]
```

## Contraintes de types

- les types peuvent être contraints (polymorphisme ad-hoc) :

```haskell
Prelude> :type (+)
(+) :: Num a => a -> a -> a

Prelude> :type 20 + (22::Int)
20 + (22::Int) :: Int

Prelude> :type 20 + (22::Double)
20 + (22::Double) :: Double

Prelude> :type 20 + 22
20 + 22 :: Num a => a

Prelude> :type True + False
<interactive>:1:1: error:
    • No instance for (Num Bool) arising from a use of ‘+’
    • In the expression: True + False
```

## Quelques classes de types 

| classe         | fonctions de la classe           |
|----------------|----------------------------------|
| `Eq`           | `== /=`
| `Ord`          | `< <= > >= min max`
| `Show`         | `show`
| `Read`         | `read`
| `Enum`         | `succ pred`
| `Bounded`      | `minBound maxBound`
| `Num`          | `+ - * negate abs signum`
| `Integral`     | `div mod`
| `Fractional`   | `/ recip`


## Quelques instances

|           | `Eq` | `Ord` | `Num` | `Show` | `Read` | `Enum` |  autres      |
|-----------|:----:|:-----:|:-----:|:------:|:------:|:------:|:------------:|
| `Bool`    |  X   |  X    |       |  X     |  X     |  X     |              |
| `Char`    |  X   |  X    |       |  X     |  X     |  X     |              |
| `String`  |  X   |  X    |       |  X     |  X     |        |              |
| `Int`     |  X   |  X    |  X    |  X     |  X     |  X     | `Integral`   |
| `Integer` |  X   |  X    |  X    |  X     |  X     |  X     | `Integral`   |
| `Float`   |  X   |  X    |  X    |  X     |  X     |  X     | `Fractional` |
| `Double`  |  X   |  X    |  X    |  X     |  X     |  X     | `Fractional` |

* * *

- info sur des types :

```haskell
Prelude> :info Bool
data Bool = False | True 	-- Defined in ‘GHC.Types’
instance Eq Bool -- Defined in ‘GHC.Classes’
instance Ord Bool -- Defined in ‘GHC.Classes’
instance Show Bool -- Defined in ‘GHC.Show’
instance Read Bool -- Defined in ‘GHC.Read’
instance Enum Bool -- Defined in ‘GHC.Enum’
instance Bounded Bool -- Defined in ‘GHC.Enum’
```

* * *

- info sur des classes :

```haskell
Prelude> :info Eq
class Eq a where
  (==) :: a -> a -> Bool
  (/=) :: a -> a -> Bool
  {-# MINIMAL (==) | (/=) #-}
  	-- Defined in ‘GHC.Classes’
instance Eq Bool -- Defined in ‘GHC.Classes’
instance Eq Int -- Defined in ‘GHC.Classes’
instance Eq Double -- Defined in ‘GHC.Classes’
instance Eq Char -- Defined in ‘GHC.Classes’
...
```

* * *

- en pratique :

```haskell
repeat2 :: [a] -> [a]
repeat2 xs = xs ++ xs

palindrome :: Eq a => [a] -> Bool
palindrome xs = xs == reverse xs

main :: IO ()
main = do
    print $ repeat2 "to"
    print $ repeat2 [True,False]
    print $ palindrome "radar"
    print $ palindrome [True,True]
```


# Fonctions d'ordres supérieurs

## Fonctions et lambdas

```haskell
Prelude> f1 xs = xs ++ reverse xs

Prelude> f2 = \xs -> xs ++ reverse xs

Prelude> f1 "an"
"anna"

Prelude> f2 "an"
"anna"
```

## Forme curryfiée

```haskell
concat1 :: String -> String -> String
concat1 xs ys = xs ++ ys

concat2 :: String -> String -> String
concat2 = \xs -> \ys -> xs ++ ys

main :: IO ()
main = do
    putStrLn $ concat1 "foo" "bar"
    putStrLn $ concat2 "foo" "bar"
```

## Évaluation partielle

```haskell
Prelude> import Data.Char

Prelude Data.Char> :type toUpper 
toUpper :: Char -> Char

Prelude Data.Char> upperize1 xs = map toUpper xs

Prelude Data.Char> upperize1 "foobar"
"FOOBAR"

Prelude Data.Char> upperize2 = map toUpper 

Prelude Data.Char> upperize2 "foobar"
"FOOBAR"
```

## Passer une fonction en paramètre

```haskell
Prelude> :type map
map :: (a -> b) -> [a] -> [b]

Prelude> map (\c -> c == 'o') "foobar"
[False,True,True,False,False,False]
```

## Composition de fonctions

```haskell
Prelude> f1 n = (n + 1) * 2

Prelude> f2 = (*2) . (+1)

Prelude> f1 20
42

Prelude> f2 20
42
```

## Traitements de liste

```haskell
Prelude> map (*2) [18..21]
[36,38,40,42]

Prelude> filter even [18..21]
[18,20]

Prelude> foldr (+) 0 [18..21]
78

Prelude> import Data.List

Prelude Data.List> foldl' (+) 0 [18..21]
78
```

## Fonction totale, fonction partielle

```haskell
Prelude> reverse "foobar"
"raboof"

Prelude> reverse ""
""

Prelude> head "foobar"
'f'

Prelude> head ""
*** Exception: Prelude.head: empty list
```


# Travaux pratiques

Travaillez dans le dossier `tp-rappels2` de votre dépôt git.


## Fonctions totales, fonctions polymorphes

- Dans le fichier `totalpoly.hs`, écrivez une fonction totale `safeTailString`
  qui retourne la queue d'une `String`. Idem avec `safeHeadString.`

```haskell
*Main> safeTailString "foobar"
"oobar"

*Main> safeTailString ""
""
```

* * *

- Écrivez des fonctions `safeTail` et `safeHead` équivalentes mais polymorphes.

```haskell
*Main> safeTail "foobar"
"oobar"

*Main> safeTail [1..4]
[2,3,4]
```

## Contraintes de types

- Dans le fichier `nocase.hs`, écrivez une fonction `isSorted` qui retourne si
  une liste est triée ou non.  Votre fonction doit être la plus générique
  possible.

```haskell
*Main> isSorted [1,2,3,4]
True

*Main> isSorted [2,3,1]
False

*Main> isSorted "abc"
True

*Main> isSorted "foobar"
False
```

* * *

- Écrivez une fonction `nocaseCmp` qui compare deux `String` sans tenir compte
  de la casse.

```haskell
*Main> "tata" < "TOTO"
False

*Main> "tata" `nocaseCmp` "TOTO"
True

*Main> "tata" `nocaseCmp` "TAT"
False
```

## Fonctions d'ordre supérieur

À partir de maintenant, écrivez du "vrai" Haskell, c'est-à-dire avec fonctions
génériques, composition, évaluation partielle, etc.

- Dans le fichier `higher-order.hs`, écrivez une fonction `threshList` qui
  seuille les éléments d'une liste selon une valeur max donnée.

```haskell
*Main> threshList 3 [1..5::Int]
[1,2,3,3,3]

*Main> threshList 'c' ['a'..'k']
"abccccccccc"
```

* * *

- Écrivez une fonction `selectList` qui sélectionne les éléments d'une liste selon
  une valeur max donnée.

```haskell
*Main> selectList 3 [1..5::Int]
[1,2]

*Main> selectList 'c' ['a'..'k']
"ab"
```

* * *

- Écrivez une fonction `maxList` qui retourne la valeur max d'une liste.

```haskell
*Main> maxList [13, 42, 37::Int]
42

*Main> maxList "barfoo"
'r'
```

## Réimplémentation des traitements de listes

Dans le fichier `list-func.hs` :

- écrivez une fonction `mymap1`, récursive non-terminale, qui implémente `map`;

- écrivez une fonction `mymap2` identique mais récursive terminale;

* * *

- écrivez une fonction `myfilter1`, récursive non-terminale, qui implémente `filter`;

- écrivez une fonction `myfilter2` identique mais récursive terminale;

* * *

- écrivez une fonction `myfoldl`, récursive terminale, qui implémente `foldl`;

- écrivez une fonction `myfoldr`, récursive non-terminale, qui implémente `foldr`.


## Mini-projet : fileviewer

- Testez le programme `hello-gtk.hs` fourni.

- Écrivez un programme `fileviewer-gtk.hs` permettant d'ouvrir un fichier et de
  l'afficher.  Indication : utilisez un `ScrolledWindow` contenant un
  `TextView`, et un `FileChooserButton`, et regardez la
  [doc de gi-gtk](https://hackage.haskell.org/package/gi-gtk-3.0.41) (vérifiez
  la version avec `ghc-pkg list`).

* * *

<video preload="metadata" controls>
<source src="files/pfa-fileviewer.mp4" type="video/mp4" />
![](files/pfa-fileviewer.png){width="60%"}

</video>

