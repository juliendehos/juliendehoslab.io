---
title: Frameworks backend
date: 2021-08-23
---

# Généralités

## Rappel sur le web-backend

- application web, côté serveur

- composants : serveur HTTP + framework + application

- implémentations types :

    - serveur générique (par exemple, apache/nginx + module PHP)
    - serveur intégré (par exemple, nodejs)

- grande variétés de frameworks, mais au moins : HTTP + routage


## Backend en Haskell

- plusieurs "bons frameworks"

- avantages :

    - stabilité, performances, maintenance
    - concurrence/asynchronisme
    - typage des données, voire des urls
    - parsing, stateless...

- cf [State of the Haskell ecosystem](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md#server-side-web-programming)


## Frameworks backend Haskell

- yesod, snap, scotty, spock, servant...

- en combinaison avec warp (serveur web) et wai (middleware)

![](files/pfa-backend-stack.png){width="50%"}


## Déploiement avec Nix + Docker

- fichier de release avec l'appli + les données (`nix/release.nix`) :

```nix
let
  pkgs = import <nixpkgs> {};
  app-src = ./.. ;
  app = pkgs.haskellPackages.callCabal2nix "monprojet" ./.. {};

in
  pkgs.runCommand "app" { inherit app; } ''
    mkdir -p $out/{bin,static}
    cp ${app}/bin/monprojet $out/bin/
    cp ${app-src}/static/* $out/static/
  ''
```

* * *

- configuration de l'image Docker (`nix/docker.nix`) :

```nix
{ pkgs ? import <nixpkgs> {} }:

let
  app = import ./release.nix;
  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in
  pkgs.dockerTools.buildImage {
    name = "monprojet";
    tag = "latest";
    config = {
      WorkingDir = "${app}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${app}/bin/monprojet" ];
    };
  }
```

* * *

- puis construire l'image et déployer comme d'habitude :

```sh
nix-build nix/docker.nix

docker load < result

heroku create monprojet-app1

docker tag monprojet:latest registry.heroku.com/monprojet-app1/web
docker push registry.heroku.com/monprojet-app1/web
heroku container:release web --app monprojet-app1
```



# Scotty

## Généralités

- framework léger : routage + warp/wai

- simple à apprendre, bonne intégration avec les libs Haskell

- pour des projets plus compliqués : yesod, servant...


## Routing de base

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

main :: IO ()
main = scotty 3000 $ do
  get "/" $ text "this is /"
  get "/page1" $ text "this is /page1"
  get "/page1/subpage2" $ text "/page1/subpage2"
```

* * *

![](files/pfa-scotty1.png)


## Middleware WAI

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Data.Maybe (fromMaybe)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Web.Scotty

main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    scotty port $ do
        middleware logStdoutDev
        middleware $ gzip def { gzipFiles = GzipCompress }
        middleware $ staticPolicy $ addBase "static"
        get "/" $ html "<img src='haskell-logo.svg'>" 
```

* * *

![](files/pfa-scotty2.png)


## Formats de réponse

- aperçu :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

main :: IO ()
main = scotty 3000 $ do
  get "/mytext" $ text "this is some text" 
  get "/myhtml" $ html "<h1>this is some html</h1>"
  get "/myjson" $ json ("toto"::String, 42::Int)
```

* * *

![](files/pfa-scotty3a.png)

* * *

- génération de json avec aeson :

```haskell
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson hiding (json)
import GHC.Generics
import Web.Scotty

data Person = Person { _name::String, _age::Int } deriving Generic

instance ToJSON Person

person :: Person
person = Person "toto" 42

main :: IO ()
main = scotty 3000 $ get "/" $ json person
```

* * *

![](files/pfa-scotty3b.png)

* * *

- génération de html avec lucid :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Web.Scotty

main :: IO ()
main = scotty 3000 $ get "/" $ html $ renderText myPage

myPage :: Html ()
myPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "mypage"
        body_ $ do
             h1_ "this is"
             p_ "mypage"
```

* * *

![](files/pfa-scotty3c.png)



## Gestion des URL

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

main :: IO ()
main = scotty 3000 $ do
  get "/" $ text "Hello unknown user !"
    -- http://localhost:3000/

  get "/name1/:name" $ do
    name <- captureParam "name"
    text $ "Hello " <> name <> " !"
    -- http://localhost:3000/name1/toto

  get "/name2" $ do
    name <- queryParam "name" `rescue` 
                (\(StatusError _ _) -> return "unknown user")
    text $ "Hello " <> name <> " !"
    -- http://localhost:3000/name2?name=toto
    -- http://localhost:3000/name2
```

* * *

![](files/pfa-scotty4.png)



## Accès concurrents/asynchrones

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import Data.IORef (modifyIORef', newIORef, readIORef)
import TextShow (showtl)
import Web.Scotty

main :: IO ()
main = do
    counterRef <- newIORef (0::Int)
    scotty 3000 $ do
        get "/" $ do
            counter <- liftIO $ readIORef counterRef
            html $ showtl counter <> " - <a href='inc'>increment</a>"
        get "/inc" $ do
            liftIO $ modifyIORef' counterRef (+1)
            redirect "/"
```

* * *

![](files/pfa-scotty5.png)


## Gestion des formulaires

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Lucid

main :: IO ()
main = scotty 3000 $ do
    get "/" $ html $ renderText $ html_ $ body_ $ do
        form_ [action_ "/send", method_ "post"] $ do
            input_ [name_ "txt", value_ "enter text here"]
            input_ [type_ "submit"]

    post "/send" $ do
        txt <- formParam "txt"
        text $ "you sent: " <> txt
```

* * *

![](files/pfa-scotty6a.png)

![](files/pfa-scotty6b.png)


## Model-View-Controler

- `app/Main.hs` :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

import Model
import View

main :: IO ()
main = do
    let model = Model 13 37
    scotty 3000 $ do
        get "/page1" $ html $ viewPage1 model
        get "/page2" $ html $ viewPage2 model
```

* * *

- `src/Model.hs` :

```haskell
module Model where

data Model = Model
    { _value1 :: Int
    , _value2 :: Int
    }
```

* * *

- `src/View.hs` :

```haskell
{-# LANGUAGE OverloadedStrings #-}

module View where

import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid
import TextShow (showtl)

import Model

viewPage1 :: Model -> L.Text
viewPage1 (Model v1 v2) = mkPage "page1" $ do
    div_ [class_ "row"] $ toHtml $ "value1: " <> showtl v1
    div_ [class_ "row"] $ toHtml $ "value2: " <> showtl v2

viewPage2 :: Model -> L.Text
viewPage2 _ = mkPage "page2" "this is page2"
```

* * *

```haskell
bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: L.Text -> Html () -> L.Text
mkPage myTitle myHtml = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ $ toHtml myTitle
        body_ $ div_ [class_ "container"] myHtml
```

* * *

![](files/pfa-scotty-mvc.png)


# Travaux pratiques

## Découverte de scotty

- Dans `tp-backend/hello-scotty`, implémentez un serveur scotty avec deux routes texte
  `route1` et `route1/route2`.

- Ajoutez des logs et la gestion des fichiers statiques, dans le middleware.

- Ajoutez une route `html1` renvoyant un code html simple et une route
  `json1` renvoyant un code json simple.

- Ajoutez une route `json2` renvoyant le code json d'une `Person`.

* * *

- Générez la page principale avec `lucid`. Implémentez des liens vers chaque
  route de l'application. 

- Implémentez une route `add1` qui ajoute 2 paramètres fournis obligatoirement.

- Implémentez une route `add2` qui ajoute 2 paramètres fournis optionnellement.

- Ajoutez une route `index` qui redirige vers la route racine.

* * *

<video preload="metadata" controls>
<source src="files/pfa-hello-scotty.mp4" type="video/mp4" />
![](files/pfa-hello-scotty.png){width="80%"}

</video>


## Mini-projet : ptivelo

À partir de `tp-backend/ptivelo`, implémentez une application avec :

- une route `/` (page HTML) et une route `/riders` (données JSON)
- logs + fichiers statiques + compression + port
- architecture MVC
- bootstrap + `static/styles.css`
- déploiement sur Heroku en utilisant Nix + Docker

* * *

<video preload="metadata" controls>
<source src="files/pfa-ptivelo.mp4" type="video/mp4" />
![](files/pfa-ptivelo.png){width="80%"}

</video>



## Mini-projet : monfauxrhum

À partir de `tp-backend/monfauxrhum`, implémentez une application de forum
basique avec :

- une page principale (route `/`) et une page de rédaction (route `write`)
- architecture MVC
- accès concurrent aux données avec `IORef`
- formulaire avec envoi en `POST`

* * *

<video preload="metadata" controls>
<source src="files/pfa-monfauxrhum.mp4" type="video/mp4" />
![](files/pfa-monfauxrhum.png){width="80%"}

</video>

