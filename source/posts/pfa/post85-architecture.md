---
title: Architecture de code
date: 2025-01-31
---


# Anim

L'objectif de cet exercice est de ré-écrire un projet (`tp-architecture/anim`)
en utilisant quelques fonctionnalités intéressantes d'Haskell (`Lens`, monade
`State`, transformateurs de monades, style MTL).


## 1. Projet initial

Le projet initial est une animation où une balle se déplace et rebondit contre
les bords de la fenêtre. La touche `Space` permet de réinitialiser la balle
aléatoirement et la touche `Esc` de quitter.

![](files/architecture-anim.png){width="30%"}

Références :

- [Linear.V2](https://hackage.haskell.org/package/linear-1.23/docs/Linear-V2.html)

* * *

1. Compilez, lancez et testez le programme.

2. Regardez le code fourni, notamment le fichier `src/Anim1.hs` :

    - Quels types sont définis?
    - Comment est implémentée la balle aléatoire?
    - Comment sont implémentés les déplacements/rebonds de la balle?


## 2. Lens

Les `Lens` permettent de manipuler efficacement des structures de données
imbriquées.

Références :

- [Control.Lens](https://hackage.haskell.org/package/lens-5.3.3/docs/Control-Lens.html)
- [Overview](https://github.com/ekmett/lens/wiki/Overview)
- [Operators](https://github.com/ekmett/lens/wiki/Operators)
- [lens: Lenses, Folds and Traversals](https://hackage.haskell.org/package/lens)

* * *

1. À partir du module `Anim1`, créez un nouveau module `Anim2` et travaillez
   dans celui-ci (pensez à modifier la configuration de projet et le programme
   principal).

2. Modifiez le code de façon à pouvoir utiliser les `Lens` sur les type `Ball`
   et `Model`.

3. Modifiez les fonctions `handle*` en utilisant les `Lens`.

4. Modifiez les fonction `update*` en utilisant les `Lens`.


## 3. La monade State

La monade `State` simule un état courant modifiable. Elle peut être pratique
pour implémenter un calcul un peu complexe, comme par exemple mettre à jour la
balle dans notre programme d'animation.

Références :

- [The State monad](https://hackage.haskell.org/package/mtl-2.3.1/docs/Control-Monad-State-Strict.html#g:2)
- [Examples](https://hackage.haskell.org/package/mtl-2.3.1/docs/Control-Monad-State-Strict.html#g:4)

* * *

1. À partir du module `Anim2`, créez un nouveau module `Anim3`.

2. Modifiez les fonctions `update*` de façon à travailler dans une monade
   `State` dont l'état courant est une balle, que l'on va mettre à jour.

3. Modifiez la fonction `handleTime` de façon à exécuter la mise à jour.


## 4. Le transformateur StateT

On voudrait afficher un message lors des rebonds (par exemple `left` quand la
balle rebondit sur le bord gauche, etc).

Références :

- [The StateT monad ](https://hackage.haskell.org/package/mtl-2.3.1/docs/Control-Monad-State-Strict.html#g:3)

* * *

1. Pourquoi ne peut-on pas faire actuellement d'affichage dans la fonction
   `updateBounces` ?

2. À partir du module `Anim3`, créez un nouveau module `Anim4`.

3. Modifiez les fonctions `update*` et `handleTime` de façon à remplacer la
   monade `State` par le transformateur `StateT` sur la monade `IO`.

4. Ajoutez les affichages voulus, dans la fonction `updateBounces`.


## 5. Implémenter un transformateur 

On voudrait maintenant implémenter notre propre fonction d'affichage des
rebonds. Pour cela, on va créer notre propre transformateur, au lieu d'utiliser
la monade `IO`.

* * *

1. À partir du module `Anim4`, créez un nouveau module `Anim5`.

2. Ajouter le transformateur `LoggerStdoutT` suivant, dans votre code :

    ```haskell
    newtype LoggerStdoutT m a = LoggerStdoutT
      { unLoggerStdoutT :: ReaderT (String -> IO ()) m a 
      } deriving (Functor, Applicative, Monad, MonadTrans)

    runLoggerStdoutT :: LoggerStdoutT m a -> m a
    runLoggerStdoutT app = 
      runReaderT (unLoggerStdoutT app) (\str -> putStrLn $ "[anim] " ++ str)

    logMsg :: MonadIO m => String -> LoggerStdoutT m ()
    logMsg str = LoggerStdoutT $ ReaderT (\f -> liftIO $ f str)
    ```

3. Modifiez vos fonctions `updateBounces` et `handleTime` de façon à utiliser
   ce transformateur.


## 6. Le style MTL 

Le "style MTL" consiste à spécifier les monades nécessaires dans les
contraintes de types, plutôt que dans une pile de transformateurs.

Références :

- [Control.Monad.State.Class](https://hackage.haskell.org/package/mtl-2.3.1/docs/Control-Monad-State-Class.html)

* * *

1. Quels sont les inconvénients d'une pile de transformateurs explicite ?

2. À partir du module `Anim5`, créez un nouveau module `Anim6`.

3. Ajoutez le code suivant, qui définit une monade `MonadLogger`, utilisable
   avec nos transformateurs :

    ```haskell
    class Monad m => MonadLogger m where
      logMsg :: String -> m ()

    instance MonadIO m => MonadLogger (LoggerStdoutT m) where
      logMsg str = LoggerStdoutT $ ReaderT (\f -> liftIO $ f str)

    instance MonadIO m => MonadIO (LoggerStdoutT m) where
      liftIO = lift . liftIO

    instance MonadLogger m => MonadLogger (StateT s m) where
      logMsg = lift . logMsg
    ```

4. Modifiez les fonctions `update*` et `handleTime` de façon à utiliser
   `MonadState` et `MonadLogger`, plutôt qu'une pile de transformateurs.


## 7. Implémenter des tests

Pour terminer, on veut pouvoir écrire des tests et récupérer les affichages
produits.

Références :

- [Control.Monad.Trans.Writer.Lazy](https://hackage.haskell.org/package/transformers-0.6.1.2/docs/Control-Monad-Trans-Writer-Lazy.html)

* * *

1. À partir du module `Anim6`, créez un nouveau module `Anim7`.

2. Ajoutez et complétez la fonction de test suivante, de façon à récupérer les
   affichages (utilisez le transformateur `WriterT`) : 

    ```haskell
    test :: (Ball, [String])
    test = 

      let app :: (MonadState Ball m, MonadLogger m) => m ()
          app = do
            get >>= logMsg . show
            updateBounces
            get >>= logMsg . show

          ball0 = Ball {_pos = V2 0 (yMax+1), _vel = V2 0 10}

      in ...
    ```

* * *

3. Modifiez la fonction `run` de façon à lancer votre test et à afficher les
   résultats :

    ```sh
    [nix-shell]$ cabal run

    result:
    Ball {_pos = V2 0.0 129.0, _vel = V2 0.0 (-10.0)}

    logs:
    Ball {_pos = V2 0.0 131.0, _vel = V2 0.0 10.0}
    top
    Ball {_pos = V2 0.0 129.0, _vel = V2 0.0 (-10.0)}
    ```


# CommitSearch

L'objectif de cet exercice est de découvrir les systèmes d'effets et de
comprendre comment on peut les utiliser pour architecturer du code en Haskell
(`tp-architecture/commitsearch`).


## 1. Projet initial

Il permet de rechercher des messages de commits dans un dépot GitLab via son
API
([ulco-pfa-etudiant](https://gitlab.dpt-info.univ-littoral.fr/api/v4/projects/14/repository/commits)).


![](files/architecture-commitsearch.png){width="70%"}

* * *

- Regardez le fichier de configuration `cabal`. Quels sont les différents
  éléments du projet (exécutables, bibliothèques)?

- Regardez le contenu du dossier `src/CommitSearch`. Comment est architecturé
  le projet ?

- Quel est l'intérêt de séparer les parties `Effects` et `Interpreters`?


## 2. Implémenter un effet

On veut ajouter un système de logging à notre projet.

- En vous inspirant du code déjà fourni (et éventuellement de la doc),
  implémentez un effet `Logger`. Celui-ci aura une fonction `LogMsg` permettant
  de logger un `Text`.

Références :

- [effectful-core: An easy to use, performant extensible effects library.](https://hackage.haskell.org/package/effectful-core)
- [effectful-th: Template Haskell utilities for the effectful library.](https://hackage.haskell.org/package/effectful-th)


## 3. Implémenter un interpréteur

- Implémentez un interpréteur `LoggerStdout` qui affiche les messages de log à
  l'écran avec un préfixe donné. Par exemple, si on demande de logger `foo bar`
  avec un interpréteur préfixant par `blou`, l'écran devra afficher `[blou] foo
  bar`.


## 4. Utiliser le nouvel effet/interpréteur dans les applications

- Dans les applications `Test` et `Server`, ajoutez quelques messages de logs
  permettant de tracer l'exécution des programmes.

- Lancez les exécutables et vérifiez que les messages de logs sont affichés
  correctement.


## 5. Implémenter des tests automatisés

- Complétez les tests automatisés de façon à vérifier les messages de logs
  produits. Pour cela, implémentez un interpréteur de l'effet `Logger` qui
  stocke les messages en mémoire.


## 6. S'il vous reste du temps

- Modifiez l'interpréteur `DbCommitHttp` de façon à ce qu'il génère des
  messages de log. Vérifiez que les messages sont bien affichés, à l'exécution.



