---
title: Types algébriques de données
date: 2021-08-23
---

# Types algébriques

## Généralités

- quelques particularités de Haskell :

    - fonctionnel pur
    - système de types
    - évaluation non-stricte 

- Haskell a un système de types évolué :

    - algebraic data type, type-class, higher-kinded type (HKT)
    - Type-Driven Development, type-level programming


## Isomorphisme de Curry-Howard

- notion de calculabilité

|  Algèbre       |  Logique         |  Types           |
|:--------------:|:----------------:|:----------------:|
|  $a + b$       |  $a \vee b$      |  `Either a b`    |
|  $a \times b$  |  $a \wedge b$    |  `(a,b)`         |
|  $a^b$         |  $a \implies b$  |  `a -> b`        |
|  $a=b$         |  $a \iff b$      |  *isomorphisme*  |
|  $0$           |  $\bot$          |  `Void`          |
|  $1$           |  $\top$          |  `()`            |

[Thinking with Types](https://thinkingwithtypes.com/)


## Définir de nouveaux types

- en Haskell, il est très courant de définir de nouveaux types, pour modéliser les données

- le nom d'un type ou d'une valeur doit commencer par une majuscule

- mots-clés `data`, `newtype`, `type`


## Type somme

- "somme de valeurs"

- union, énumérations

```haskell
data Color = Blue | White | Red

getCode :: Color -> String
getCode Blue  = "#0000FF"
getCode White = "#FFFFFF"
getCode Red   = "#FF0000"

main = putStrLn $ getCode Red
```


## Type produit

- "produit de valeurs"

```haskell
data Rectangle = MkRectangle Double Double

getArea :: Rectangle -> Double
getArea (MkRectangle w h) = w*h

main = print $ getArea $ MkRectangle 4 2
```


## Types algébriques

- "somme de types produits"

```haskell
data Shape = Disk Double | Rectangle Double Double

getArea :: Shape -> Double
getArea (Disk r) = pi*r^2
getArea (Rectangle w h) = w*h

main = do
    print $ getArea $ Disk 2
    print $ getArea $ Rectangle 4 2
```


## Type enregistrement

- type algébrique, avec champs nommés

```haskell
data Rectangle = MkRectangle
    { rectangleWidth :: Double
    , _height        :: Double
    } 

r1 = MkRectangle 4 2

r2 = MkRectangle { _height = 2, rectangleWidth = 4 }

r3 = r1 { rectangleWidth = 42 }

w3 :: Double
w3 = rectangleWidth r3
```


## Types paramétrés

- on peut utiliser des paramètres pour les types des champs

```haskell
data Rectangle a = MkRectangle a a

r1 :: Rectangle Int
r1 = MkRectangle 4 2

r2 :: Rectangle Double
r2 = MkRectangle 42 13.37
```


## Types récursifs

- un type peut être défini en fonction de lui-même

```haskell
data List = Nil | Cons Int List

toHaskell :: List -> [Int]
toHaskell Nil = []
toHaskell (Cons x xs) = x : toHaskell xs

main :: IO ()
main = print $ toHaskell (Cons 13 (Cons 37 Nil))
```


## Synonyme de type

- nouveau nom

- mais même sémantique

```haskell
type List = [Int]

concatList :: List -> List -> List
concatList xs ys = xs ++ ys

main :: IO ()
main = print $ concatList [13, 37] [42]
```


## Wrapper de type

- type avec une seule valeur

- permet de définir une nouvelle sémantique

```haskell
newtype List = List [Int]

concatList :: List -> List -> List
concatList (List xs) (List ys) = List (xs ++ ys)

toHaskell :: List -> [Int]
toHaskell (List xs) = xs

main :: IO ()
main = print $ toHaskell $ concatList (List [13, 37]) (List [42])
```


# Exemples de types algébriques

## Maybe

```haskell
data Maybe a = Nothing | Just a 
```

```haskell
safeSqrt :: Double -> Maybe Double
safeSqrt x = 
    if x >= 0 then Just (sqrt x) else Nothing

main = do
    print $ safeSqrt (-1)
    print $ safeSqrt 1764
```

```haskell
Nothing
Just 42.0
```

## Either

```haskell
data Either a b = Left a | Right b
```

```haskell
safeSqrt :: Double -> Either String Double
safeSqrt x = 
    if x >= 0 then Right (sqrt x) else Left "invalid number" 

main = do
    print $ safeSqrt (-1)
    print $ safeSqrt 1764
```

```haskell
Left "invalid number"
Right 42.0
```




# Travaux pratiques

Travaillez dans le dossier `tp-adt` de votre dépôt git.


## Jours de la semaine

- Dans le fichier `jours.hs`, écrivez un type `Jour` représentant les jours de
  la semaine.

- Écrivez une fonction `estWeekend` qui retourne si un `Jour` est un jour du
  week-end.

- Écrivez une fonction `compterOuvrables` qui retourne le nombre de jours
  ouvrables (c'est-à-dire pas du week-end), d'une liste.


## User

- Dans le fichier `user.hs`, écrivez un type enregistrement `User` (nom,
  prénom, age).

- Écrivez une fonction `showUser` permettant "d'afficher" un `User`, dans une
  `String`.

- Écrivez une fonction `incAge` permettant "d'incrémenter" l'age d'un `User`.


## Liste

- Dans le fichier `list.hs`, écrivez un type `List` implémentant une liste
  d'entiers.

- Écrivez une fonction `sumList`, qui calcule la somme d'une `List` d'entiers.

- Modifiez votre code de façon à rendre votre liste générique.

- Écrivez une fonction `flatList`, qui "aplatit" une `List` de `String`. 

* * *

- Écrivez une fonction `toHaskell`, qui transforme une `List` en liste Haskell. 

- Écrivez une fonction `fromHaskell`, qui transforme une liste Haskell en
  `List`.

- Écrivez une fonction `myShowList`, qui "affiche" une `List` dans une `String`
  (en séparant les éléments par un espace).


## Arbre binaire de recherche

Un arbre binaire de recherche est un arbre binaire où, pour chaque nœud, la
valeur est strictement supérieure aux valeurs du sous-arbre gauche et
inférieure aux valeurs du sous-arbre droit.

![](dot/pfa-tp-abr.svg){width="40%"}

* * *

- Dans le fichier `abr.hs`, écrivez un type `Abr` représentant un arbre binaire
  de recherche d'entiers (c'est-à-dire une feuille sans valeur ou un noeud).

- Écrivez une fonction `insererAbr`, qui ajoute un élément dans un `Abr`.

- Écrivez une fonction `listToAbr`, qui transforme une liste en `Abr`.

- Écrivez une fonction `abrToList`, qui transforme un `Abr` en liste.

- Modifiez votre code de façon à rendre votre `Abr` générique.


## SpinusBob

Pour cet exercice, travaillez dans un `nix-shell`, dans le répertoire `tp-adt`.

- Exécutez le programme `hello-gloss.hs`.

- Écrivez un programme `spinusbob.hs` qui fait bouger SpongeBob selon un sinus.
  Les coefficients doivent pouvoir être réglés avec les flêches du clavier.
  Indication : ajoutez quelques coefficients dans le calcul de position, le
  repère est centré et en pixels (cf la 
  [doc de Gloss](https://hackage.haskell.org/package/gloss)).

* * *

<video preload="metadata" controls>
<source src="files/pfa-tp-spinusbob.mp4" type="video/mp4" />
![](files/pfa-tp-spinusbob.png){width="40%"}

</video>

## Tictactoe graphique

Pour cet exercice, travaillez dans un `nix-shell`, dans le répertoire `tp-adt`.

- En reprenant votre tictactoe du tp-rappels1, écrivez un programme `tictactoe-gui.hs`
  implémentant un tictactoe graphique entre Bob et Gary. Un clic gauche 
  joue le coup courant, un clic droit recommence le jeu.

* * *

<video preload="metadata" controls>
<source src="files/pfa-tp-tictactoe-gui.mp4" type="video/mp4" />
![](files/pfa-tp-tictactoe-gui.png){width="40%"}

</video>

* * *

- Modifiez votre code de façon à utiliser un type enregistrement pour
  `Tictactoe`.

