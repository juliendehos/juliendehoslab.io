---
title: API web
date: 2021-08-23
---

# Généralités

## API web

- permet d'implémenter des services web (REST, SOAP...)

- requête (HTTP)

- réponse (JSON, XML)

- notion de route/url/endpoint


## Servant

- libs Haskell pour implémenter des applis web :

    - serveur
    - clients (Haskell, JavaScript)
    - documentation
    - description Swagger...

- intérêts : type-safe, conversion ADT-JSON...

- [doc Servant](https://docs.servant.dev/en/stable/)


## Définir une API

- importer Servant et activer quelques extensions

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module MathApi where

import Servant 
```

* * *

- définir chaque route par un type

```haskell
type Math42 = "quarante-deux" :> Get '[JSON] Int

type MathMul2 = "mul2" :> Capture "x" Int :> Get '[JSON] Int
```


## Serveur

- importer l'API (+ Servant et extensions) :

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Network.Wai.Handler.Warp (run)
import Servant 

import MathApi
```

* * *

- définir l'API à servir (type + handlers) :

```haskell
type ServerApi
    =    Math42
    :<|> MathMul2

handleServerApi :: Server ServerApi
handleServerApi
    =    (return 42)
    :<|> handleMathMul2

handleMathMul2 :: Int -> Handler Int
handleMathMul2 x = return (x*2)
```

* * *

- lancer l'application serveur :

```haskell
serverApp :: Application
serverApp = serve (Proxy @ServerApi) handleServerApi

main :: IO ()
main = run 3000 serverApp
```

* * *

- exécuter le programme et tester avec `curl` :

```text
$ curl "localhost:3000/quarante-deux"
42

$ curl "localhost:3000/mul2/3"
6
```

## Client

- importer l'API :

```haskell
{-# LANGUAGE TypeApplications #-}

import Data.Proxy (Proxy(..))
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.Client 

import MathApi
```

* * *

- définir des requêtes, via l'API :

```haskell
query42 :: ClientM Int
query42 = client (Proxy @Math42)

queryMul2 :: Int -> ClientM Int
queryMul2 = client (Proxy @MathMul2)
```

* * *

- exécuter les requêtes :

```haskell
main :: IO ()
main = do
    myManager <- newManager defaultManagerSettings
    let myClient = mkClientEnv myManager
                        (BaseUrl Http "localhost" 3000 "")
    runClientM query42 myClient >>= print
    runClientM (queryMul2 12) myClient >>= print
```

* * *

- tester :

```text
$ runghc client.hs 
Right 42
Right 24
```


## Isomorphic web app

- "appli web exécutable à la fois par le serveur et par le client" :

    - au début, le serveur envoie l'appli et génère les premières pages
    - quand l'appli est chargée chez le client, elle prend le relais
    - l'appli contacte le serveur quand c'est nécessaire (SPA classique)

* * *

- réalisable en Haskell avec Miso + Servant

- intérêts : type-safe, routes serveur/client/communes

- concrètement :

    - code commun : routes communes+client, modèle, vue
    - code client : gestion des événements locaux
    - code serveur : API serveur, server-side rendering (SSR)

- (mais on n'en fera pas en TP)


# Travaux pratiques

## Client github

> Attention, Github limite le nombre de requêtes par IP. Pensez à vérifier vos
> requêtes avant de les exécuter.
> 

* * *

- Testez l'[API Github](https://developer.github.com/v3/) avec `curl` :

```text
$ curl "https://api.github.com/users/juliendehos"
{
  "login": "juliendehos",
  "id": 11947756,
...
```

* * *

- Dans `tp-api/github`, regardez comment `Repo` et `User` sont définis et faites
  le lien avec votre requête HTTP précédente.

- Testez et complétez `client-custom.hs`.

- Testez et complétez `client-servant.hs` et `GithubApi.hs`.

* * *

```text
$ runghc -Wall client-servant.hs 

getUser:
Right (Just (User {id = 11947756, login = "juliendehos"}))

getRepo:
Right (Repo {id = 137743929, name = "nixpkgs", full_name = "juliendehos/nixpkgs", owner = User {id = 11947756, login = "juliendehos"}})

getUserRepos:
Repo {id = 191574311, name = "clay", full_name = "juliendehos/clay", owner = User {id = 11947756, login = "juliendehos"}}
Repo {id = 241107854, name = "DL-AAN", full_name = "juliendehos/DL-AAN", owner = User {id = 11947756, login = "juliendehos"}}
Repo {id = 244645747, name = "fast-cma-es", full_name = "juliendehos/fast-cma-es", owner = User {id = 11947756, login = "juliendehos"}}
```


## Math

- Dans `tp-api/math`, regardez le code fourni, lancez le serveur, testez-le
  avec `curl` puis avec le client.

- On veut implémenter une route `mul2`, qui multiplie un entier par 2, et une
  route `add`, qui ajoute deux entiers. Implémentez les types et le serveur
correspondants. Testez avec `curl`.

- Dans le client, implémentez deux requêtes pour tester ces routes, ainsi
  qu'une requête qui accède aux deux routes et retourne les résultats dans un
tuple.


## Music

- Dans `tp-api/music`, complétez les fichiers `server-scotty.hs` et `Music.hs`
  de façon à implémenter un serveur web qui fournit une API JSON (une route
  `/api` qui affiche toutes les infos et une route `/api/artist` qui affiche
  les infos d'un artiste donné) et une page HTML (route `/`). 

* * *

<video preload="metadata" controls>
<source src="files/pfa-api-music.mp4" type="video/mp4" />
![](files/pfa-api-music.png){width="80%"}

</video>

* * *

- Implémentez un serveur équivalent mais avec la bibliothèque Servant. Pour
  cela, complétez les fichiers `Route.hs` et `server-servant.hs`. Utilisez les
  fontionnalités de Servant pour générer les URI vers votre API, dans la page
  HTML. 

* * *

- Implémentez un client en utilisant Servant. Testez avec vos deux serveurs
  (Servant et Scotty).

```html
$ runghc client-servant.hs 

queryAll:
Music {_title = "Paranoid android", _artist = "Radiohead", _year = 1997}
Music {_title = "Just", _artist = "Radiohead", _year = 1995}
Music {_title = "Take the power back", _artist = "Rage against the machine", _year = 1991}
Music {_title = "How I could just kill a man", _artist = "Rage against the machine", _year = 2000}
Music {_title = "La porte bonheur", _artist = "Ibrahim Maalouf", _year = 2014}

queryArtist:
Music {_title = "Paranoid android", _artist = "Radiohead", _year = 1997}
Music {_title = "Just", _artist = "Radiohead", _year = 1995}
```

