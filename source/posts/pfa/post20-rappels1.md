---
title: Rappels (expressions, variables, fonctions, récursivité)
date: 2021-08-23
---

# Rappels de cours

## Programmation fonctionnelle

- idée de base : programmer avec des fonctions (pures)

- notions : 
    - expressions
    - variables, fonctions
    - fonctions récursives
    - fonctions d'ordres supérieur...

- intérêts :
    - expressivité
    - pas d'effet de bord...


## Haskell

- langage fonctionnel (pur)

- système de types (types algébriques, classes de types...)

- évaluation non-stricte

- utilisation : recherche, enseignement, industrie

- compilateur (ghc, runghc), interpréteur (ghci)


## Expression

- morceau de code évaluable et typé

- propriété de transparence référentielle : remplacer une expression par sa
  valeur ne change pas le programme

```haskell
Prelude> "foo" ++ "bar"
"foobar"

Prelude> :type "foo" ++ "bar"
"foo" ++ "bar" :: [Char]
```

## Variable (au sens de la PF pure)

- nom donné à une expression

- doit commencer par une minuscule

```haskell
Prelude> str = "foo" ++ "bar"

Prelude> str
"foobar"

Prelude> :type str
str :: [Char]
```

* * *

- variable locale avec `where` ou `let-in`

```haskell
foobar :: String 
foobar = x0 ++ x1
    where x0 = "foo"
          x1 = "bar"

boofar :: String 
boofar = 
    let x0 = "boo"
        x1 = "far"
    in x0 ++ x1
```


## Fonction

- variable avec des paramètres

- définition, évaluation

```haskell
Prelude> fooPrefix s = "foo" ++ s

Prelude> fooPrefix "bar"
"foobar"

Prelude> :type fooPrefix 
fooPrefix :: [Char] -> [Char]
```


## Types élémentaires

- `Bool` : `True`, `False`
- `Char` : `'a'`, `'b'`
- `String` ou `[Char]` : 
    - `"foo"` 
    - `['f','o','o']` 
    - `'f':'o':'o':[]`
- `Int`, `Integer` : `42`, `(-37)`, `91832498723409832709`
- `Double`, `Float` : `42`, `1.3`

## Types composés

- liste :
    - éléments de même type mais en nombre indéfini
    - définie avec `:` et `[]` (par exemple, `1:2:3:[]`)
    - raccourcis de syntaxe (`[1,2,3]`, `[1..3]`)

```haskell
Prelude> ['f','o','o']
"foo"

Prelude> :type ['f','o','o']
['f','o','o'] :: [Char]

Prelude> :type "foo"
"foo" :: [Char]
```

* * *

- tuple ou n-uplet :
    - éléments de type possiblement différents mais en nombre prédéfini
    - `("foo", 42, True)`

```haskell
Prelude> ("bar", False)
("bar",False)

Prelude> :type ("bar", False)
("bar", False) :: ([Char], Bool)
```


## Pattern matching (au sens large)

- `if-then-else` : définit une expression selon deux cas possibles

```haskell
main = putStrLn $ if 21*2 < 37 then "erreur" else "un"
```

* * *

- `case-of` : définit une expression selon $n$ cas possibles

```haskell
main = putStrLn $ case 21 * 2 of
                    42 -> "un"
                    37 -> "erreur"
                    _ -> "erreur aussi"
```

* * *

- gardes : définit une fonction (ou une variable) selon des conditions booléennes

```haskell
parite :: Int -> String
parite x
    | x == 0 = "zero"
    | x `mod` 2 == 0 = "pair"
    | otherwise = "impair"
```

* * *

- pattern matching : définit une fonction selon des valeurs

```haskell
nulStr :: Int -> String
nulStr 0 = "nul"
nulStr _ = "non nul" 
```

* * *

- mais en fait, c'est plus général que ça :

```haskell
zarbi :: [Int] -> Bool
zarbi [] = False
zarbi [x]
    | x < 0 = True
    | otherwise = False
zarbi (x1:x2:xs) = x1 == x2
```

```haskell
mylist = [13, 37, 42]
(x:xs) = mylist 
[y1,y2,y3] = mylist

mytuple = ("foo", True)
(t1,t2) = mytuple 
```


## Récursivité

- une fonction récursive est une fonction qui s'appelle elle-même

- structure :
    - cas d'arrêt
    - appel récursif

```haskell
factorielle :: Int -> Int
factorielle 1 = 1
factorielle n = n * factorielle (n-1)

taille :: [a] -> Int
taille [] = 0
taille (x:xs) = 1 + taille xs
```

* * *

- récursivité terminale : 
    - l'appel récursif donne directement la valeur de retour
    - permet d'éviter d'empiler les appels récursifs
    - peut nécessiter une fonction auxiliaire

```haskell
factorielle :: Int -> Int
factorielle n = go n 1
    where go 1 acc = acc
          go i acc = go (i-1) (acc*i)

taille :: [a] -> Int
taille l = go l 0
    where go [] acc = acc
          go (x:xs) acc = go xs (acc+1)
```

* * *

- notion de variable récursive :

```haskell
Prelude> x = 'x':x

Prelude> take 3 x
"xxx"

Prelude> take 12 x
"xxxxxxxxxxxx"
```

## Entrées/sorties

- effets de bord

- en Haskell, avec la notation `do` :
    - "contexte" `IO`
    - "actions" `IO` qui peuvent retourner une valeur
    - `<-` pour extraire la valeur d'une action `IO`
    - `let` pour définir une valeur pure dans un contexte `IO`
    - `return` pour construire une action `IO` contenant une valeur pure

- actions `IO` de base : `print` `putStrLn` `getLine` `getArgs` `readFile`...

* * *

```haskell
saisir :: String -> IO Int
saisir msg = do
    putStrLn msg
    saisie <- getLine
    return $ length saisie

main :: IO ()
main = do
    t1 <- saisir "saisie 1 ?"
    t2 <- saisir "saisie 2 ?"
    let str1 = show t1
        str2 = show t2
    putStrLn $ "vous avez saisi " ++ str1
                ++ " puis " ++ str2 ++ " caractères"
```



# Travaux pratiques

Travaillez dans le dossier `tp-rappels1` de votre dépôt git.


## Rappels sur GHC

1. Dans le fichier `mul2.hs`, écrivez :
    1. une fonction `mul2 :: Int -> Int` qui multiplie son paramètre par $2$
    1. un `main` qui affiche le résultat de `mul` sur $42$ et sur $-42$

2. Exécutez votre fichier avec `runghc`.

3. Compilez votre fichier avec `ghc` et exécutez le binaire obtenu.

* * *

4. Lancez `ghci` et :
    1. chargez `mul2.hs`
    1. exécutez le `main`
    1. appelez la fonction `mul2` sur différentes valeurs
    1. affichez le type de `mul2`


## Fonctions de seuillage

- Dans le fichier `seuil.hs`, écrivez une fonction `seuilInt` qui seuille un
  entier entre deux bornes :

```haskell
*Main> seuilInt 1 10 0
1

*Main> seuilInt 1 10 2
2

*Main> seuilInt 1 10 42
10
```

- Écrivez une fonction `seuilTuple` équivalente à `seuilInt` mais où les bornes
  sont données dans un tuple.


## Fizzbuzz

- Dans le ficher `fizzbuzz.hs`, écrivez une fonction `fizzbuzz1 :: [Int] ->
  [String]` récursive non-terminale qui implémente un fizzbuzz.

```haskell
*Main> fizzbuzz1 [1..15]
["1","2","fizz","4","buzz","fizz","7","8","fizz"
,"buzz","11","fizz","13","14","fizzbuzz"]
```

* * *

- Écrivez une fonction `fizzbuzz2 :: [Int] -> [String]` équivalente mais
  récursive terminale 

- Implémentez fizzbuzz avec une liste en compréhension.

```haskell
*Main> take 15 fizzbuzz
["1","2","fizz","4","buzz","fizz","7","8","fizz"
,"buzz","11","fizz","13","14","fizzbuzz"]
```

## Saisir 4 entiers

- Dans le fichier `saisir4entiers.hs`, écrivez un programme qui saisit 4
  entiers en vérifiant les saisies :

```sh
$ runghc saisir4entiers.hs
saisie 1 : 13
-> vous avez saisi l'entier 13
saisie 2 : 37
-> vous avez saisi l'entier 37
saisie 3 : 42
-> vous avez saisi l'entier 42
saisie 4 : toto
-> saisie invalide
```


## Tableau

- Dans le fichier `majTab.hs`, écrivez un programme qui permet de manipuler un
  tableau de 4 `Double` :

```sh
$ runghc majTab.hs 
array (1,4) [(1,0.0),(2,0.0),(3,0.0),(4,0.0)]
(2, 13.37)
array (1,4) [(1,0.0),(2,13.37),(3,0.0),(4,0.0)]
(1, 42)
array (1,4) [(1,42.0),(2,13.37),(3,0.0),(4,0.0)]
toto
array (1,4) [(1,42.0),(2,13.37),(3,0.0),(4,0.0)]
```


## Mini-projet : Tictactoe

- Dans le fichier `tictactoe.hs`, implémentez un jeu de Tictactoe :

```sh
...

..X
.X.
..O
O plays
> (3,2)

..X
.X.
.OO
X plays
> (4,2)
```

* * *

```sh
..X
.X.
.OO
X plays
> toto

..X
.X.
.OO
X plays
> (3,1)

..X
.X.
XOO
X wins
```

* * *


