---
title: Projets
date: 2021-08-23
---

# ulcoforum

*Scotty, BD*

Complétez le projet `projets/ulcoforum` de façon à implémenter un forum basique
avec :

- une page d'accueil
- une page affichant tous les messages
- une page affichant toutes les discussions et permettant d'en sélectionner une
- une page affichant la discussion sélectionnée et permettant d'y poster un message

* * *

<video preload="metadata" controls>
<source src="files/pfa-ulcoforum.mp4" type="video/mp4" />
![](files/pfa-ulcoforum.png){width="60%"}

</video>

* * *


Pour cela, écrivez un serveur web en Haskell et utilisez une base de données Sqlite.

S'il vous reste du temps, vous pouvez ajouter une page pour créer une nouvelle
discussion, implémenter l'horodatage des messages, gérer une authentification
des utilisateurs, etc.


# html2md

*Compilation*

On veut implémenter un programme capable de lire un sous-ensemble du HTML
(paragraphe, division, titre, texte/gras/italique, image, lien) et de
générer du Markdown et du HTML.

- Terminez le projet `projets/html2md` fourni : programme principal, parseur,
  génération de code, tests unitaires...

* * *

```text
$ cabal run html2md data/test4.html 
Up to date

-> RAW:
<div>
    <p><b>foo</b><i>faa</i></p>
    <div>
        <p>bar</p>
    </div>
</div>


-> AST:
[Div [Para [Strong [Str "foo"],Emph [Str "faa"]],Div [Para [Str "bar"]]]]

-> MD:
**foo***faa*

bar


-> HTML:
<div>
<p>
<b>foo</b><i>faa</i>
</p>
<div>
<p>
bar
</p>
</div>
</div>
```

# breakthrough

*Gloss*

[Breakthrough](https://en.wikipedia.org/wiki/Breakthrough_(board_game)) est un
jeu de plateau où chaque joueur avance alternativement un de ses pions.
L'objectif est de placer un pion de l'autre côté du plateau. Les mouvements
autorisés sont les déplacements (en avant ou en diagonal) et les captures de
pions adverses (en diagonal uniquement). 

* * *

- Complétez le projet `projets/breakthrough` : implémentation du jeu, interface
  texte et interface graphique.

- Si vous avez le temps, implémentez une IA.

* * *

<video preload="metadata" controls>
<source src="files/pfa-breakthrough.mp4" type="video/mp4" />
![](files/pfa-breakthrough.png){width="60%"}

</video>



# paletizer

*Traitement d'images, k-means*

On veut implémenter un programme de "palettisation d'images" en utilisant
[l'algorithme des k-moyennes](https://fr.wikipedia.org/wiki/K-moyennes).
Attention : il s'agit d'une segmentation de l'espace des couleurs et non des
régions de l'image. Pour cela, on propose de :

* * *

- lire l'image PNM d'entrée
- initialiser les valeurs des $k$ centroïdes en les répartisant sur l'axe des gris
- labelliser chaque pixel de l'image au centroïde le plus proche
- recalculer les centroïdes puis les labels, et recommencer tant que plus de 1% des pixels ont changé de label
- recoloriser chaque pixel en utilisant le centroïde correspondant
- enregistrer l'image PNM de sortie

* * *

```text
$ cabal run paletizer data/cliffs.pnm out.pnm 16

reading data/cliffs.pnm...
paletizing...
13 iterations
writing out.pnm...
```

![](files/pfa-paletizer.png) 


# tictactoe client-serveur

*WebSockets, STM*

On veut implémenter un jeu de
[Tictactoe](https://fr.wikipedia.org/wiki/Tic-tac-toe) en réseau : le serveur
attend que deux clients se connectent puis déroule des parties de jeu
successives entre eux. On veut un client natif (en Haskell) et un client web
(en HTML/CSS/JavaScript).

* * *

- Regardez le projet fourni (`projet/tictactoe`).
- Implémentez un client local (`tictactoe-local`), en mode texte où les joueurs
  entre successivement leurs coups au clavier.
- Implémentez le projet final (`tictactoe-server`, `tictactoe-client` et
  `web`).
- S'il vous reste du temps, implémentez des clients graphiques (web et/ou
  haskell).

* * *

<video preload="metadata" controls>
<source src="files/pfa-projet-tictactoe.mp4" type="video/mp4" />
![](files/pfa-projet-tictactoe.png){width="70%"}

</video>


# jeu temps-réel multi-utilisateur

*WebSockets, Gloss*

On veut implémenter une application de type jeu temps-réel multi-joueur. Chaque
joueur lance un client de l'application, où il peut réaliser des actions.
Chaque action est transmise au serveur, qui met à jour le jeu et retransmet à
tous les clients le nouvel état du jeu.

Un projet de base (`projets/game`) vous est fourni. Pour l'instant, chaque joueur
possède juste un texte, qu'il peut modifier. 

* * *

- Testez le projet et regardez le code.

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-game1.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-game1.png){width="70%"}

</video>

* * *

- Modifiez le code de telle sorte qu'un joueur possède désormais une position
  2D au lieu d'un texte. Un joueur doit pouvoir se déplacer avec des actions
  `MoveUp`, `MoveDown`, `MoveLeft` et `MoveRight`. Indications : 

    - écrivez une énumération représentant les actions possibles.
    - un `data` qui dérive des classes `Show` et `Read` peut ensuite être
      converti en `String` (avec les fonctions `show` et `read`, par exemple).

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-game2.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-game2.png){width="70%"}

</video>

* * *

- Ajoutez une interface graphique qui affiche les joueurs sous forme de carrés
  et permet à chaque joueur de se déplacer avec les flêches du clavier.
  Vous pouvez avoir besoin des fonctions
  [exitSucess](https://hackage.haskell.org/package/base/docs/System-Exit.html#v:exitSuccess)
  et
  [playIO](http://hackage.haskell.org/package/gloss/docs/Graphics-Gloss-Interface-IO-Game.html#v:playIO).

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-game.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-game.png){width="70%"}

</video>


* * *

- S'il vous reste du temps, vous pouvez :

    - implémenter un jeu plus intéressant (par exemple, avec des cibles à atteindre)
    - déployer le serveur sur Heroku et testez votre jeu "en vrai" sur Internet
    - coder un projet équivalent en C++ "pour comparer"


# baltig

*Servant, Miso*

On veut implémenter une appli web permettant de récupérer des informations de
Gitlab (nom d'un utilisateur, liste de ses projets...), via l'[API
Gitlab](https://docs.gitlab.com/ee/api/api_resources.html#project-resources).
Notre appli doit également proposer une page d'accueil, avec des news fournies
via notre propre API.

* * *

<video preload="metadata" controls>
<source src="files/pfa-baltig.mp4" type="video/mp4" />
![](files/pfa-baltig.png){width="60%"}

</video>

* * *

Le code fourni est une "Isomorphic-Web-App" basique, codée en Haskell avec Miso
et Servant. Par rapport aux TPs, cette application comporte plusieurs pages
("home" et "gitlab"); ceci est codée via une API cliente. On a donc 3
API à manipuler : l'API Gitlab, notre API client (page "home", page "gitlab")
et notre API serveur (news, fichiers statiques).

* * *

Implémentez les fonctionnalités suivantes, dans l'ordre que vous voulez :

- lire les news dans la base de données et les servir via une API
- dans la page "home", récupérer les news (via l'API) et les afficher
- dans la page "gitlab", récupérer les informations et projets d'un utilisateur
  (via l'API Gitlab), et les afficher
- dans la page "gitlab", pouvoir saisir le username gitlab à utiliser
- typer toutes les API utilisées


* * *

Indications :

- testez les API avec curl (par exemple, `curl "localhost:3000/news"`)
- au final, toutes les API doivent être typées mais dans un premier temps, vous
  pouvez utiliser des URL "en dur"
- consultez les docs (Miso, Servant, Hackage, [API
  Gitlab](https://docs.gitlab.com/ee/api/api_resources.html#project-resources)...)


