---
title: Génération de code
date: 2021-08-23
---


# Présentation

## Rappels de compilation

- le parsing permet de "comprendre" du code source

- la génération de code produit du code cible, à partir de cette
  "compréhension"

- outils de compilation classiques : LLVM...


## À partir d'un ADT

- facile : fonction qui prend une valeur du type et retourne du texte ou un
  flux binaire, dans le format cible

- si on a un type enregistrement, on peut sortir automatiquement dans un format
  clé-valeur (JSON, YAML...) 

- exemple avec `aeson` :

```haskell
> data Person = Person {name :: Text, age :: Int}

> encode (Person {name = "Joe", age = 12})
{ "name": "Joe", "age": 12 }
```


## À partir d'un AST

- implémenté par un ADT en Haskell

- structure arborescence

- élément central dans la chaine de compilation

- exemple avec `aeson` (voir aussi [json.org](https://www.json.org/json-fr.html)) :

```haskell
data Value = Object !Object
           | Array !Array
           | String !Text
           | Number !Scientific
           | Bool !Bool
           | Null
             deriving (Eq, Read, Show, Typeable, Data, Generic)
...
```


## Avec un DSL

- "langage dédié" (Domain Specific Language)

- idée : implémenter un format de fichier ou un langage métier dans le langage hôte

- par exemple, `lucid` est un DSL du HTML en Haskell (la "fonction haskell"
  `h1_` représente la balise HTML `<h1>...</h1>`, etc)

* * *

- avantages :

     - on a déjà les fonctionnalités du langage hôte (syntaxe, conditions, répétitions...)
     - le compilateur du langage hôte vérifie automatiquement le DSL

- on peut voir ça comme "l'inverse des templates" : au lieu de "mettre du
  code" dans un fichier HTML, on génère complètement le HTML depuis le code


# Travaux pratiques

Pour les exercices suivants, allez dans le dossier `tp-codegen/exos` de votre dépôt git,
lancez un `nix-shell` et exécutez vos programmes avec des `runghc -Wall`.


## ADT (aeson/yaml)

Le fichier `aeson1.hs` définit un ADT et une variable l'utilisant.

- Modifiez le fichier de façon à exporter les données vers un fichier JSON.
  Pour cela, utilisez `Generic`.

```sh
$ runghc -Wall aeson1.hs 

$ cat out-aeson1.json 
[{"lastname":"Doe","birthyear":1970,"firstname":"John"}
,{"lastname":"Curry","birthyear":1900,"firstname":"Haskell"}]
```

* * *

- Écrivez un fichier `aeson2.hs` équivalent à `aeson1.hs` mais avec un export
  personnalisé. Pour cela, utilisez `toJSON` ou `toEncoding`.

```sh
$ runghc -Wall aeson2.hs 

$ cat out-aeson2.json 
[{"first":"John","birth":"1970","last":"Doe"}
,{"first":"Haskell","birth":"1900","last":"Curry"}]
```

* * *

- En vous inspirant de `aeson1.hs`, écrivez un fichier `aeson3.hs` utilisant
  deux ADT.

![](files/pfa-tp-tojson.png)

* * *

- Idem mais pour exporter un fichier YAML.

```sh
$ runghc -Wall yaml3.hs 

$ cat out-yaml3.yaml 
- lastname: Doe
  address:
    road: Pont Vieux
    zipcode: 43000
    city: Espaly
    number: 42
  birthyear: 1970
  firstname: John
- lastname: Curry
  address:
    road: Pere Lachaise
    zipcode: 75000
    city: Paris
    number: 1337
  birthyear: 1900
  firstname: Haskell
```

## AST (aeson/yaml)

- En utilisant l'AST de `aeson` (cf l'ADT `Value`), créez les valeurs suivantes
  et affichez-les en JSON :

    - un texte
    - un nombre
    - un objet à un champ
    - un objet à trois champs

```sh
$ runghc -Wall ast-aeson1.hs 
"John"
21
{"birthyear":1970}
{"lastname":"Doe","birthyear":1970,"firstname":"John"}
```

* * *

- Idem en YAML.

```sh
$ runghc -Wall ast-yaml1.hs 
John

21

birthyear: 1970

lastname: Doe
birthyear: 1970
firstname: John
```


## DSL (lucid)

- Écrivez un fichier `lucid1.hs` qui génère et affiche du code HTML en
  utilisant `lucid`.

```sh
$ runghc -Wall lucid1.hs 
<h1>hello</h1><p>world</p>
```

* * *

- Écrivez un fichier `lucid2.hs` équivalent mais avec du HTML plus complexe et
  vers un fichier.

```sh
$ runghc -Wall lucid2.hs 

$ cat out-lucid2.html 
<!DOCTYPE HTML><html><head><meta charset="utf-8"></head>
<body><h1>hello</h1><img src="toto.png">
<p>this is <a href="toto.png">a link</a></p></body></html>
```


## Mini-projet : genalbum

On veut implémenter une application qui :

- lit le fichier `data/genalbum.json` 
- en génère une page HTML synthétisant les images de chaque site
- et des pages HTML implémentant un slideshow des images.

* * *

<video preload="metadata" controls>
<source src="files/pfa-genalbum.mp4" type="video/mp4" />
![](files/pfa-genalbum.png){width="80%"}

</video>

* * *

Indications :

- Complétez le projet, déjà fourni, `tp-codegen/genalbum`.
- Implémentez la lecture du fichier JSON.
- Implémentez la page de synthèse.
- Implémentez les pages de slideshow sans les liens entre les pages.
- Implémentez les liens entre les pages de slideshow.


## AST (pandoc)

- Dans le dossier `tp-codegen/exos`, écrivez un fichier `pandoc1.hs` qui parse
  le fichier `pandoc-test1.md` et affiche son AST.  Indication : regardez
l'[introduction à l'API pandoc](https://pandoc.org/using-the-pandoc-api.html).

* * *

- Écrivez un fichier `pandoc2.hs` qui construit directement un AST équivalent
  et qui l'affiche en Markdown. Ajoutez ensuite quelques éléments dans cet AST.
  Indication : regardez les doc de `pandoc` et de `pandoc-types`.

```sh
$ runghc -Wall pandoc2.hs 
hello
=====

my text 1

world
=====

my text 2
```

* * *

- Écrivez un fichier `pandoc3.hs` équivalent mais qui affiche en HTML.

```sh
$ runghc -Wall pandoc3.hs 
<h1>hello</h1>
my text 1
<h1>world</h1>
my text 2
```

