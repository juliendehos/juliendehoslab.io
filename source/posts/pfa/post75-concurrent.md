---
title: Programmation concurrente (ou pas)
date: 2021-08-23
---

# Généralités

## Rappels

- asynchrone : "2 sections de code qui s'exécutent indépendamment"

- parallèle : "les 2 sections de code peuvent s'exécuter en même temps (par
  exemple sur des coeurs différents)"

- concurrent : "les 2 sections de code peuvent potentiellement accéder à une
  même donnée en même temps"


## Threads légers

- threads systèmes : gérés par l'OS (threads POSIX par exemple)

- threads légers : gérés par le runtime du langage

- en Haskell : module
  [Control.Concurrent](https://hackage.haskell.org/package/base/docs/Control-Concurrent.html),
  `forkIO`, etc


## Variables mutables

- permet de gérer des données modifiables avec accès concurrents (ou pas)

- différents outils disponibles en Haskell :

     - [IORef](https://hackage.haskell.org/package/base/docs/Data-IORef.html) : références mutables basiques

     - [MVar](https://hackage.haskell.org/package/base/docs/Control-Concurrent-MVar.html) : idem + variables synchronisées, file à accès concurrent...

     - [STM](https://hackage.haskell.org/package/stm) : Software Transactional Memory, modulaire/composable


## Websockets

- implémentation des WebSockets en Haskell : protocole, client, serveur (cf
  [doc](https://hackage.haskell.org/package/websockets))

- le serveur permet de gérer les clients sans manipuler explicitement des
  threads

- mais il est parfois nécessaire d'utiliser explicitement des threads,
  mutables, etc

- utilisable aussi avec des serveurs plus performants, par exemple via
  [wai-websockets](https://hackage.haskell.org/package/wai-websockets)


## Gloss

- bibliothèque graphique simple : affichage, animation, interaction (cf
  [doc](https://hackage.haskell.org/package/gloss))

- donc potentiellement avec des accès concurrents (par exemple, animation +
  événement clavier)

- mais qui peuvent être gérés par Gloss : architecture principale + fonctions
  de rappel

- fonctionnement assez classique : Elm, Redux, Gtk, Glut...

* * *

![](files/pfa-concurrent-gloss.png)


# Travaux pratiques

## game-local

Le projet `tp-concurrent/game-local` implémente un début de jeu (un "joueur",
avec une position 2D). 

- Complétez la fonction `playMove` de façon à déplacer le joueur.

- Implémentez une interface, avec Gloss, permettant d'afficher le joueur et de
  le controller avec les flêches du clavier.

* * *

<video preload="metadata" controls>
<source src="files/pfa-concurrent-game-local.mp4" type="video/mp4" />
![](files/pfa-concurrent-game-local.png){width="60%"}

</video>


## random-delay-mt

- Complétez le programme `tp-concurrent/random-delay-mt` de façon à avoir 3
  threads qui, en boucle, affichent un message et attendent une durée
  aléatoire (de 1 à 10 secondes). Indications : `forkIO`, `forever`,
  `randomRIO`.

```sh
$ runghc Main.hs 
thread 0 is waitintgth hr3re.ea8ad4d   1s2 . i.is.s 
 wwaaiittiinngg  97..9938  ss......

thread 0 is waiting 4.66 s...
thread 2 is waiting 7.87 s...
thread 0 is waiting 6.97 s...
thread 1 is waiting 5.98 s...
```

## ws-clock (1)

On veut implémenter un programme client-serveur en WebSockets où le serveur
envoie l'heure toutes les 2s (indépendamment pour chaque client).

- Terminez l'implémentation du projet `tp-concurrent/ws-clock`.

- Vérifiez que votre serveur fonctionne également avec le client web
  `client.html`.

* * *

<video preload="metadata" controls>
<source src="files/pfa-concurrent-ws-clock.mp4" type="video/mp4" />
![](files/pfa-concurrent-ws-clock.png){width="60%"}

</video>


## ws-clock (2)

On veut maintenant synchroniser, pour tous les clients, l'heure fournie par le
serveur. Pour cela, on peut stocker les clients dans une variable mutable et
lancer, dans un thread, une fonction qui envoie régulièrement l'heure à tous
les clients.

- Copiez le fichier `server.hs` dans un nouveau fichier `server-ioref.hs`.

- Ajoutez le type `Model` suivant et modifiez votre code de façon à implémenter
  les fonctionnalités demandées (la fonction `sendClock` doit envoyer l'heure
courante à tous les clients). 

* * *

```haskell
data Model = Model
    { _clients :: [(Int, WS.Connection)]
    , _nextId :: Int
    }

newModel :: Model
newModel = Model [] 0

sendClock :: IORef Model -> IO ()
...
```

* * *

<video preload="metadata" controls>
<source src="files/pfa-concurrent-ws-clock-mvar.mp4" type="video/mp4" />
![](files/pfa-concurrent-ws-clock-mvar.png){width="60%"}

</video>


## ws-clock (3)

- Copiez votre fichier `server-ioref.hs` dans un nouveau fichier
  `server-stm.hs` et modifiez ce dernier de façon à utiliser le type `TVar` de
la bibliothèque STM au lieu de `IORef`.

- Vérifiez que le client web fonctionne toujours avec votre serveur.


## echo-servant

On veut implémenter un système d'echo (client-serveur WebSockets), avec une
page publique qui récapitule les messages (API JSON et page HTML). 

- Dans l'application HTTP, ajoutez une route "messages" qui
  retourne la liste des messages en JSON.

- Dans le rendu HTML, ajoutez un lien vers la route "messages" (en utilisant
  `safeLink`) ainsi que la liste des messages.

- Implémentez le client-serveur d'echo, en utilisant les WebSockets.

* * *

<video preload="metadata" controls>
<source src="files/pfa-concurrent-echo-servant.mp4" type="video/mp4" />
![](files/pfa-concurrent-echo-servant.png){width="60%"}

</video>

