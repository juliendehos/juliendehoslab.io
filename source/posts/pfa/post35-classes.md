---
title: Classes de types
date: 2021-08-23
---

# Classes de types

## Intuition

- classe de type = ensemble de signatures de fonctions (= interface Java)

- instance : type implémentant ces fonctions


## Classes de types standards

- correspondances types/classes standards :

|           | `Eq` | `Ord` | `Num` | `Show` | `Read` | `Enum` |  autres      |
|-----------|:----:|:-----:|:-----:|:------:|:------:|:------:|:------------:|
| `Bool`    |  X   |  X    |       |  X     |  X     |  X     |              |
| `Char`    |  X   |  X    |       |  X     |  X     |  X     |              |
| `String`  |  X   |  X    |       |  X     |  X     |        |              |
| `Int`     |  X   |  X    |  X    |  X     |  X     |  X     | `Integral`   |
| `Integer` |  X   |  X    |  X    |  X     |  X     |  X     | `Integral`   |
| `Float`   |  X   |  X    |  X    |  X     |  X     |  X     | `Fractional` |
| `Double`  |  X   |  X    |  X    |  X     |  X     |  X     | `Fractional` |

* * *

- info sur une classe :

```haskell
Prelude> :info Num 
class Num a where
  (+) :: a -> a -> a
  (-) :: a -> a -> a
  (*) :: a -> a -> a
  negate :: a -> a
  abs :: a -> a
  signum :: a -> a
  fromInteger :: Integer -> a
instance Num Word
instance Num Integer
instance Num Int
instance Num Float
instance Num Double
```


## Contraintes de types

```haskell
data Rectangle a = MkRectangle a a

getArea :: Num a => Rectangle a -> a
getArea (MkRectangle w h) = w*h

main = do
    print $ getArea $ MkRectangle (21::Int) 2
    print $ getArea $ MkRectangle (21::Double) 2
```

```haskell
42
42.0
```

## Définir une classe de types

```haskell
class Show a where
  show :: a -> String
```


## Instancier une classe de types

```haskell
data Rectangle = MkRectangle Double Double

instance Show Rectangle where
  -- show :: Rectangle -> String
  show (MkRectangle w h) = "MkRectangle " ++ show w ++ " " ++ show h

main = putStrLn $ show $ MkRectangle 4 2
-- or: main = print $ MkRectangle 4 2
```

```haskell
MkRectangle 4.0 2.0
```

## Instancier une classe avec contraintes 

```haskell
data Rectangle a = MkRectangle a a

instance Show a => Show (Rectangle a) where
  -- show :: Show a => Rectangle a -> String
  show (MkRectangle w h) = "MkRectangle " ++ show w ++ " " ++ show h
```

```haskell
MkRectangle 4 2
```

## Instancier une classe automatiquement

```haskell
data Rectangle a = MkRectangle a a 
    deriving (Show)
```

## Héritage entre classes de types

```haskell
class (Eq a) => Ord a  where
    compare              :: a -> a -> Ordering
    (<), (<=), (>=), (>) :: a -> a -> Bool
    max, min             :: a -> a -> a
```


# Exemples de classes de types

## La classe Monad

```haskell
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b    -- "bind"

    (>>) :: m a -> m b -> m b            -- "then"
    m >> k = m >>= \_ -> k
```

## La monade IO (exemple 1)

- avec les operateurs :

```hs
main :: IO ()
main = putStrLn "input?" >> getLine >>= putStrLn
```

- avec la notation `do` :

```hs
main :: IO ()
main = do
    putStrLn "input?"
    str <- getLine
    putStrLn str
```

## La monade IO (exemple 2)

```haskell
import Data.Char (toUpper)

getName :: IO String
getName = putStrLn "Name ?" >> getLine >>= (return . map toUpper)

main :: IO ()
main = getName >>= putStrLn . ("Hello "++)
```

```haskell
Name ?
Julien
Hello JULIEN
```

* * *

```haskell
import Data.Char (toUpper)

getName :: IO String
getName = do
    putStrLn "Name ?" 
    line <- getLine
    return $ map toUpper line

main :: IO ()
main = do
    name <- getName
    putStrLn $ "Hello " ++ name
```

## La classe Functor

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b

    (<$>) = fmap
```


## La classe Applicative

```haskell
class Functor f => Applicative f where
    pure :: a -> f a    -- equivalent to "return" in Monad
    (<*>) :: f (a -> b) -> f a -> f b       -- "ap"
```

## (Le foncteur Maybe)

```haskell
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

mul2 :: Double -> Double
mul2 = (*2)

safeMul2 :: Maybe Double -> Maybe Double
safeMul2 = fmap mul2

main :: IO ()
main = do
    print $ safeMul2 $ safeSqrt (-1)
    print $ safeMul2 $ safeSqrt 441
    print $ fmap mul2 $ safeSqrt (-1)
    print $ mul2 <$> safeSqrt 441
```

## (L'applicative Maybe)

```haskell
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

main = do
    print $ Just (*2) <*> safeSqrt 441 
    print $ (*2) <$> safeSqrt 441
    print $ (*) <$> safeSqrt (-1) <*> Just 2
```

```haskell
Just 42.0
Just 42.0
Nothing
```

## La monade Maybe

```haskell
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

safeMul2 :: Double -> Maybe Double
safeMul2 x = Just $ x*2

main :: IO ()
main = do
    print (safeSqrt 441 >>= safeMul2)
    print (safeMul2 882 >>= safeSqrt)
    print $ do
        s <- safeSqrt 441
        safeMul2 s
```

```haskell
Just 42.0
Just 42.0
Just 42.0
```

## Applicative vs Monad

```haskell
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

safeMul2 :: Double -> Maybe Double
safeMul2 = return . (*2)
```

* * *

```hs
main = do
    -- monad (notation do) :
    print $ do
        x <- safeMul2 11
        y <- safeSqrt 400
        return (x + y)

    -- monad (opérateurs) :
    print $ 
        safeMul2 11  >>= (\x -> 
        safeSqrt 400 >>= (\y ->
        return (x + y)))

    -- applicative :
    print $ (+) <$> safeMul2 11 <*> safeSqrt 400
```

## Théorie des catégories

- lois des Monad/Applicative/Functor

- Monad $\Rightarrow$ Applicative $\Rightarrow$ Functor

```haskell
Prelude> :info Monad
class Applicative m => Monad (m :: * -> *) where
  (>>=) :: m a -> (a -> m b) -> m b
  (>>) :: m a -> m b -> m b
  return :: a -> m a
  fail :: String -> m a
```


## Typeclassopedia

![[typeclassopedia](https://wiki.haskell.org/Typeclassopedia)](dot/pfa-typeclassopedia.svg){width="95%"}




# Travaux pratiques

Travaillez dans le dossier `tp-classes` de votre dépôt git.


## Contraintes de types

- Ouvrez le fichier `compute.hs` et vérifiez que le code fonctionne pour des
  entiers mais pas pour des doubles.

- Modifiez la fonction `kernel` de façon à la rendre la plus générique
  possible. Ne touchez pas à la fonction `compute` et vérifiez que le code
  fonctionne toujours pour des entiers mais toujours pas pour des doubles. 

- Modifiez la fonction `compute` de façon à la rendre la plus générique
  possible. Vérifiez que le code fonctionne pour des entiers et pour des
  doubles. 


## Either

On veut implémenter des fonctions de calculs qui peuvent échouer, avec message
d'erreur. Pour cela, on se propose d'utiliser un `Either`.

- Dans le fichier `either.hs`, implémentez les fonctions `mySqrt`, `myMul2` et
  `myNegate`.

- Implémentez le calcul $2 \times - \sqrt {16}$ dans `myCompute1`, en utilisant
  des `case-of`.

- Implémentez le même calcul dans `myCompute2`, en utilisant l'opérateur `>>=`.

- Implémentez le même calcul dans `myCompute3`, en utilisant la notation `do`.


## Instances de classes

On veut manipuler des arbres binaires. Le fichier `tree.hs` définit une
variable `mytree1` représentant l'arbre suivant.

![](dot/pfa-tp-tree.svg){width="50%"}

* * *

- Écrivez un type implémentant un arbre binaire d'entiers et dérivant de la
  classe `Show`. Exécutez le programme et vérifiez que l'arbre `mytree1`
  s'affiche.

- Remplacez la dérivation par une instanciation, de façon à obtenir l'affichage
  suivant :

```haskell
(7(2__)(37(13__)(42__)))
```

* * *

- Modifiez votre code de façon à avoir un arbre générique et non un arbre
  d'entiers.

- Testez avec un arbre d'entiers et un arbre de doubles :

```haskell
(7(2__)(37(13__)(42__)))
(7.0(2.0__)(37.0(13.0__)(42.0__)))
```

* * *

- En utilisant `ghci`, trouvez quelle classe définit les fonctions `sum` et
  `maximum`.

- En regardant la documentation correspondante, instancier cette classe pour
  votre type d'arbre.

- Affichez la somme et le maximum de `mytree1`.


## User

- Implémentez la fonction `parseUser`, qui construit un `User` à partir d'une
  `String`. Pour cela, on récupère les deux champs séparés par un `;` dans la
  `String`, ou on retourne un `Nothing`.

- Implémentez la fonction `upperize` qui met en majuscule le nom.

- Implémentez un `main` qui saisit une ligne au clavier, construit le `User`
  correspondant et met le nom en majuscule. Pour cela, utilisez la notation
  `do`.

- Implémentez un `main` équivalent mais utilisant uniquement les opérateurs à
  la place de la notation `do`.


## Monte-Carlo Pi

On veut implémenter l'approximation de $\pi$ par la méthode de Monte-Carlo.
Pour cela, on tire $N$ points aléatoires dans $[0,1]^2$, on compte le nombre 
$D$ de points $\mathbf x$ tels que $||\mathbf x|| < 1$ et on calcule :

$$\pi \approx 4 \times \frac{D}{N}$$

* * *

![](files/pfa-mcpi.svg){width="60%"}

* * *

- Dans le fichier `mcpi.hs`, implémentez la fonction `iterMcpi`, qui calcule
  une nouvelle itération de la méthode de Monte-Carlo, c'est-à-dire la nouvelle
valeur de l'approximation et le nouvel état obtenu. 

- Implémentez la fonction `run3`, qui calcule trois itérations successives et
  retourne la liste des approximations et l'état obtenu.

- Implémentez la fonction `runN`, qui calcule $N$ itérations successives.

- Calculez et affichez l'approximation obtenue après dix milles itérations.

- Dans le fichier `mcpi-state.hs`, réimplémentez votre code avec la monade
  `State` (cf la doc).

