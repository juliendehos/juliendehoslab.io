---
title: Environnement de développement en Haskell
date: 2021-08-23
---

# Généralités

## Langage de programmation

- défini par une spécification

- ou par un compilateur de référence

- Haskell : Haskell98, Haskell2010 puis GHC


## Outils 

- compilateur/interpréteur : GHC

- gestion de projets (sources, dépendances, build) : cabal, stack

- gestion de toolchains : ghcup, nix

- dépôt de bibliothèques : hackage, stackage

- tests : hspec, quickcheck

- documentation : haddock

- debug, profilage, benchmark : GHC, criterion

- déploiement : binaires, packages, conteneurs, backend js/wasm...


## Organisation d'un projet Haskell

- 1 module = 1 fichier `.hs` = 1 fichier `.o`

- pas d'organisation imposée

- mais organisation classique :

    - programmes principaux dans le dossier `app`
    - bibliothèques dans `src`
    - tests dans `test`
    - fichiers de config (cabal, stack ou nix)

* * *

```text
monprojet/
|-- app
|   |-- app1
|   |    |-- Main.hs
|   |-- app2
|        |-- Main.hs
|-- src
|   |-- Module1
|       |-- Module1a.hs
|       |-- Module1b.hs
|-- test
|   |-- Module1
|   |   |-- Module1aSpec.hs
|   |   |-- Module1bSpec.hs
|   |-- Spec.hs
|-- monprojet.cabal 
```

# GHC

## Généralités 

- [Glasgow Haskell Compiler](https://www.haskell.org/ghc/)

- quasiment le compilateur de référence (Haskell2010 + extensions)

- projet open-source (BSD3)

- écrit principalement en Haskell, et un peu en C/C--

- [doc GHC](https://downloads.haskell.org/ghc/latest/docs/html/users_guide/)


## Compilation

- programme `ghc`

- compilation classique

- code natif, js ou wasm

- plutôt destiné aux releases finales, optimisées

```text
$ ghc -Wall -O2 helloworld.hs 
[1 of 1] Compiling Main             ( helloworld.hs, helloworld.o )
Linking helloworld ...

$ ./helloworld 
Hello World!
```


## Exécution directe 

- programme `runghc`

- exécute un code source sans compilation préalable

- plutôt destiné à tester des petits programmes en développement

```text
$ runghc -Wall helloworld.hs
Hello World!
```


## Interpréteur interactif

- programme `ghci`

- interpréteur interactif, REPL (Read-Eval-Print-Loop)

- programmation interactive, test interactif de modules existants

```haskell
$ ghci
GHCi, version 8.6.5: http://www.haskell.org/ghc/  :? for help

Prelude> mul2 x = 2*x

Prelude> mul2 21
42

Prelude> :type mul2
mul2 :: Num a => a -> a
```

* * * 

- `ghci` est un outil très pratique :

```haskell
Prelude> import Data.List

Prelude Data.List> :info foldl'
class Foldable (t :: * -> *) where
  ...
  foldl' :: (b -> a -> b) -> b -> t a -> b
  ...
  	-- Defined in ‘Data.Foldable’

Prelude Data.List> :load helloworld.hs
[1 of 1] Compiling Main             ( helloworld.hs, interpreted )
Ok, one module loaded.

*Main> main
Hello World!
```

* * *

- inline GHCi :

![](files/pfa-env-inline-ghci.png){width="70%"}


## Runtime system

- un programme Haskell s'exécute via un runtime

- ce runtime peut être paramétré :

    - à la compilation, spécifier l'option `-rtsopts`
    - à l'exécution, mettre les options dans `+RTS ... -RTS`

- par exemple pour limiter la heap memory à 100 Mo:

```text
$ ghc -rtsopts helloworld.hs

$ ./helloworld +RTS -M100m -RTS
Hello World!
```


# Cabal

## Généralités

- système de packages pour Haskell :

    - description de projet, compilation...
    - gestionnaire de paquets 

- programme de gestion de projet : [cabal](https://www.haskell.org/cabal/users-guide/index.html)

- dépot de paquets : [hackage](http://hackage.haskell.org/)

- et pour chercher des fonctions Haskell : [hoogle](https://hoogle.haskell.org/)


## Configuration d'un projet Cabal

- cabal-file, au nom du projet (par exemple, `monprojet.cabal`)

- paramètres globaux :

```haskell
cabal-version:      2.2
name:               monprojet
version:            1.0
build-type:         Simple
license:            MIT

common deps
    ghc-options:        -Wall -O
    default-language:   Haskell2010
    build-depends:      base
```

* * *

- description des lib, exe...

```haskell
library
    import:             deps
    hs-source-dirs:     src
    exposed-modules:    Module1.Module1a
                        Module1.Module1b

executable app1
    import:             deps
    hs-source-dirs:     app/app1
    main-is:            Main.hs
    build-depends:      monprojet

executable app2
    import:             deps
    hs-source-dirs:     app/app2
    main-is:            Main.hs
    build-depends:      monprojet
```


## Utilisation de Cabal

- construire le projet :

```text
$ cabal build
Resolving dependencies...
Build profile: -w ghc-8.6.5 -O1
In order, the following will be built (use -v for more details):
 - monprojet-1.0 (lib) (first run)
 - monprojet-1.0 (exe:app2) (first run)
 - monprojet-1.0 (exe:app1) (first run)
Configuring library for monprojet-1.0..
...
```

* * *

- exécuter un programme :

```text
$ cabal run app1
Up to date
42
```

- lancer un interpréteur :

```text
$ cabal repl
```

- nettoyer le projet :

```text
$ cabal clean
```


# Nix

## Généralités

- [Nix](https://nixos.org/) :

    - gestionnaire de paquets logiciels
    - langage de description de paquets (fonctionnel pur à évaluation paresseuse)
    - logithèque (nixpkgs), distribution linux (NixOS)...

- fonctionne bien avec Haskell :

    - Nix + GHC
    - Nix + Cabal

- différentes configurations possibles


## Nix + GHC

- exemple de fichier `shell.nix`

```nix
let

  pkgs = import <nixpkgs> {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    gloss
    random
  ]);

in pkgs.stdenv.mkDerivation {
  name = "my-haskell-env";
  buildInputs = [
    ghc 
  ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}
```

* * *

- utilisation dans un `nix-shell` :

```text
$ nix-shell 

[nix-shell]$ runghc ...

[nix-shell]$ exit
exit
```


## Nix + Cabal

- projet avec un cabal-file (par exemple, `monprojet.cabal`)

* * *

- `default.nix` :

```nix
{ pkgs ? import <nixpkgs> {} }:

let

  ghc = pkgs.haskellPackages;

in ghc.developPackage {

  root = ./.;

  withHoogle = false;

  modifier = drv:
    pkgs.haskell.lib.addBuildTools drv (with ghc; [
      cabal-install
    ]);
}
```

* * *

- utilisation dans un `nix-shell` :

```text
$ nix-shell 

[nix-shell]$ cabal build
...
```


# IDE

## Généralités 

- gestion de projet : build, run, test, git...

- édition de code : coloration, complétion, navigation...


## IDE pour Haskell

- éditeurs : vscode, idea, emacs, vim...

- outils d'édition/analyse : hlint, ghcid, hls...


## Setup de TP

- cabal + nix

- vscode + simple-ghc

- voir la section [environnement de travail](../env/index.html)


# Test

## Généralités 

- le système de types réduit les sources d'erreurs mais ne les empêche pas
  complètement

- Haskell a des outils de test intéressants :

    - framework de test ([hspec](https://hspec.github.io/))
    - test de propriétés (quickcheck)


## Configuration de Hspec

- cabal-file (`monprojet.cabal`) :

```haskell
test-suite spec
    import:             deps
    main-is:            Spec.hs
    hs-source-dirs:     test
    type:               exitcode-stdio-1.0
    build-depends:      monprojet, hspec, QuickCheck
    other-modules:      Module1.Module1aSpec
                        Module1.Module1bSpec
```

- découverte automatique (`test/Spec.hs`) :

```haskell
{-# OPTIONS_GHC -F -pgmF hspec-discover #-}
```

## Module de test

- par exemple, `test/Module1/Module1aSpec.hs` :

```haskell
module Module1.Module1aSpec (main, spec) where

import Test.Hspec
import Module1.Module1a

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "mul2" $ do
        it "mul2 0" $ mul2 0 `shouldBe` (0::Int)
        it "mul2 (-21)" $ mul2 (-21) `shouldBe` (-42::Int)
    ...
```

## Exécution des tests

- avec Cabal :

```text
[nix-shell]$ cabal test
...
Running 1 test suites...
Test suite spec: RUNNING...
Test suite spec: PASS
1 of 1 test suites (1 of 1 test cases) passed.
```


# Documentation

## Généralités 

- [haddock](https://www.haskell.org/haddock/)

- outil de génération à la javadoc/doxygen

- utilisé pour générer la doc sur hackage


## Écrire une documentation

- par exemple, `src/Module1/Module1a.hs` :

```haskell
-- |
-- Module: Module1a
-- This is my Module1a module
module Module1.Module1a where

-- | Multiplies a number by two.
mul2 :: Num a => a -> a
mul2 = (*2)
```

## Générer une documentation

- `cabal haddock` 

```text
[nix-shell]$ cabal haddock 
Build profile: -w ghc-8.6.5 -O1
In order, the following will be built (use -v for more details):
 - monprojet-1.0 (lib) (ephemeral targets)
Preprocessing library for monprojet-1.0..
Running Haddock on library for monprojet-1.0..
Haddock coverage:
 100% (  2 /  2) in 'Module1.Module1a'
...
```

* * *

![](files/pfa-haddock.png){width="60%"}


# Déploiement

## Généralités 

- déploiement classique d'un programme compilé

- projet sur hackage

- paquet Nix

- image Docker...

## Installation avec Nix

```text
nix-env -if .
```

## Docker avec Nix

- écrire un `docker.nix` :

```nix
{ pkgs ? import <nixpkgs> {} }:
let
  app = pkgs.callPackage ./default.nix {};
  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';
in
  pkgs.dockerTools.buildLayeredImage {
    name = "monprojet";
    tag = "latest";
    config = {
      WorkingDir = "${app}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${app}/bin/monprojet" ];
    };
  }
```

* * *

- construire l'image Docker et la charger dans docker :

```text
$ nix-build docker.nix

$ docker load < result
```

- tester l'image Docker :

```text
docker run --rm -it -p 3000:3000 monprojet:latest
```


## Déployer une image Docker sur Heroku

- créer un compte sur [Heroku](https://www.heroku.com/) et installer le client `heroku`

- créer une application Heroku :

```text
heroku login
heroku container:login
heroku create monprojet-app1
```

* * *

- déployer l'image Docker :

```text
docker tag monprojet:latest registry.heroku.com/monprojet-app1/web
docker push registry.heroku.com/monprojet-app1/web
heroku container:release web --app monprojet-app1
```

- éteindre l'application :

```text
heroku ps:scale web=0 --app monprojet-app1
```


## CI/CD gitlab (avec Nix)

- `.gitlab-ci.yml` :

```yaml
image: nixos/nix

build:
    stage: build
    script:
        - nix-build

pages:
    stage: deploy
    script:
        - nix-build -A doc
        - cp -r $(find ./result-doc/share/doc -name html) public
    artifacts:
        paths:
            - public
```


# Performances en Haskell

## Généralités 

- Haskell est capable de produire du code performant

- mais l'optimisation est particulière (évaluation non-stricte, gestion mémoire, garbage collector)

- d'où la nécessité de mesurer : profilage temps/mémoire, benchmark...


## Profilage avec GHC

- [section Profiling de la doc](https://downloads.haskell.org/ghc/latest/docs/html/users_guide/profiling.html)

- exemple (`fib.hs`) :

```haskell
main = print (fib 30)

fib n = if n < 2 then 1 else fib (n-1) + fib (n-2)
```

- à la compilation, activer le profilage :

```text
$ ghc -prof -fprof-auto -rtsopts fib.hs
```

* * *

- exécution avec rapport de profilage :

```text
$ ./fib +RTS -p

$ cat fib.prof 
...
COST CENTRE  SRC             no.     entries  %time %alloc

MAIN         <built-in>      115          0    0.0    0.0 
 CAF         <entire-module> 229          0    0.0    0.0 
  main       fib.hs:1:1-21   230          1    0.0    0.0 
   fib       fib.hs:2:1-50   232    2692537  100.0  100.0 
 CAF         <entire-module> 224          0    0.0    0.0 
 CAF         <entire-module> 215          0    0.0    0.0 
 CAF         <entire-module> 213          0    0.0    0.0 
 CAF         <entire-module> 205          0    0.0    0.0 
 CAF         <entire-module> 145          0    0.0    0.0 
 main        fib.hs:1:1-21   231          0    0.0    0.0 
```

* * *

- utilisation mémoire :

```text
$ ./fib +RTS -h
1346269

$ hp2ps -c fib.hp 

$ inkscape --export-plain-svg="fib.svg" fib.ps
```

* * *

![](files/pfa-ghc-prof.svg){width="90%"}


## Benchmarking avec criterion

- micro-benchmarks statistiques

- [homepage criterion](http://www.serpentine.com/criterion/)

* * *

- exemple (`fibber.hs`) :

```haskell
import Criterion.Main

fib m | m < 0     = error "negative!"
      | otherwise = go m
  where go 0 = 0
        go 1 = 1
        go n = go (n-1) + go (n-2)

main = defaultMain [
  bgroup "fib" [ bench "1"  $ whnf fib 1
               , bench "5"  $ whnf fib 5
               , bench "11" $ whnf fib 11
               ]
  ]
```

* * *

```text
$ ghc -O fibber.hs
[1 of 1] Compiling Main             ( fibber.hs, fibber.o )
Linking fibber ...

$ ./fibber --output fibber.html
benchmarking fib/1
time                 14.31 ns   (14.31 ns .. 14.32 ns)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 14.32 ns   (14.31 ns .. 14.33 ns)
std dev              39.50 ps   (29.67 ps .. 59.45 ps)
...
```

* * *

![](files/pfa-criterion.png){width="80%"}

* * *

- intégration dans un cabal-file (pour un fichier `bench/Main.hs`) :

```haskell
benchmark criterion
  type:                exitcode-stdio-1.0
  main-is:             Main.hs
  hs-source-dirs:      bench
  default-language:    Haskell2010
  ghc-options:         -Wall -O
  build-depends:       base, criterion
```

puis `cabal bench` 



# Travaux pratiques

Travaillez dans le dossier `tp-env` de votre dépôt git.

## Nix-shell simple 

- Dans le dossier `tp-env/random`, essayez d'exécuter les programmes Haskell
  fournis.

- Écrivez un `shell.nix` permettant de satisfaire les dépendances nécessaires.
  Testez dans un `nix-shell`.


## Templates de projet

- Créez un projet `myproject` avec le template `hs-full` du dépôt
  <https://gitlab.com/juliendehos/kickstart-templates>.

- Testez la compilation/test/doc avec Nix + Cabal. 


## Mise en place d'un projet

Le dossier `tp-env/arithm` contient le code d'une calculatrice gérant des entiers,
des additions et des multiplications, en notation préfixée.

- Testez le programme fourni.

- Réorganisez le projet de façon à le rendre utilisable avec Cabal et Nix.

- Implémentez des tests.

- Documentez le code.

- Mettez votre projet sur Gitlab et configurez une CI qui construit le projet,
  exécute les tests et publie sa documentation.


## Application Heroku

Le projet `tp-env/helloapp` contient un serveur web qui affiche un message
d'accueil, éventuellement personnalisé.

- Lancez le programme avec Nix + Cabal et testez via un navigateur web.

- Écrivez un `docker.nix`, construisez l'image et testez.

* * *

- Déployez sur Heroku (ou pas car Heroku ne propose plus de comptes gratuits).

<video preload="metadata" controls>
<source src="files/pfa-helloapp.mp4" type="video/mp4" />
![](files/pfa-helloapp.png){width="80%"}

</video>


## Blog statique avec Hakyll

Le projet `tp-env/blog` implémente un blog statique avec
[Hakyll](https://jaspervdj.be/hakyll/).

- Regardez les fichiers fournis et comprenez le fonctionnement d'un projet
  Hakyll.

- Ouvrez un terminal, lancez un `nix-shell` puis un `make watch` et regardez le résultat.

- Laissez tourner le serveur et ajoutez quelques pages de blog.

- Stoppez le serveur et quittez le `nix-shell`. Faites un `nix-build` et regardez le résultat produit.

- Créez un projet Gitlab, copiez-y les fichiers nécessaires et ajoutez une CI avec Nix pour
  générer/déployer automatiquement le blog lors des `git push`.


