---
title: Compilation
date: 2021-08-23
---

# Généralités

## Notion de compilateur

- lit un texte en entrée

- le "comprend", selon des règles prédéfinies

- fait quelque chose avec le résultat

![](files/compiler.svg){width="60%"}

## Notion de langage

- grammaire formelle

- contextuelle, non-contextuelle

- ambigüe, non-ambigüe


## Pourquoi utiliser Haskell ?

- fonctionnel pur

- pattern matching

- système de types

- ADT, type-class, higher-kinded types...

## Outils de compilation en Haskell

- compilateur de compilateurs (à la lex/yacc) : Alex/Happy

- combinateurs de parseurs monadiques : ReadP, Parsec, Megaparsec... 


# Exemple

## Projet list-simple

- liste de chiffres séparés par un espace, le tout encadré par des parenthèses

```sh
(1 2 3)
(1)
()
```

- en sortie : code Lisp, code Haskell
- exemple trivial

## AST / représentation intermédiaire

```haskell
newtype List
    = List [Int]
    deriving (Eq, Show)
```

- ici c'est trivial (mais souvent on a un arbre / type récursif)

## Génération de code

```haskell
toLisp :: List -> String
toLisp (List xs) = "(" ++ unwords (map show xs)  ++ ")"

toHaskell :: List -> String
toHaskell (List xs) = "[" ++ intercalate "," (map show xs)  ++ "]"
```

## Application principale

```sh
> (1 2 3)
List [1,2,3]
(1 2 3)
[1,2,3]

> (1)
List [1]
(1)
[1]

> ()
List []
()
[]

> (1
error: no parse
```

## Grammaire

```sh
list        ::= '(' digitList ')'

digitList   ::= digit ' ' digitList | digit | empty

digit       ::= '0' | '1' | ... | '9'
```

- il ne reste plus qu'à écrire un parseur qui reconnait cette grammaire...


# Parseurs récursifs descendants (1)

## Type Result

```haskell
type Result v = Either String (v, String)
```

- quand le parsing échoue : `Left` avec un message d'erreur
- quand le parsing réussit : `Right` avec valeur parsée + texte restant


## Type ParseFunc

```haskell
type ParseFunc v = String -> Result v
```

- un `ParseFunc` est une fonction qui prend un texte d'entrée et retourne un `Result`

## Parseur élémentaire

```haskell
itemF :: ParseFunc Char
itemF "" = Left "no input"
itemF (x:xs) = Right (x, xs)
```

- `itemF` consomme un caractère de l'entrée et le retourne dans un `Result`

```haskell
*Codegen Parser.Recursive1> itemF "1 2 3"
Right ('1'," 2 3")

*Codegen Parser.Recursive1> itemF ""
Left "no input"
```

## Parser la grammaire

```haskell
-- digit ::= '0' | '1' | ... | '9'
digitF :: ParseFunc Int
digitF s0 = case itemF s0 of
    Right (v1, s1) -> if isDigit v1 
        then Right (digitToInt v1, s1)
        else Left "no parse"
    Left err -> Left err
```

* * *

```haskell
-- digitList ::= digit ' ' digitList | digit | empty
digitListF :: ParseFunc [Int]
digitListF = alt1
    where
        alt1 s0 = case digitF s0 of
             Right (v1, s1) -> case itemF s1 of
                Right (v1', s1') -> if v1' == ' ' 
                    then case digitListF s1' of 
                        Right (v2, s2) -> Right (v1:v2, s2)
                        Left _ -> alt2 s0
                    else Right ([v1], s1)
                Left _ -> alt2 s0
             _ -> alt2 s0
        alt2 s0 = case digitF s0 of
            Right (v1, s1) -> Right ([v1], s1)
            Left _ -> alt3 s0
        alt3 s0 = Right ([], s0)
```

* * *

```haskell
-- list ::= '(' digitList ')'
listF :: ParseFunc List
...

parseList :: String -> Either String List
parseList input = case listF input of
    Right (v1, _) -> Right v1
    Left err -> Left err
```


## En résumé

- on définit un type (`Result v`) pour représenter le résultat d'un parsing

- on définit des `ParseFunc v` comme des fonctions `String -> Result v`,
  progressivement, jusqu'au parseur principal, qui parse complètement la
  grammaire

- un parseur peut appeler un autre parseur (et le compilateur vérifie que les
  types sont compatibles)

- mais c'est verbeux, avec beaucoup de code qui se ressemble

- idée suivante : implémenter des fonctions pour "manipuler" des parseurs


# Parseurs récursifs descendants (2)

## Construire un parseur à partir d'un prédicat

```haskell
satisfyF :: (Char -> Bool) -> ParseFunc Char
satisfyF f s0 = case itemF s0 of 
    Right (v1, s1) -> if f v1 then Right (v1, s1) else Left "no parse"
    Left err -> Left err

digitF :: ParseFunc Int
digitF s0 = case satisfyF isDigit s0 of
    Right (v1, s1) -> Right (digitToInt v1, s1)
    Left err -> Left err
```

## Combiner deux parseurs

```haskell
thenF :: ParseFunc a -> ParseFunc b -> ParseFunc b
thenF p1 p2 s0 = case p1 s0 of
    Right (_, s1) -> p2 s1
    Left err -> Left err
```

* * *

```haskell
digitListF :: ParseFunc [Int]
digitListF = alt1
    where
        alt1 s0 = case digitF s0 of
             Right (v1, s1) -> case (charF ' ' `thenF` digitListF) s1 of 
                Right (v2, s2) -> Right (v1:v2, s2)
                Left _ -> alt2 s0
             _ -> alt2 s0
        alt2 s0 = case digitF s0 of
            Right (v1, s1) -> Right ([v1], s1)
            Left _ -> alt3 s0
        alt3 s0 = Right ([], s0)
```

## En résumé

- quand on implémente un parseur par une fonction, on peut écrire des fonctions
  qui combinent des parseurs

- mais on peut faire mieux...


# Combinateurs de parseurs

## Type Parser

```haskell
type Result v = Either String (v, String)

newtype Parser v = Parser { runParser :: String -> Result v }
```

- ici, le type `Parser v` représente une valeur `Parser` comportant un
  champ `runParser`; ce champ est une fonction qui prend un `String` et
  retourne un `Result v`

- autrement dit, "on intègre dans un type algébrique" la fonction de parsing de
  la section précédente

## Parseur élémentaire

- dans la section précédente, on avait :

```haskell
itemF :: ParseFunc Char     -- équivalent à : String -> Result Char
itemF "" = Left "no input"
itemF (x:xs) = Right (x, xs)
```

* * *

- on peut donc en faire un `Parser` :

```haskell
itemP :: Parser Char
itemP = Parser { runParser = itemF }
```

```haskell
> runParser itemP "foobar"
Right ('f', "oobar")

> runParser itemP ""
Left "no parse"
```

* * *

- autres écritures possibles :

```haskell
itemP :: Parser Char
itemP = Parser itemF
```

```haskell
itemP :: Parser Char
itemP = Parser $ \s0 ->
    case s0 of
        (v1:s1) = Right (v1, s1)
        _       = Left "no input"
```


## Parser la grammaire

- on peut réécrire les parseurs précédents avec le type `Parser`...

* * *

- avant :

```haskell
satisfyF :: (Char -> Bool) -> ParseFunc Char
satisfyF f s0 = case itemF s0 of 
    Right (v1, s1) -> if f v1 then Right (v1, s1) else Left "no parse"
    Left err -> Left err

digitF :: ParseFunc Int
digitF s0 = case satisfyF isDigit s0 of
    Right (v1, s1) -> Right (digitToInt v1, s1)
    Left err -> Left err

...
```

* * *

- après :

```haskell
satisfyP :: (Char -> Bool) -> Parser Char
satisfyP f = Parser $ \s0 -> case runParser itemP s0 of
    Right (v1, s1) -> if f v1 then Right (v1, s1) else Left "no parse"
    Left err -> Left err

digitP :: Parser Int
digitP = Parser $ \s0 -> case runParser (satisfyP isDigit) s0 of
    Right (v1, s1) -> Right (digitToInt v1, s1)
    Left err -> Left err

...
```

## En résumé

- on définit un type algébrique `Parser v` qui contient une fonction de parsing

- en utilisant ce type, on écrit les parseurs qui vont bien pour notre
  grammaire

- mais le code est plus compliqué... pas glop

- mais on va pouvoir implémenter des fonctionnalités au niveau du type `Parser
  v`...


# Combinateurs de parseurs monadiques

## Retour sur digitP

```haskell
digitP :: Parser Int
digitP = Parser $ \s0 -> case runParser (satisfyP isDigit) s0 of
    Right (v1, s1) -> Right (digitToInt v1, s1)
    Left err -> Left err
```

- autrement dit, `digitP` c'est `satisfyP isDigit` où on applique `digitToInt`
  sur la valeur parsée; ce qui revient à fmapper `digitToInt` sur `satisfyP
  isDigit`:

```haskell
digitP :: Parser Int
digitP = digitToInt <$> satisfyP isDigit
```

- et ça tombe bien car comme `Parser` est un ADT, on va pouvoir intancier des
  classes dessus


## Classes intéressantes pour le type Parser

- Functor ("appliquer une fonction à l'intérieur du type"):

```haskell
(<$>) :: Functor f => (a -> b) -> f a -> f b
```

- Applicative functor ("appliquer une fonction elle-même à l'intérieur du type"):

```haskell
(<*>) :: Applicative f => f (a -> b) -> f a -> f b

pure :: Applicative f => a -> f a
```

- Monad ("séquencer des actions"): 

```haskell
(>>=) :: Monad m => m a -> (a -> m b) -> m b

return :: Monad m => a -> m a
```

* * *

- Alternative ("monoïde sur les foncteurs applicatifs"): 

```haskell
(<|>) :: Alternative f => f a -> f a -> f a

empty :: Alternative f => f a
```


## Maintenant, c'est plus simple !

- avec `ParseFunc` :

```haskell
digitListF :: ParseFunc [Int]
digitListF = alt1
    where
        alt1 s0 = case digitF s0 of
             Right (v1, s1) -> case itemF s1 of
                Right (v1', s1') -> if v1' == ' ' 
                    then case digitListF s1' of 
                        Right (v2, s2) -> Right (v1:v2, s2)
                        Left _ -> alt2 s0
                    else Right ([v1], s1)
                Left _ -> alt2 s0
             _ -> alt2 s0
        alt2 s0 = case digitF s0 of
            Right (v1, s1) -> Right ([v1], s1)
            Left _ -> alt3 s0
        alt3 s0 = Right ([], s0)
```

* * *

- avec `Parser` (en "style monade") :

```haskell
digitListP :: Parser [Int]
digitListP = alt1 <|> alt2 <|> alt3
    where
        alt1 = do
            x <- digitP
            _ <- charP ' '
            xs <- digitListP
            return (x:xs)
        alt2 = do
            x <- digitP
            return [x]
        alt3 = return []
```

* * *

- avec `Parser` (en "style applicatif") :

```haskell
digitListP :: Parser [Int]
digitListP 
    =   ((:) <$> digitP <*> (charP ' ' *> digitListP))
    <|> (:[]) <$> digitP
    <|> return []
```


## Mais il reste à écrire les instances...

```haskell
instance Functor Parser where

    -- (<$>) :: (a -> b) -> Parser a -> Parser b
    fct <$> p = 
        Parser $ \s0 -> do
            (v1, s1) <- runParser p s0
            return (fct v1, s1)

...
```

- Question : d'où vient ce "do" ?


# ReadP

## Généralités

- module de combinateurs de parseurs monadiques

- disponible dans la bibliothèque de base

- fournit un type de parseur appelé `ReadP`, avec les instances qui vont bien

- fournit des parseurs de base, des combinateurs...

## Utilisation

- importer le module `Text.ParserCombinators.ReadP`

- écrire les parseurs voulus, à partir des élements fournis par ReadP

- écrire la fonction de parsing principale

## Exemple avec list-simple

```haskell
digitP :: ReadP Int
digitP = digitToInt <$> satisfy isDigit

digitListP :: ReadP [Int]
digitListP 
    =   ((:) <$> digitP <*> (char ' ' *> digitListP))
    <|> (:[]) <$> digitP
    <|> return []

listP :: ReadP List
...
```

* * *

```haskell
runParser :: ReadP a -> String -> Either String (a, String)
runParser parser input = case readP_to_S parser input of
    [] -> Left "no parse"
    xs -> Right $ last xs

parseList :: String -> Either String List
parseList input = case runParser listP input of
    Right (v1, _) -> Right v1
    Left err -> Left err
```


# Travaux pratiques

## list-simple

- Dans `tp-compilation/list-simple`, complétez la fonction `listF` du module
  `Parser.Recursive1`. Vérifiez votre implémentation avec des tests unitaires.

- Dans le module `Parser.Recursive2`, complétez les fonctions `charF` et
  `listF` (en utilisant `thenF`). Testez.

- Idem pour le module `Parser.Parsec` : complétez `charP` et `listP`. Testez.

* * *

- Dans le module `Parser.Monadic`, complétez `charP` et `listP` (en utilisant
  la notation do). Testez.

- Dans le module `Parser.ReadP`, complétez `listP` (en utilisant la "notation
  do"). Testez.

- Réécrivez `listP` en "style applicatif".


## list-expr

On veut un programme capable de lire des listes (d'entiers ou de sous-listes)
au format Lisp et de générer les listes correspondantes aux formats Lisp et
JSON.

```sh
> (1 2 3 ())
List [Atom 1,Atom 2,Atom 3,List []]
(1 2 3 ())
[1,2,3,[]]

> ((13   37)42 )  
List [List [Atom 13,Atom 37],Atom 42]
((13 37) 42)
[[13,37],42]

> (1 2()
error: no parse
```

* * *

- Complétez le projet `tp-compilation/list-expr` de façon à implémenter ce
  programme. Pour cela, utilisez `ReadP` et les combinateurs (`many`,
  `many1`, `skipSpaces`...). Vérifiez avec des tests unitaires.

- Réécrivez votre parser en utilisant `between` et `sepBy` (ou sans si vous les
  aviez déjà utilisés).


## myjson

On veut parser/générer du [JSON](https://www.json.org/json-fr.html) simplifié.

```json
value                                member
    object                               string ':' element
    string                           
    number                           element
    "true"                               value
    "false"                          
    "null"                           string
                                         '"' characters '"'
object                               
    '{' members '}'                  number
                                         integer
members                              
    member                           
    member ',' members               
```

* * *

- Regardez l'AST proposé dans le projet `tp-compilation/myjson`.

- Complétez l'implémentation de la génération de code. Vérifiez avec les tests
  unitaires.

- Complétez l'implémentation du parsing. Vérifiez avec les tests unitaires et
  avec des fichiers de test.

```sh
$ cabal run myjson data/test5.json 
syntax: ValueObject (Object [Member "foo" (ValueNumber (-42)),Member "boz" ValueFalse])
codegen: {"foo":-42,"boz":false}
```


## mycalc-readp

Le projet `tp-compilation/mycalc-readp` implémente une calculatrice qui gère
les nombres à virgule, les additions, les soustractions et les parenthèses.

- Regardez le code fourni et ajoutez la gestion de la multiplication, division
  et puissance. Attention : la multiplication/division est infixe à gauche
  alors que la puissance est infixe à droite.

```sh
$ cabal run mycalc 

> 1+2^3^2+2*3/2
EAdd (EAdd (EVal 1.0) (EPow (EVal 2.0) (EPow (EVal 3.0) (EVal 2.0)))) (EDiv (EMul (EVal 2.0) (EVal 3.0)) (EVal 2.0))
(+ (+ 1.0 (^ 2.0 (^ 3.0 2.0))) (/ (* 2.0 3.0) 2.0))
516.0
```

