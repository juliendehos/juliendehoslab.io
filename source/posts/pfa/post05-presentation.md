---
title: Présentation du module
date: 2021-08-23
---

## Objectifs du module

- rappeler les notions de base de PF, en Haskell
- voir quelques fonctionnalités plus avancées du système de types
- réaliser quelques projets concrets (web, compilation...)

## Pré-requis 

- notions de base d'algo
- notions de base de développement web
- (module de programmation fonctionnelle de la L3)

## Organisation

- 14 séances de 3h

## Évaluation

- session 1 : contrôle continu
- session 2 : devoir sur feuille (aucun document autorisé)
- voir la section [Remarques sur le contrôle continu](../env/post40-cc.html)

## Travaux pratiques

- VM fournie (voir la section [Environnement de travail, Machine Virtuelle](../env/post10-vm.html))
- [dépôt gitlab](https://gitlab.dpt-info.univ-littoral.fr/julien.dehos/ulco-pfa-etudiant) à forker (voir la section [Environnement de travail, GIT](../env/post20-git.html))
- [flash-cards](https://gitlab.com/juliendehos/juliendehos.gitlab.io/-/raw/master/source/posts/pfa/files/PFA.apkg?ref_type=heads&inline=false) au format [Anki](https://apps.ankiweb.net/)

