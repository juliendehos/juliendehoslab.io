---
title: GL1 TP4 - Documentation
date: 2023-08-23
---

# Édition de code avec VSCode

(Voir également la [présentation de VSCode](../env/post30-vscode.html) dans la
section environnement de travail)

## Intégration de Make

- Allez dans le dossier `ulco-gl1-etudiant/tp-documentation/editeur1` et lancez
  VSCode avec la commande suivante.

```text
code .
```

- Regardez les fichiers fournis. Ouvrez le terminal VSCode (raccourci clavier
  `Ctrl-;`), compilez et testez.

* * *

![](files/tp-documentation-editeur1.png){width="70%"}


## Intégration de CMake

- Copiez le dossier `tp-documentation/editeur1` dans un dossier
  `tp-documentation/editeur2`. Ouvrez VSCode dans ce nouveau dossier, supprimez
le `Makefile` et écrivez un `CMakeLists.txt`.

- Compilez en utilisant les fonctionnalités de VSCode : boutons dans la barre
  du bas ou  `Ctrl-Shift-p` + les commandes  `CMake: Configure` et  `CMake:
Build`. Si VSCode vous demande de choisir un compilateur, sélectionnez le
compilateur "Non spécifié".

- Exécutez le programme généré, via les boutons de VSCode et via son terminal.


## Intégration de Git

- Toujours depuis votre VSCode dans `editeur2`, ouvrez le panneau Git.

- Ajoutez les fichiers C++ et la configuration CMake. Commitez, pushez et
  vérifiez sur Gitlab. 

- Dans VSCode, modifiez un fichier du projet. Vérifiez que le panneau Git
  indique une modification et ouvrez un onglet montrant ces changements.


## Intégration de Nix

- Allez dans le dossier `tp-documentation/hello-gtk` et lancez VSCode.

- Essayez de compiler le projet et regardez les éventuelles erreurs.

- Ce projet utilise une configuration Nix. Pour que VSCode la prenne en compte,
  lancez la commande VSCode `Nix-Env: Select environment` (via `Ctrl-Shift-p`),
sélectionnez le fichier `default.nix` du projet puis cliquez sur `Reload` (dans
la pop-up en bas à droite).

- Regardez le contenu du dossier `.vscode` généré.

- Supprimez le dossier `build` généré précédemment puis relancez la compilation
  et l'exécution. Vérifiez que tout fontionne désormais.

* * *

Remarque 1 : la commande `Nix-Env: Select environment` permet que VSCode
prenne en compte la configuration Nix pour son interface mais pas pour son
terminal. Pour utiliser la configuration Nix dans le terminal VSCode, il faudra
donc utiliser un `nix-shell`.

Remarque 2 : normalement, les projets fournis en TP contiennent déjà la
configuration VSCode + Nix, donc vous ne devrez pas avoir besoin d'utiliser la
commande `Nix-Env: Select environment`.


# Fichiers Markdown

## Principe

Markdown est un langage de balisage léger, lisible directement ou après un
rendu.  Il est notamment reconnu et interprété par Gitlab/VSCode/etc, ce qui
permet d'écrire des petits fichiers de documentation qui seront affichés
joliment (cf [GitLab Flavored
Markdown](https://docs.gitlab.com/ee/user/markdown.html),
[CommonMark Spec](https://spec.commonmark.org/0.30/) et [Basic writing and
formatting
syntax](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)).

## Exercices

- Ouvrez un VSCode dans `tp-documentation/mydoc-md`.

- Créez un fichier `README.md`, ouvrez-le (ainsi que la fenêtre d'aperçu) et remplissez-le de façon à obtenir le rendu ci-dessous.

- Ajoutez votre fichier au dépôt Git et vérifiez son rendu sur Gitlab.

* * *

![](files/TP_documentation_markdown.png){width="70%"}



# Documentation développeur avec Doxygen

## Principe

Une documentation développeur permet de faciliter la maintenance du code ou
l'utilisation d'une bibliothèque. Doxygen permet de générer ce genre de
documentation. Il s'utilise de la façon suivante : 

- dans le code source, écrire en commentaire (avec un formatage spécial) les
  informations à mettre dans la documentation 

- écrire un fichier de configuration `Doxyfile` indiquant comment générer la
  documentation

- exécuter le programme `doxygen` pour générer la documentation

Remarque : Doxygen propose également un générateur de configuration (`doxygen
-g`) ainsi qu'une interface graphique (`doxywizard`), et peut s'intégrer à
CMake.


## Exercices

- Dans le dossier `tp-documentation/drunk_player`, regardez le fichier
  `Doxyfile` fourni.  Générez la documentation html et vérifiez le résultat
  obtenu.

- Dans le code source, modifiez les commentaires afin d'obtenir une
  documentation ressemblant à cet 
  [exemple de doc doxygen](files/gl-doxygen/index.html). Pensez à utiliser les
  fonctionnalités de formatage des préconditions, exception, description brève,
  todo, bug... (cf la 
  [documentation doxygen](http://www.doxygen.nl/manual/index.html)).

* * *

![](files/TP_documentation_doxygen.png)


# Documentation utilisateur avec mdBook

## Principe

Une documentation utilisateur permet d'expliquer comment mettre en place et
utiliser le logiciel correspondant au projet, sans entrer dans les détails du
code. [mdBook](https://rust-lang.github.io/mdBook/index.html) permet de générer
ce type de documentation (entre autres). Il s'utilise de la façon suivante : 

- écrire un fichier de configuration `book.toml` indiquant comment générer la
  documentation

- écrire des fichiers contenant la documentation proprement dite, au
  format Markdown, et référencés dans un fichier `SUMMARY.md`.

- générer la documentation

## Exercices

- Dans le dossier `tp-documentation/drunk_player`, créez un dossier `manual`,
  allez dans ce dossier et initialisez une documentation mdBook avec la commande
  `mdbook init`. 

- Générez la documentation, avec aperçu dynamique dans un navigateur, via la
  commande `mdbook serve --open`.

- Dans VSCode, ouvrez le fichier `SUMMARY.md` et référencez un fichier
  `introduction.md`. Vérifiez que mdBook crée automatiquement le fichier et met
à jour l'aperçu dans le navigateur.

- Écrivez une documentation utilisateur pour le projet `drunk_player`
  ressemblant à cet [exemple de doc mdBook](files/gl-mdbook/index.html).

* * *
  
![](files/TP_documentation_mdbook.png){width="90%"}



# S'il vous reste de temps

- Testez l'intégration de Doxygen dans CMake.


