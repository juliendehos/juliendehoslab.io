---
title: GL1 TP1 - Environnement Unix
date: 2023-08-22
---

> Pour ce TP, utilisez un terminal et **ne touchez pas à la souris** (sauf
> éventuellement pour le navigateur web).

# Commandes shell basiques

## Commandes ls, pwd, more et less 

- À quoi servent-elles ? Consultez les pages man et testez-les.

- Comment afficher les fichiers cachés du chemin courant ?

- Comment afficher le détail des fichiers du chemin courant ?

[//]: # (ls -a)

[//]: # (ls -l)

## Notion de chemin

- Qu'est-ce-qu'un chemin absolu ? Un chemin relatif ? 

- Que signifie les symboles `/`, `~`, `..`, `.` et `*`.

- Comment afficher le contenu de votre dossier personnel (indépendamment du
  chemin courant) ?

- Comment afficher le contenu du dossier parent ? Du parent du parent ?

- Comment afficher les fichiers/dossiers dont le nom contient un "e" ?

[//]: # (ls ~)

[//]: # (ls ..)
[//]: # (ls ../..)

[//]: # (ls *e*)

## Navigation et manipulation de fichier 

- À quoi servent les commandes `cd`, `mkdir`, `mv` et `rm`

- En utilisant `cd` et `mkdir`, créez l'arborescence suivante :

```text
  ~
  |-- ...
  |-- tmp/
      |-- tata
      |-- toto
          |-- titi
```

- En utilisant `mv` et `rm`, modifiez l'arborescence en :

```text
  ~
  |-- ...
  |-- tmp/
      |-- tata
      |-- tutu
```

## Commande echo, variables d'environnement

- Quelle commande permet d'afficher "Bonjour !"

- Quelle commande permet d'afficher "Bonjour \<user\> !", où \<user\> est votre
  identifiant.

[//]: # (echo "Bonjour ${USER} !")

[//]: # (echo "Bonjour !")

## Contrôle de processus

- À quoi servent les signaux `C-c` (Ctrl+c) et `C-d` ?

- À quoi sert le symbole `&` à la fin d'une commande ? Testez en lançant le
  programme `mousepad`. Comment peut-on obtenir le même résultat sans utiliser
ce symbole ?


## Édition des commandes

Testez les fonctionnalités suivantes dans un terminal :

- `<tab>` : compléter le mot courant

- `M-b` (Alt+b) : aller au mot précédent

- `C-a` / `C-e` :  aller au début / à la fin de la ligne 

- `M-backspace` : supprimer le début du mot 

- `C-r` / `C-n` :  rechercher une commande précédente / suivante

- `↑` / `↓` : naviguer dans les commandes précédentes

- `history` historique des commandes précédentes

- `!<n>` relancer la commande `<n>`


# Commandes shell avancées

E/S standards, redirection, pipe :

- Quelles sont les entrées/sorties standards ?

- Qu'est qu'une redirection ?

- Qu'est qu'un « pipe » ?

- On peut représenter la commande `ls -l > liste.txt` par le schéma suivant.

![](files/TP_unix_processus.svg){width="30%"}

* * *

- Faites le schéma des commandes :

    - `cat < liste.txt`
    - `cat < liste.txt > copie_liste.txt`
    - `cat liste.txt`
    - `cat liste.txt liste.txt > liste_2_fois.txt`
    - `cat liste.txt | sort`
    - `cat liste.txt | sort > liste_triee.txt`
    - `cat liste.txt | sort | less`

## Commandes tr, grep, find et xargs

- Quelle commande permet de lister les fichiers en l33t sp34k (c'est-à-dire
  en remplaçant les "a" par "4", "e" par "3", "i" par "1" et "o" par "0") ?

- Quelle commande permet d'afficher le contenu d'un fichier en mettant
  toutes ses lettres en majuscule ?

- Quelle commande permet d'afficher les lignes d'un fichier contenant "42" ? 

- Quelle commande permet d'afficher tous les fichiers ".conf" contenus
  dans le dossier `/etc` ou ses sous-dossiers ? 

- Complétez la commande précédente pour afficher toutes les lignes
  contenant "user" parmi ces fichiers.

* * *

- Créez deux fichiers `a.txt` et `b.txt` contenant tous les deux :

```text
toto
tata
```

- Quelle commande permet d'afficher, pour chaque fichier ".txt", son
  contenu trié ?  Indication : utilisez l'option `-exec` de `find`.

- Modifiez la commande précédente pour trier l'ensemble de ces contenus ?


[//]: # (ls | tr aeio 4310)

[//]: # (cat fichier | tr [a-z] [A-Z])

[//]: # (grep "42" fichier)

[//]: # (find /usr/include -name "*.hpp")

[//]: # (find /usr/include -name "*.hpp" | xargs grep "TODO")

[//]: # (find . -name "*.txt" -exec sort {} \;)

[//]: # (find . -name "*.txt" -exec sort {} \+)

## Commande sed

- À quoi servent le programme `sed` ? 

- Quelle commande permet d'afficher un fichier en remplaçant le premier
  "t" de chaque ligne par "blou" ?

- Quelle commande permet d'afficher un fichier en remplaçant le deuxième
  "t" de chaque ligne par un espace ?

[//]: # (sed 's/t/blou/' fichier)

[//]: # (sed 's/\(.*t.*\)t/\1 /' fichier)


# Scripts shell 

## Rappels sur les scripts shell 

Pour rappel, un script shell est un fichier texte exécutable, contenant des 
commandes shell (et éventuellement des variables et structures de contrôle), 
par exemple :

```bash
#!/bin/sh

# check command line arguments
if [ $# -ne 1 ] ; then
    echo "usage: $0 <nb hello>"
    exit
fi
NB_HELLO=$1

# say hello
for i in `seq ${NB_HELLO}`; do
    echo "hello (${i} of ${NB_HELLO})"
done
```

* * *

- Quelle commande permet de rendre un fichier exécutable ?

- Quelle commande permet d'exécuter un fichier exécutable ?

- Écrivez un script simple qui affiche "Bonjour" et la liste des éléments
  commençant par un "h" du dossier "/etc/" (mais pas des sous-dossiers). 

* * *

- Écrivez un script qui affiche le contenu des fichiers dont le nom est donné
  en argument du script, par exemple :

```text
$ ./TP_unix_script2.sh *.txt

**** a.txt ****
toto
tata

**** b.txt ****
toto
tata
```

## Script shell : mesurer des temps d'exécution

Si ce n'est pas déjà fait, forkez/clonez le dépôt de code fourni (en suivant
les instructions données dans la section [environnement de
travail](../env/index.html)). Allez dans le dossier
`ulco-gl1-etudiant/tp-unix`. Le fichier `fibo.cpp` permet de calculer le
$n^{e}$ terme de la suite de Fibonacci, où $n$ est passé en argument.

- Compilez le fichier `fibo.cpp`, exécutez-le et vérifiez la sortie produite :

```text
$ c++ -o fibo.out fibo.cpp

$ ./fibo.out 42
n; fibo(n); duration
42; 267914296; 1.0023
```

* * *

- Écrivez un script qui calcule les 42 premiers termes de la suite de
  Fibonacci, en utilisant `fibo.out`, et sort son résultat dans un fichier
  `fibo.csv` selon la forme suivante. Indication : utilisez la commande
  `tail`.

```text
$ cat fibo.csv 
1; 1; 7.9e-08
2; 1; 1.43e-07
3; 2; 1.13e-07
4; 3; 1.76e-07
5; 5; 1.52e-07
6; 8; 1.86e-07
7; 13; 3.25e-07
8; 21; 3.28e-07
...
```

* * *

- Ouvrez votre fichier `fibo.csv` dans libreoffice et tracez la courbe du
  temps d'exécution en fonction de $n$. Vous avez droit à la souris pour cette
  question et la suivante... 

* * *

![](files/TP_unix_fibo_office.png)

* * *

- Exportez votre graphique dans un fichier `fibo_office.png`. Que faut-il
  (re)faire pour obtenir ce graphique sur une autre machine avec des temps
  d'exécution différents ?

- Modifiez votre script pour générer automatiquement un graphique
  `fibo_gnuplot.png`, avec la commande suivante. 

```bash
gnuplot -e "set out 'fibo_gnuplot.png'; \
    set terminal png size 640,360; \
    set style data linespoints; \
    set grid xtics ytics; \
    plot 'fibo.csv' using 1:3"
```

* * *

![](files/TP_unix_fibo_gnuplot.svg){width="70%"}

* * *

- Que faut-il (re)faire désormais pour obtenir ce graphique sur une autre
  machine avec des temps d'exécution différents ?


## Script shell : télécharger des images

La page <https://www.mackinacbridge.org/fares-traffic/bridge-cam> contient des
images qu'on voudrait récupérer.

- Écrivez un script permettant de récupérer toutes les images `.jpg` de cette
  page web. Indications : 

    - récupérer la page html avec `wget` 
    - en extraire les fichiers `.jpg`, par exemple en remplaçant les `"` et les
      `?` par des sauts de ligne `\n` puis en filtrant les lignes 
    - récupérer les fichiers `.jpg` trouvés avec `wget`

* * *

![](files/TP_unix_photo_eog.png)

* * *

- Modifiez votre script afin qu'il s'utilise de la façon suivante :

```text
$ ./download_all_jpg.sh 
usage: ./download_all_jpg.sh <url> <output directory>
example: ./download_all_jpg.sh "https://www.mackinacbridge.org/fares-traffic/bridge-cam" output_jpg

$ ./download_all_jpg.sh "https://www.mackinacbridge.org/fares-traffic/bridge-cam" output_jpg
https://www.mackinacbridge.org/wp-content/uploads/2016/10/DI-02213-0042-slideshow.jpg
  -> output_jpg/DI-02213-0042-slideshow.jpg
https://www.mackinacbridge.org/wp-content/uploads/2016/10/DI-02213-0042-slideshow.jpg
  -> output_jpg/DI-02213-0042-slideshow.jpg
...
```

## Script shell : générer une vidéo 

L'image
<https://mackinacbridge.org/wp-content/camimages/MacBridge_image4_medium.jpg>
est actualisée toutes les 60 secondes, à partir d'une webcam (si cette webcam
est HS, trouvez-en une autre).

- Écrivez un script qui récupère un nombre donné de ces images et en génère un
  gif animé. Pour cela, vous pouvez utiliser les programmes :

    - `wget` pour récupérer les images
    - `sleep` pour attendre une nouvelle image
    - `convert` pour créer le gif animé

* * *

![](files/TP_unix_video.gif)

* * *

- Modifiez votre script afin qu'il s'utilise de la façon suivante :

```text
$ ./download_and_loop.sh 
usage: ./download_and_loop.sh <nb loops> <output directory>
example: ./download_and_loop.sh 5 output_loop

$ ./download_and_loop.sh 5 output_loop
image downloaded to output_loop/Photo_1.jpg
waiting 60 seconds...
image downloaded to output_loop/Photo_2.jpg
waiting 60 seconds...
...
```

