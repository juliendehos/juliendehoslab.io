---
title: GL1 TP2 - Git
date: 2023-08-22
---

[Git](https://git-scm.com/book/fr/v2) est un logiciel de gestion de code
source. Il permet notamment de journaliser du code, gérer des branches, 
travailler en équipe…

> Attention à ne pas créer ou copier un dépôt git dans un dépôt git. Un dépôt
> git est un dossier qui contient un dossier `.git` (vous pouvez le vérifier
> avec un `ls -a`).

# Dépôt local

## Journaliser du code

Dans ce premier exercice, vous allez utiliser git pour journaliser votre code,
c'est-à-dire sauvegarder différentes versions successives de vos fichiers.
Ceci vous permettra de voir l'historique des modifications, voir les
différences entre les versions, revenir à une version précédente, annuler
des modifications…

* * *

- Créez un dossier `casse` (en dehors de votre dossier `ulco-gl1-etudiant`) et
  copiez-y le fichier `ulco-gl1-etudiant/tp-git/changeCasse.cpp`.

- Dans votre dossier `casse`, initialisez un dépôt git avec la
  commande `git init`. 

- Regardez l'état de votre dépôt avec la commande `git status`. Que se
  passe-t-il ?

- Demandez à git de versionner votre fichier `changeCasse.cpp` avec la commande
  `git add`. Faites un `git status`; que se passe-t-il ?

- Enregistrez la version courante de vos fichiers avec un `git commit`;
  mettez comme message de commit "première version du programme de conversion
  de casse".

* * *

- Regardez l'état de votre dépôt avec la commande `git status` et avec la
  commande `git log`.

- Créez un script `compile.sh` qui compile votre programme `changeCasse.cpp`.
  Ajoutez votre script au dépôt git et committez. Vérifiez avec un `git log`
  que vous avez bien vos deux commits.

- Modifiez `changeCasse.cpp` n'importe comment. Vérifiez avec `git status` que
  le fichier a bien été modifié et affichez les modifications avec `git diff`.
  Annulez les modifications courantes avec la commande `git checkout --
  changeCasse.cpp` (vérifiez-le en ouvrant le fichier).

* * *

- Supprimez `changeCasse.cpp` avec `git rm` et renommez `compile.sh` en `toto`
  avec `git mv`. Committez.

- Annulez le dernier commit avec `git reset` (utilisez le log pour connaitre
  l'identifiant du commit auquel revenir). Affichez le log et l'état du dépôt;
  que se passe-t-il ? Annulez complètement les modifications courantes.

- Ajoutez un fichier `.gitignore` pour ignorer les fichiers autogénérés.
  Vérifiez avec un `git status` et ajoutez le `.gitignore` au dépôt (et committez).

## Gérer des branches

Actuellement, votre dépôt `casse` contient deux commits (ou plutôt trois, mais
j'ai pas envie de refaire les schémas), que l'on peut schématiser ainsi :

![](files/TP_git_branches_1.svg){width="20%"}

* * *

En fait, git place également des étiquettes sur les commits. L'étiquette `HEAD`
indique le commit courant (auquel les fichiers courants correspondent),
L'étiquette `main` (anciennement `master`) indique le dernier commit de la
branche principale. 

![](files/TP_git_branches_2.svg){width="20%"}

* * *

- Affichez le log détaillé avec `git log --graph --decorate`. Affichez les
  branches avec `git branch`.

- Revenez au premier commit en utilisant `git checkout <commit>`. Affichez le
  log détaillé et les branches.

![](files/TP_git_branches_3.svg){width="20%"}

* * *

Vous allez maintenant modifier votre programme `changeCasse.cpp` selon deux
versions différentes : une version "ligne de commande" et une version
"entrée/sortie standard". Pour cela, vous allez développer les deux versions en
parallèle dans des branches différentes.

- Revenez sur la branche `main`. Créez une branche `cli` avec la commande `git branch
  cli` et passez sur cette branche.

- Modifiez `changeCasse.cpp` pour qu'il utilise désormais un texte passé en
  argument de la ligne de commande.

```text
$ ./changeCasse.out "Toto, Tata"
tOTO, tATA
```

* * *

- Committez votre modification. Affichez le log détaillé.

![](files/TP_git_branches_4.svg){width="30%"}

- Revenez sur la branche `main`, créez une branche `es` et passez sur cette
  branche.

* * *

- Modifiez `changeCasse.cpp` pour qu'il utilise désormais un texte passé par
  l'entrée standard. Committez.

```text
$ echo "Toto, Tata" | ./changeCasse.out 
tOTO, tATA
```

![](files/TP_git_branches_5.svg){width="30%"}

* * *

- Revenez sur la branche `main`, créez un fichier `README.md`, ajoutez-le au
  dépôt et committez. 

![](files/TP_git_branches_6.svg){width="40%"}

* * *

- Dans la branche `main`, intégrez la branche `es` avec la commande `git merge
  <branche>`.  Affichez le log détaillé et vérifiez qu'un commit fusionnant les
  deux branches est créé.

![](files/TP_git_branches_7.svg){width="40%"}

# Dépôts distants

Vous allez maintenant créer des projets synchronisés avec un serveur, ce qui vous permettra :

- d'avoir une sauvegarde (journalisée) de vos projets
- de récupérer et mettre à jour vos projets à partir de n'importe quel ordinateur connecté à internet
- de travailler en équipe
- de faciliter le travail d'espionnage des services secrets et des robots logiciels à usage commercial

* * *

Notez que git est un système décentralisé. Un projet est stocké dans un dépôt
qui peut-être copié (cloné). Toutes les copies ont les mêmes fonctionnalités.
Pour des raisons pratiques, on crée souvent une copie
sur un serveur et on synchronise les dépôts «locaux» via ce dépôt «distant».
Cependant, on peut tout à fait synchroniser entre dépôts «locaux» ou
utiliser plusieurs dépots «distants».

## Héberger du code

- Vérifiez que vous avez un compte sur le serveur GitLab de la fac et que vous
  arrivez à vous connecter à l'[interface
web](https://gitlab.dpt-info.univ-littoral.fr).

## Cloner un dépôt distant 

La façon la plus simple pour commencer un projet est de créer un dépôt distant
puis de le récupérer (cloner) sur votre machine locale.

- Connectez-vous sur la page web du serveur et créez un nouveau dépôt
  `casse_bash`.

- Pour cloner le dépôt distant, allez dans un dossier qui n'est déjà versionné
  par git et tapez la commande suivante (en spécifiant votre login).

```text
git clone https://<serveur>/<login>/casse_bash.git
```

* * *

- Allez dans le dossier `casse_bash` et écrivez un script `changeCasse.sh`
  qui, pour l'instant, affiche "hello" (pensez à mettre les droits d'exécution
  et à tester). 

- Ajoutez ce fichier au dépôt (local) et committez. 

- Allez sur la «page web du dépôt distant» et vérifiez que rien n'a changé.

- La commande `git push` permet d'envoyer les commits locaux «sur le
  serveur».  Exécutez-la et vérifier sur la page web que le dépôt distant
  contient désormais votre fichier.

* * *

- Tapez la commande `git log --decorate`. À quoi correspondent les étiquettes
  rattachées au commit courant ?

- Modifiez le fichier `changeCasse.sh` de façon à implémenter le changement de
  casse en bash :

```bash
#!/bin/sh
tr '[a-z][A-Z]' '[A-Z][a-z]'
```

- Committez, pushez et vérifiez sur la page web.

## Créer un dépôt distant à partir d'un dépôt local

Le but de cet exercice est de mettre votre projet `casse` local sur le serveur.

- Connectez-vous sur la page web du serveur et créez un nouveau dépôt `casse`.

* * *

- Dans un terminal, allez dans votre dossier `casse` et tapez la commande suivante, en spécifiant votre login.

```text
git remote add origin https://<serveur>/<login>/casse.git
```

- La commande précédente indique au dépôt local l'existence du dépôt distant
  mais pour l'instant, vous n'avez encore rien modifié sur le dépôt distant.
  Vérifiez-le en consultant la «page web du dépôt distant».

* * *

- Dans votre dépôt local, passez sur la branche `main` et faites un `git
  push`. Git vous signale une erreur : la branche locale n'est pas rattachée à
  une branche distante.  Vérifiez-le avec un `git log --decorate`.

- Rattachez la branche `main` locale à la branche `main` distante avec la
  commande suivante. 

```text
git push -u origin main
```

- Vérifiez que la branche distante est bien rattachée, avec un `git log
  --decorate`.  

* * *

Vous pouvez maintenant faire des `git push` classiques sur cette branche. Notez
que les branches `cli` et `es` sont toujours locales; si vous voulez les
envoyez sur le serveur, il faudra également faire un `git push -u` pour
chacune.

## Travailler en équipe

Le but de cet exercice est de voir comment travailler à plusieurs sur un
projet, via un dépôt distant.

* * *

Push/pull sans conflit :

- Allez dans un dossier vide et clonez `casse_bash` dans deux dossiers
  `casse_bash_equipe1` et `casse_bash_equipe2`.

- Dans `casse_bash_equipe1`, créez un fichier `README.md` contenant une ligne
  de texte de test. Ajoutez, committez, pushez.

- Vérifiez sur la page web que le fichier a bien été ajouté sur le serveur.

- Dans `casse_bash_equipe2`, faites un `git pull` pour récupérer les
  modifications sur le serveur. Vérifiez que vous avez bien récupéré le fichier
`README.md`.

* * *

Push/pull avec conflit :

- Dans `casse_bash_equipe1`, modifiez/committez/pushez le fichier `README.md`. 

- De même, dans `casse_bash_equipe2`, modifiez/committez/pushez le fichier
  `README.md`. Git indique que le dépôt distant a changé et que vous devez
  vous mettre à jour (pull) avant de pouvoir pusher.

- Pullez. Git indique un conflit dans le fichier `README.md`.

- Ouvrez le fichier et regardez comment git indique les lignes en
  conflit. Modifiez le fichier pour le mettre dans l'état que vous voulez puis
  committez/pushez.

- Dans `casse_bash_equipe1`, pullez et vérifiez que vous avez bien le même
  fichier que dans `casse_bash_equipe2`.

## Contribuer à un projet open-source

Classiquement, un projet open-source est hébergé sur une plateforme, comme
github ou gitlab, qui permet de récupérer le code source. Cependant, pour des
raisons évidentes, il faut des droits particuliers pour pouvoir modifier
directement ce dépôt.  Pour contribuer à un projet open-source, la démarche
classique est de créer une copie (appelée "fork") du dépôt principal sur son
compte personnel puis de modifier sa copie personnelle et de demander d'intégrer
les modifications dans le dépôt principal ("pull request" ou "merge request").

- Pour cet exercice, mettez-vous en binôme (vous aurez besoin de deux comptes gitlab).
- Sur le premier compte, créez un dépôt principal public contenant un fichier `README.md`.

* * *

Soumettre une modification au dépôt principal :

- Sur le deuxième compte, forkez le dépôt principal, clonez-le, modifiez le
  fichier `README.md` et pushez-le **dans une branche `super-modif`**.
- Sur gitlab, envoyez une "merge request" au dépôt principal.
- Toujours sur gitlab, passez sur le compte principal et acceptez la "merge
  request".

* * *

Synchroniser un dépôt forké avec le dépôt principal :

- Dans le dépôt forké, repassez dans la branche `main` et vérifiez dans les
  logs que vous n'avez pas votre modification (vous pourriez merger directement
  votre branche `super-modif` mais on va plutôt se resynchroniser avec le dépôt
  principal).
- Ajoutez à votre dépôt un `remote` que vous appelerez `upstream`, vers le
  dépôt principal : `git remote add upstream https://...`
- Récupérez les dernières modifications de `upstream` localement : `git fetch
  upstream`
- Fusionnez les modifications de `upstream` dans votre `main` et vérifiez
  dans les logs que vous avez votre modification.
- Pushez le `main` et vérifiez sur gitlab que le dépôt forké contient bien la
  modification.


## Modules git

Git permet d'intégrer des dépôts dans un dépôt. Ceci peut être utile pour gérer
des dépendances de code.

- Créez un dépôt `people` sur gitlab et clonez-le (en dehors du dossier
  `ulco-gl1-etudiant`).

- Copiez-y les fichiers `people.cpp` et `people.csv` fournis dans le dossier
  `ulco-gl1-etudiant/tp-git` et ajoutez-les au dépôt avec `git add`.

- Essayez de compiler avec la commande suivante et vérifiez qu'il manque une
  dépendance.

```text
c++ -pthread -o people people.cpp
```

* * *

- Ajoutez un module git pour intégrer le dépôt de [Fast C++ CSV
  Parser](https://github.com/ben-strasser/fast-cpp-csv-parser), avec la
commande suivante.

```text
git submodule add https://github.com/ben-strasser/fast-cpp-csv-parser ext/fccp
```

- Vérifiez qu'un dossier `ext/fccp` a été créé et contient le code source de
  Fast C++ CSV Parser.

- Regardez comment sont indiqués les modules dans les fichiers `.git/config`
  et `.gitmodules`.

- Vérifiez que la compilation fonctionne avec la commande suivante.

```text
c++ -I./ext/fccp -pthread -o people people.cpp
```

- Faites un `git status` puis **committez/pushez votre projet**. 

* * *

- Supprimez votre dossier `people` et clonez votre dépôt gitlab à nouveau.

- Essayez de compiler. Que se passe-t-il ? Que contient le dossier `ext/fccp` ?

- Chargez les modules avec la commande suivante puis mêmes questions.

```text
git submodule update --init
```

* * *

- Supprimez votre dossier `people` et clonez votre dépôt gitlab à nouveau
  **mais cette fois en ajoutant l'option** `--recursive`.

- Essayez de compiler. Que se passe-t-il ? Que contient le dossier `ext/fccp` ?


## S'il vous reste du temps

- Mettez à jour vos dépôts de TP (GL1, PF) pour y ajouter le travail que vous
  avez réalisés lors des séances précédentes. Et pensez à les tenir à jour
  désormais.

- Testez la commande `git stash`.


