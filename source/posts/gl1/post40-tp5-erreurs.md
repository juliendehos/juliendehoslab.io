---
title: GL1 TP5 - Gestion d'erreurs
date: 2023-08-23
---

On peut distinguer deux "types" d'erreur : 

- les erreurs de programmation (bugs) : ces erreurs ne devraient pas se
  produire et doivent être corrigées (on peut les détecter par des assertions,
  tests unitaires...).

- les erreurs d'utilisation : ces erreurs peuvent se produire et doivent être
  gérées (par des exceptions, codes d'erreur...).


## Assertions

Les assertions permettent de vérifier, à l'exécution, des conditions à
l'intérieur du code (préconditions, postconditions, invariants...).


- Allez dans `ulco-gl1-etudiant/tp-erreurs/fibo` et lancez VSCode.

- Regardez dans le fichier `src/Fibo.cpp` comment sont utilisées les assertions.

- Compilez et exécutez le programme. Vérifiez que l'assertion "de test" échoue.

* * *

- Passez en mode Release (dans la barre de status), compilez, exécutez et vérifiez que les assertions ne sont plus vérifiées.

- Repassez en mode Debug, commentez l'assertion de test et vérifiez que le programme s'exécute correctement.


## Gestion des TODO

Les `TODO` sont des "choses à faire" dans un projet. En fait, il peut s'agir
également de `BUG`, `FIXME`...  On peut les définir sous forme de ticket
Gitlab, les noter dans un `README.md` ou encore les noter en commentaires de
code. Cette dernière méthode est très simple à mettre en œuvre (éditeur de
code, `grep`...).

- Toujours dans le projet `fibo`, ouvrez la section TODOS du panneau Explorer.

- Retrouvez les TODO du projet et réalisez-les.

- Évidemment : compilez et testez.

* * *

![](files/tp-erreurs-todo.png){width="90%"}


## Codes d'erreur

Pour indiquer une erreur, une fonction peut retourner un code d'erreur ou
mettre un code d'erreur dans une variable globale. Ce mécanisme est beaucoup
utilisé dans les bibliothèques C et permet de gérer explicitement les erreurs.
Les codes d'erreurs sont également utilisés pour la valeur retour du programme
principal (fonction `main`). 

- Allez dans le dossier `tp-erreurs/errno`, regardez et testez les fichiers fournis.

- Comment sont gérées les erreurs avec `sqrt` ? Avec `strtod` ? Quels 
  inconvénients ?

* * *

- Exécutez le programme manuellement et regardez sa valeur de retour.


```text
$ ./a.out 2
1.41421

$ echo $?
0

$ ./a.out foobar
not a number

$ echo $?
255
```



## Exceptions

Les exceptions permettent de gérer des erreurs d'utilisation et sont
généralement préférables aux codes d'erreurs. Elles permettent notamment de
rendre le code plus lisible (en séparant le cas nominal des cas d'erreur) et
plus robuste (pas de risque d'oublier de vérifier les codes d'erreur)...

* * *

On peut distinguer deux types d'exceptions. Les exceptions de *runtime* sont
des erreurs de fonctionnement du système (par exemple : une allocation mémoire
a échouée); elles sont généralement déjà représentées dans le langage de
programmation (par exemple : `std::bad_alloc` en C++).  Les exceptions de
*logique métier* sont des erreurs du domaine cible du projet (par exemple :
dans le SI d'un garage automobile, on a demandé de réparer le pot d'échappement d'une
voiture électrique); on définit généralement nos propres classes pour les
représenter.

* * *

- Testez le programme `replicate42/replicate42a.cpp` fourni :

```text
$ ./replicate42a.out 
enter num: 3
num = 3
42 42 42 
```

* * *

- Complétez ce programme de façon à gérer les erreurs détectées :

```text
$ ./replicate42a.out 
enter num: -1
error: negative

$ ./replicate42a.out 
enter num: 0
num = 0
error: too low

$ ./replicate42a.out
enter num: 37
error: 37

$ ./replicate42a.out
enter num: 44
num = 44
error: too high
```

* * *

- Même question mais en utilisant des exceptions (`replicate42/replicate42b.cpp`).

- Capturez également les exceptions de *runtime* :

```text
$ ./replicate42b.out 
enter num: foobar
std::exception: stod
```

* * *

> La gestion d'erreurs est importante dans un projet. N'hésitez à vous
> documenter sur le sujet. Par exemple en C++ : 
> 
> - [Exceptions (cppreference.com)](https://en.cppreference.com/w/cpp/language/exceptions)
> - [Exceptions and Error Handling (isocpp.org)](https://isocpp.org/wiki/faq/exceptions)
> - [Error handling (C++ Core Guidelines)](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#S-errors)


## Logs

Les logs permettent d'avoir une trace de l'exécution d'un programme (via un
affichage direct, un fichier de logs...).  Les logs peuvent indiquer différents
types de message (INFO, WARNING, ERROR) voire même interrompre le programme
(FATAL). 

- En utilisant [glog](http://rpg.ifi.uzh.ch/docs/glog.html), ajoutez quelques
  logs dans la classe `MyWindow` du projet `tp-erreurs/selectfile`.

- Testez l'exécution du programme et le paramétrage des logs via les variables
  d'environnement (niveau de logs, dossier de sortie, sortie vers `stderr`...). 

* * *

```text
$ cat selectfile.INFO 
Log file created at: 2020/07/11 19:03:50
Running on machine: nixos
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
I0711 19:03:50.111076 11213 MyWindow.cpp:22] starting app
I0711 19:03:54.897243 11213 MyWindow.cpp:41] selection ok (favicon.ico)
W0711 19:03:57.161931 11213 MyWindow.cpp:44] selection cancelled
I0711 19:03:58.065423 11213 MyWindow.cpp:26] ending app
```

## Exercice d'application

Écrivez un projet `reverselines` qui permet de lire les lignes d'un fichier en
mémoire puis de les afficher dans l'ordre inverse. Pour cela, utilisez CMake,
Glog, les exceptions et le type `Error` suivant.

```cpp
enum class Error { NoSuchFile, EmptyFile };
```

* * *

Exemples de cas d'erreurs :

```sh
$ GLOG_log_dir="." ./build/reverselines 
usage: <filename>

$ GLOG_log_dir="." ./build/reverselines foobar
E0712 22:09:10.401561 21601 main.cpp:57] no such file

$ touch foobar

$ GLOG_log_dir="." ./build/reverselines foobar
E0712 22:09:24.551084 21616 main.cpp:60] empty file
```

* * *

Exemple de cas nominal :

```sh
$ echo "foo" > foobar ; echo "bar" >> foobar

$ GLOG_log_dir="." ./build/reverselines foobar
bar
foo

$ cat reverselines.INFO 
Log file created at: 2020/07/12 22:10:40
Running on machine: nixos
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
I0712 22:10:40.857177 21667 main.cpp:11] readLines (foobar)
I0712 22:10:40.857494 21667 main.cpp:30] printReverseLines (2 lines)
```

## Debugger

Le debugger est un outil classique pour trouver et corriger des bugs. Il s'agit
d'un programme qui lance notre programme tout en surveillant son exécution
(code source, mémoire, pile d'appels...).

- Dans le projet `fibo`, ouvrez l'onglet `Debug`, créez un fichier
  `launch.json` et ajoutez une configuration `gdb` (vérifiez les entrées
`program`, `arguments`...).

- Lancez le debugging. Vérifiez que le programme s'exécute correctement.

- Ajoutez quelques points d'arrêt et relancez un debugging. Vérifiez que
  l'exécution s'arrête sur les points d'arrêt et que vous voyez le code, la
  mémoire, la pile d'appel, etc.

* * *

![](files/tp-erreurs-debug.png){width="90%"}


