---
title: GL1 CM - Numérique et environnement
date: 2023-08-22
---


## Motivation

- quelques rappels sur le changement climatique
- rapport avec l'informatique/numérique
- rapport avec votre Licence Informatique


## À propos de ce cours

- en attendant le [vrai module prévu par le ministère](https://www.enseignementsup-recherche.gouv.fr/fr/plan-climat-biodiversite-et-transition-ecologique-de-l-enseignement-superieur-et-de-la-recherche-91292)
- c'est purement informatif
- n'hésitez pas à intervenir : questions, compléments, corrections...
- (je ne suis pas un expert du domaine)


# Changement climatique

## Quiz

- Quelle est la différence entre le climat et la météo ?
- Comment a évolué la température moyenne depuis l'ère pré-industrielle ?
- Quels sont conséquences du réchauffement sur les phénomènes naturels/météo ?
- Quels sont les principaux Gaz à Effet de Serre qui contribuent au réchauffement climatique ?
- Pourquoi les GES renforcent le réchauffement ?
- Quels sont les principales activités humaines qui émettent des GES ?
- Quels sont les pays les plus responsables des GES ?


## Climat et météo

![[Tout comprendre (ou presque) sur le climat](https://calypso.univ-littoral.fr/permalink/33BULCO_INST/1f1llk9/alma991001441832404626)](files/env-bonpote-climat-meteo.png){width="70%"}

* * *

![Prévision météo du 05/09/2022 pour Calais, [meteocity](https://www.meteocity.com/france/calais-v3029162)](files/env-meteo-calais.png){width="50%"}

* * *

![Climat à Calais, [wikipedia](https://fr.wikipedia.org/wiki/Calais#Climat)](files/env-climat-calais.png){width="80%"}



## Réchauffement climatique

<iframe width="640" height="480" 
  src="https://www.youtube.com/embed/8nzRXxPnlPQ" 
  title="Comment le réchauffement climatique va bouleverser l’humanité (ft. Le Réveilleur)" 
  frameborder="0" 
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>

* * *

- le climat se réchauffe (en lien avec les GES) :

![](files/env-lemonde1-01-temperature.png){width="100%"}

* * *

- le réchauffement n'est pas homogène selon les régions du monde :

![](files/env-lemonde1-03-evolution-monde.png){width="100%"}

* * *

- les étés sont plus chauds et plus extrèmes :

![](files/env-lemonde1-04-distribution-etes.png){width="100%"}

* * *

- exemple de la surmortalité de la canicule de 2003 :

![](files/env-lemonde1-05-canicule-2003.png){width="100%"}

* * *

- la transpiration permet à l'organisme de se refroidir :

![](files/env-lemonde1-06-transpiration.png){width="100%"}

* * *

- un air trop chaud/humide ne permet pas la transpiration :

![](files/env-lemonde1-07-temp-humidite.png){width="100%"}

* * *

- des régions très peuplées seraient inhabitables :

![](files/env-lemonde1-08-carte-chaleur.png){width="100%"}

* * *

- au niveau des océans, les précipitations augmenteront :

![](files/env-lemonde1-09-humidite-ocean.png){width="100%"}

$\Rightarrow$ inondations...

* * *

- au niveau des continents, les précipitations diminueront : 

![](files/env-lemonde1-10-humidite-terre.png){width="100%"}

$\Rightarrow$ aridité des sols, sécheresses, feux de forets...

* * *

- impact sur les populations des changements de précipitations :

![](files/env-lemonde1-11-carte-precipitations.png){width="100%"}

* * *

- élévation du niveau de la mer :

![](files/env-lemonde1-15-niveau-mer.png){width="100%"}

par fonte des glaces terrestres et dilatation de l'eau

* * *

- augmentation des submersions marines :

![](files/env-lemonde1-16-submersion-marine.png){width="100%"}

* * *

- acidifications des océans :

![](files/env-lemonde1-18-acidification-oceans.png){width="100%"}

$\Rightarrow$ destruction des récifs coraliens...

* * *

- impact sur les populations vivant de la pêche :

![](files/env-lemonde1-19-peche.png){width="100%"}

* * *

- nombreuses conséquences pour l'humanité :

![](files/env-lemonde1-21-consequences.png){width="100%"}






## Causes du réchauffement climatique

* * *

- forçage radiatif :

![](files/env-forcage-radiatif.jpg){width="80%"}

* * *

- vie du CO2 atmosphérique :

![](files/env-vie-ges.jpg){width="80%"}

* * *

- attribution du réchauffement climatique :

![](files/env-timeline-attribution-ges.png){width="60%"}

* * *

- attribution actuelle :

![](files/env-timeline-attribution-ges.jpg){width="90%"}

* * *

## Origine des GES

<iframe width="640" height="480" src="https://www.youtube.com/embed/GVJRZqI6h2k"
  title="Qui réchauffe le climat (et comment) ? Ft. Le Réveilleur"
  frameborder="0"
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>

* * *

![](files/env-lemonde2-02-rapports-giec.png){width="100%"}

* * *

![](files/env-lemonde2-01-rechauffement-anthropique.png){width="100%"}

* * *

![](files/env-lemonde2-07-concentration-co2.png){width="100%"}

* * *

- le potentiel de réchauffement d'un gaz à effet de serre :

![](files/env-lemonde2-04-eqco2.png){width="100%"}

* * *

- émissions en tonnes de gaz :

![](files/env-lemonde2-05-emissions-ges.png){width="100%"}

* * *

- émissions en tonnes équivalent CO2 :

![](files/env-lemonde2-05-emissions-eqco2.png){width="100%"}

* * *

![](files/env-lemonde2-05-repartition-ges.png){width="100%"}

$\Rightarrow$ 2 430 Gt entre 1850 et 2019

* * *

![](files/env-lemonde2-06-origines-ges.png){width="100%"}

* * *

![](files/env-lemonde2-17-secteurs.png){width="100%"}

* * *

![](files/env-lemonde2-10-2019-co2.png){width="100%"}

* * *

![](files/env-lemonde2-11-habitant-co2.png){width="100%"}

* * *

![](files/env-lemonde2-14-industrie.png){width="100%"}

* * *

![](files/env-lemonde2-09-cumul-co2.png){width="100%"}

* * *

- différentes façons de voir le problème, et les responsabilités :

![](files/env-lemonde2-15-choix.png){width="100%"}


# Il n'y a pas que le climat

## Quiz

- Qu'est-ce que l'IPBES ?
- De combien de décès prématurés est responsable la pollution atmosphérique en France ?
- Qu'est-ce que l'énergie ? Un Joule ? Un Watt ? Un Watt-heure ?
- Quelle est la puissance électrique consommée/produite en France ?
- Quelle est la consommation électrique annuelle française ?


## Pollution

- [dégradation d'un écosystème par l'introduction d'entités (physiques,
  chimiques ou biologiques), ou de radiations altérant le fonctionnement de cet
écosystème](https://fr.wikipedia.org/wiki/Pollution)

- quelques exemples :
  - qualité de l'air : particules fines, ozone, oxydes d'azote...
  - pollutions industrielles/agricoles : métaux lourds, PFAS, pesticides
  - pollution sonore/lumineuse...
  - plastiques
  - (GES)

* * *

- les activités humaines produisent de nombreuses pollutions, avec de
  nombreuses conséquences ([9 millions de morts chaque année dans le
monde](https://www.lemonde.fr/planete/article/2022/05/18/la-pollution-est-responsable-de-9-millions-de-morts-chaque-annee-dans-le-monde_6126552_3244.html))

- à considérer tout au long du cycle de vie d'un produit/service : 
  - extraction des matières premières
  - production
  - utilisation
  - fin de vie 


## Biodiversité

- variété des formes de vie sur la Terre

- IPBES : "GIEC de la biodiversité"

* * *

- importance de la biodiversité ([rapport IPBES 2022](https://www.goodplanet.info/2022/07/08/rapport-ipbes-exploitation-especes-sauvages/)) :

    - l'humanité utilise plus de 50 000 espèces sauvages pour se nourrir, se chauffer ou se soigner 

    - 1 personne sur 5 dépend des espèces sauvages pour se nourrir ou en tire directement des revenus

    - les trafics d’espèces sauvages se classent au 3e rang des trafics internationaux illégaux en valeur (entre 69 et 199 milliards de dollars)

    - (notion d'éthique/morale)

* * *

- chute de la biodiversité :

    - destruction des milieux

    - exploitation des espèces sauvages par les humains (34 % des stocks de
      poissons, 1 341 mammifères sauvages, 12 % des espèces d'arbres sauvages)

    - changement climatique

* * *

- des solutions existent :

    - réduire la pêche illégale

    - supprimer les subventions néfastes, soutenir la petite pêche

    - mettre en place des certifications pour l'exploitation forestière...


## Ressource en eau

* * *

- réserves d'eau :

![](files/env-water-stores.jpg){width="90%"}

* * *

- cycle de l'eau :

![](files/env-water-fluxes.jpg){width="90%"}

* * *

- utilisation de l'eau :

    - industrie

    - agriculture

    - usage domestique

    - consommation type : entre 1000 et 2000 $m^3$/personne/an

* * *

- l'[empreinte eau](https://fr.wikipedia.org/wiki/Empreinte_eau)

    - volume total d'eau virtuelle utilisée pour produire un produit ou un service

    - eau verte : eau de pluie stockée dans le sol

    - eau bleue : eau captée pour les usages domestiques et agricoles (lacs, rivières, aquifères)

    - eau grise : eau polluée par les processus de production


## Minérais

* * *

- [roches contenant des minéraux exploitables pour l'industrie](https://fr.wikipedia.org/wiki/Minerai)

- exemple des métaux : 
  - fer, cuivre, aluminium, or, argent, plomb, zinc...
  - cobalt, lithium, gallium, tungstène, indium...
  - terres rares : scandium, yttrium, lanthanides (néodyme, dysprosium...)

* * *

- évolution de la consommation mondiale de métaux :

![](files/env-evolution-metaux.png){width="100%"}

* * *

- problèmes :

    - ressources limitées

    - coût d'extraction (pollutions, énergie, conditions de travail...)

    - recyclage pas toujours facile (alliages, petites quantités, filières de
      recyclage...)

    - "paradoxe des terres rares" : beaucoup utilisées dans les "technologies vertes" mais exploitation polluante

* * *

![Mine de cobalt, Kolwezi, RDC](files/env-cobalt-kolwezi.png){width="70%"}

* * *

![Champs de lithium, Atacama, Chili](files/env-lithium-atacama.jpg){width="70%"}

* * *

<iframe width="640" height="480" 
  src="https://www.youtube.com/embed/i0nc10DKk4g?si=jnIU5Cogt3LzjxKW"
  title="Dezoom - Allemagne : exploiter du charbon à ciel ouvert | ARTE" 
  frameborder="0" 
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>



## Énergie

- [grandeur physique qui mesure la capacité d'un système à modifier un état, à produire un travail entraînant un mouvement, un rayonnement électromagnétique ou de la chaleur](https://fr.wikipedia.org/wiki/%C3%89nergie_(physique))

- quelques unités : J, W, Wh

* * *

- exemples d'utilisation de l'énergie :
  - chaleur : chauffage, cuisson
  - mécanique : transport
  - chimique : agriculture (engrais)
  - électricité : services numériques

* * *

- Quelques ordres de grandeur approximatifs (production ou consommation) :
  - un corps humain : 100 W mécanique (+ 100 W thermique)
  - un four de cuisine : 2 kW
  - une voiture (twingo) : 60 kW
  - une éolienne : 1 à 10 MW
  - un réacteur nucléaire : 1 GWe
  - puissance consommée/produite en France : entre 50 et 100 GW
  - consommation électrique annuelle en France : 500 TWh

* * *

- quelques sources d'énergie :
  - biomasse
  - hydraulique
  - éolien
  - géothermie
  - charbon
  - pétrole
  - gaz
  - nucléaire
  - photovoltaïque

* * *

- principe d'irréversibilité et d'entropie
  - énergie primaire, énergie finale
  - efficacité d'un système
  - bilan énergétique
  - stockage...

* * *

- caractéristiques de la production d'énergie :
  - source d'énergie, vecteur d'énergie
  - énergie fossile, énergie renouvelable
  - énergie carbonée, énergie bas-carbone
  - énergie intermittente, énergie pilotable
  - ressources, coût, pollution/déchets, facilité d'utilisation...

* * *

- [consommation énergétique mondiale](https://www.alternatives-economiques.fr/consommation-energetique-mondiale-exajoules-0101201550141.html) :

![](files/env-conso-energie-monde.png){width="80%"}

* * *

- [bilan énergétique de la France](https://www.statistiques.developpement-durable.gouv.fr/edition-numerique/chiffres-cles-energie-2021/6-bilan-energetique-de-la-france) :

![](files/env-energie-france.svg){width="70%"}



# Transition écologique

## Quiz

- Comment peut-on réduire le réchauffement climatique, d'un point de vue physique ?
- Que peut-on changer dans nos modes de vie pour y arriver ?
- Quelle est l'empreinte carbone moyenne d'un français ? 
- Quel est l'objectif à 2050 pour respecter l'Accord de Paris ?
- Quelle est la différence entre sobriété et efficacité ?
- Qu'est-ce que le greenwashing ?

* * *

- Pour chaque proposition ci-dessous, est-ce une source d'électricité bas-carbone ? renouvelable ?
  - centrale à charbon
  - gaz
  - nucléaire
  - hydroélectrique
  - photovoltaïque
  - éolien
  - biomasse
  - hydrogène


## Problématique

- consensus scientifique : le réchauffement climatique anthropique met en danger les sociétés humaines

- c'est une bonne nouvelle : il "suffit" d'arrêter nos émissions de GES pour arrêter le réchauffement

- mais :
  - est-ce que "nous" voulons vraiment le faire ?
  - si oui, comment y arriver ?
  - et les autres problèmes (biodiversité, ressources, pollutions...) ?


* * *

- [scénario de transition](https://twitter.com/valmasdel/status/1566732252863778821) :

![mesures par rapport à 1850-1900 + projections](files/env-vmd-co2.png){width="90%"}

* * *

![](files/env-vmd-ges.png){width="60%"}

* * *

- [budget carbone](https://datagir.ademe.fr/blog/budget-empreinte-carbone-c-est-quoi/) :

![](files/env-ademe-budget.png){width="100%"}

* * *

- objectifs de la France (COP 21) :

![](files/env-carbone4-co2-francais.png){width="90%"}


## Atténuation et adaptation

- attenuer le changement climatique

- adapter nos sociétés au réchauffement actuel et à venir

* * *

- leviers d'action :
  - GES : réduire les emissions, augmenter les puits de carbone
  - activités humaines : sobriété (réduire), efficacité (améliorer)

![](files/env-vmd-avoid.png){width="40%"}

* * *

- où agir ?

![](files/env-vmd-leviers3.png){width="70%"}
![](files/env-vmd-leviers1.png){width="25%"}
(potentiel de réduction à 2030)

* * *

![](files/env-vmd-leviers2.png){width="60%"}

* * *

- [exemples de consommations électriques types](https://www.lemonde.fr/les-decodeurs/article/2022/10/06/combien-d-electricite-consomment-les-francais-et-comment-reduire-les-factures_6143074_4355771.html) :

![](files/env-lemonde-conso-info.png){width="50%"}


## Difficultés

- comment mesurer :

  - émission de GES
  - [empreinte carbone](https://fr.wikipedia.org/wiki/Empreinte_carbone) 
  - [empreinte écologique](https://fr.wikipedia.org/wiki/Empreinte_%C3%A9cologique)
  - [empreinte matières](https://fr.wikipedia.org/wiki/Material_input_per_unit_of_service)
  - [Analyse du cycle de vie](https://fr.wikipedia.org/wiki/Analyse_du_cycle_de_vie)

* * *

![](files/env-acv-ciraig.jpg){width="60%"}

* * *

- acceptation sociale de la transition et des efforts :

![](files/env-revenu-co2.jpg){width="90%"}

* * *

- changements individuels, changements collectifs/structurels

![<https://www.chronotrains.com>](files/env-train-isochrone.png){width="90%"}

* * *

- [effet rebond, paradoxe de Jevons](https://fr.wikipedia.org/wiki/Effet_rebond_(%C3%A9conomie)) :

> les économies d’énergie ou de ressources initialement prévues par
> l’utilisation d’une nouvelle technologie sont partiellement ou complètement
> compensées à la suite d'une adaptation du comportement de la société



## Quelques remarques sur l'électricité

- problème important : 

    - consommation importante 
    - électrification de certaines activités

- mais : 

    - ressources (cuivre, acier, lithium, terres rares)
    - intermittence, sobriété/efficacité
    - occupation des sols...



# Le secteur du numérique

## Quiz

- Quelle est la part du numérique dans les émissions mondiales de GES ?
- La consommation électrique du numérique est principalement due :
    - aux data-centers ?
    - à l'infrastructure réseau ?
    - aux terminaux utilisateurs ?
- Les émissions de GES d'un ordinateur sont principalement dues :
    - à la fabrication
    - à l'utilisation
    - à la fin de vie (recyclage, déchet)



## Problématique

- derrière la "dématérialisation", il y a de nombreux équipements :
  - ordinateurs (fixe, portable, tablette)
  - smartphones
  - objets connectés
  - serveurs, data-centers
  - équipements de réseaux

* * *

- impact environnemental :
  - consommation électrique
  - matières premières
  - eau (construction des équipements, refroidissement des data-centers)
  - fin de vie : recyclage, déchets, pollutions

* * *

- obésiciel :
  - les logiciels deviennent de plus en plus lourd
  - effet rebond du gain de performance matériel (CPU, mémoires, réseaux fibre/4G/5G...)
  - bibliothèques/frameworks avec de plus en plus de fonctionnalités
  - exemple de la configuration minimale de Windows :

    +------+-------------------+-------------------+
    |      | Windows XP (2001) | Windows 11 (2021) |
    +------+-------------------+-------------------+
    | CPU  | 233 MHz           | 2 x 1 GHz    (x8) |
    +------+-------------------+-------------------+
    | RAM  | 64 Mo             | 4 Go       (x 62) |
    +------+-------------------+-------------------+
    | Disk | 1,5 Go            | 64 Go      (x 42) |
    +------+-------------------+-------------------+



* * *

- obsolescence :
  - logicielle (obésiciel, support des anciennes versions)
  - [voire même programmée](https://www.usine-digitale.fr/article/obsolescence-programmee-de-l-iphone-apple-accepte-de-payer-500-millions-de-dollars-de-dedommagement.N936319)
  - conception de matériel spécifique (connecteurs), fragile (écran en verre) ou faiblement réparable (batterie soudée)


* * *

- pratiques commerciales et morales :
  - incitation au renouvellement du matériel (nouvelle gamme sans évolution significative)
  - omniprésence de vidéo, publicité (traffic réseau)
  - conception de logiciels addictifs (nudges)
  - exploitation des données (cookies, plateformes captives)
  - éthique des IA (utilisation, biais, [travail déguisé](https://www.lemonde.fr/pixels/article/2016/02/10/petite-histoire-des-captchas-ces-tests-d-identification-en-pleine-mutation_4862727_4408996.html))

* * *

- choix/dette technique :
  - [le logiciel "juste assez bon"](https://en.wikipedia.org/wiki/Principle_of_good_enough)
  - ["it’s not a bug, it’s a feature"](https://fr.wiktionary.org/wiki/it%E2%80%99s_not_a_bug,_it%E2%80%99s_a_feature)

![](files/env-numerique-gif.png){width="80%"}


## Quelques chiffres

- Ademe (La face cachée du numérique) :
  - les téléphones, tablettes et autres écrans consomment 10% de l'électricité en France
  - le numérique génère 4 % des émissions mondiales de GES (ce chiffre devrait doubler d'ici 2025), dont :
    - 25% dues aux data-centers
    - 28% dues aux infra-structures réseau
    - 47% dues aux équipements des consommateurs

* * *

![](files/env-enfer-minute.png){width="60%"}

* * *

![](files/env-enfer-cables.png){width="80%"}


* * *

- [exemple de consommation d'eau du data-center à Hollands Kroon](https://www.lesnumeriques.com/pro/microsoft-dans-la-tourmente-apres-avoir-sous-evalue-la-consommation-d-eau-d-un-de-ses-datacenters-neerlandais-n190041.html) :
  - autorisée initialement : 20k $m^3$
  - réelle en 2021 : 100k $m^3$

* * *

- conception d'un smartphone :

![](files/env-metaux-tel.png){width="90%"}

* * *

![](files/env-pitron-atome-tel.png){width="90%"}




## Moyens d'actions

- en tant qu'utilisateur du numérique :
  - globalement : Réduire, Réparer, Réemployer, Recycler
  - $+$ les "petits gestes"
  
- par exemple :
  - allonger durée de vie appareils, reconditionnement, recyclage, mutualisation
  - éteindre les appareils quand on ne les utilise pas
  - connexion Internet : fibre, sinon wifi, sinon 4G/5G
  - vidéo/streaming : baisser la résolution, voire audio seule
  - limiter l'usage du cloud
  - mails : réduire les destinataires/PJ/newsletters

* * *

- en tant qu'acteur du numérique :

  - volonté globale de l'entreprise, de la conception à la fin de vie
  - sobriété des fonctionnalités et des IHM
  - utiliser/contribuer au logiciel libre
  - éviter l'obésiciel
  - éco-conception "mobile first"
  - tests/preuves des logiciels 
  - réparabilité du matériel (exemple du fairphone)


* * *

- mais :

  - certains domaines cibles ne seront jamais écologiquement responsable
  - modèle économique (publicité, certaines demandes imposées par le client, collecte/analyse/vente de données)
  - difficulté de mise en place technique (+ rentabilité)
  - et aussi : respect des données personnelles, éthiques des IA, nudge/addiction...



## Quelques liens

- synthèses et recommandations sur le numérique :
  - [Pollution numérique : du clic au déclic](https://www.qqf.fr/infographie/69/pollution-numerique-du-clic-au-declic)
  - [Liste des ressources ADEME sur le numérique responsable](https://ecoresponsable.numerique.gouv.fr/publications/ressources-ademe/)
  - [Quelle démarche Green IT pour les grandes entreprises françaises ?](https://www.wwf.fr/sites/default/files/doc-2018-10/20181003_etude_wegreenit_d%C3%A9marche_green_it_entreprises_francaises_WWF-min.pdf)

* * *

- autres ressources :
  - [Mooc Impacts environnementaux du numérique](https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/1-explorer.html)
  - [Lean ICT – Les impacts environnementaux du Numérique](https://theshiftproject.org/lean-ict/)
  - [greenit.fr](https://www.greenit.fr/)
  - [éco-conception et green-it](https://www.programmez.com/dossier/presentation/ecoconception/semaine-de-leco-conception-et-du-green-it)
  - [Référentiel général d'écoconception de services numériques](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/)
  - [GR491, Le guide de Référence de Conception Responsable de Services Numériques](https://gr491.isit-europe.org/)
  - [https://climat.cned.fr](B.A.-BA du climat et de la biodiversité)



