---
title: GL1 TP7/8 - Mini-projets
date: 2023-08-23
---

> L'objectif n'est pas de terminer les projets mais de travailler correctement,
> c'est-à-dire étape par étape et avec du code propre, des tests, de la doc...


## Guessing game

- Regardez le projet de base fourni `projets/guess`.

- Implémentez un programme qui demande de deviner un nombre choisi
  aléatoirement entre 1 et 100. Le joueur a 5 essais maximum pour deviner le
  nombre. À chaque essai, le programme indique si le nombre donné est trop
  petit ou trop grand, ou si le jeu est gagné ou perdu.  Écrivez des tests
  unitaires et une documentation Doxygen. S'il vous reste du temps, ajoutez des
  logs et améliorez la gestion d'erreurs.

* * *

<video preload="metadata" controls>
<source src="files/gl1-guess.mp4" type="video/mp4" />
![](files/gl1-guess.png){width="50%"}

</video>

## Chiffrement de César

- Regardez le projet de base fourni `projets/caesar`.

- Implémentez un programme qui permet de casser le chiffrement de César. Le
  programme lit le message chiffré dans l'entrée standard, affiche la clé
  trouvée dans la sortie d'erreur et écrit le message déchiffré dans la sortie
  standard.  On considère uniquement les lettres minuscules (les autres
  caractères sont ignorés ou inchangés). Écrivez des tests unitaires et une
  documentation mdBook.  Vous pouvez vous inspirer du [TP5 de
  PF](../pf/post25-tp5.html), et utiliser les fréquences de référence et
  données de test suivantes :

* * *

```cpp
const std::vector<double> refFreqs {
    0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07,
    0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06,
    0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
};
```

```text
crkzqflkxi moldoxjjfkd fp pl zxiiba ybzxrpb x moldoxj zlkpfpqp bkqfobiv lc crkzqflkp.
qeb jxfk moldoxj fqpbic fp tofqqbk xp x crkzqflk tefze obzbfsbp qeb moldoxj’p fkmrq xp fqp xodrjbkq xka abifsbop qeb moldoxj’p lrqmrq xp fqp obpriq.
qvmfzxiiv qeb jxfk crkzqflk fp abcfkba fk qbojp lc lqebo crkzqflkp, tefze fk qrok xob abcfkba fk qbojp lc pqfii jlob crkzqflkp rkqfi xq qeb ylqqlj ibsbi qeb crkzqflkp xob ixkdrxdb mofjfqfsbp.
```

* * *

<video preload="metadata" controls>
<source src="files/gl1-caesar.mp4" type="video/mp4" />
![](files/gl1-caesar.png){width="50%"}

</video>


## Hashell

- Regardez le projet de base fourni `projets/hashell`.

- Implémentez un shell simple en Haskell, notamment les commandes `cat`, `grep`
  et `wc`. Pour cela, implémentez des modules `Cat`, `Grep` et `Wc`, ainsi
  qu'un programme principal, des tests unitaires et de la documentation de
  code.

* * *

<video preload="metadata" controls>
<source src="files/gl1-hashell.mp4" type="video/mp4" />
![](files/gl1-hashell.png){width="50%"}

</video>


## Cshell

- Regardez le projet de base fourni `projets/cshell`.

- Implémentez un shell simple en C++, notamment les commandes `cat`, `grep`
  et `wc`. Pour cela, implémentez des modules `Cat`, `Grep` et `Wc`, ainsi
  qu'un programme principal, des tests unitaires et de la documentation de
  code.

* * *

<video preload="metadata" controls>
<source src="files/gl1-cshell.mp4" type="video/mp4" />
![](files/gl1-cshell.png){width="50%"}

</video>


## csvstats

- Regardez le projet de base fourni `projets/csvstats`.

- Implémentez un programme qui lit des données CSV (valeurs séparées par des
  virgules), puis affiche les données (avec un délimiteur donné) et des
  statistiques par colonne (min, max, moyenne, écart-type). Écrivez des tests
  et une documentation de code. Si vous avez le temps, gérez les lignes vides
  et les données manquantes.

* * *

```sh
$ cat data/test3.csv 
1,0,9

4,2,6
7,0,3


$ ./build/csvstats-app data/test3.csv 

**** data ****
1;0;9
4;2;6
7;0;3

**** stats ****
min = 1 0 3
max = 7 2 9
avg = 4 0.666667 6
stddev = 2.44949 0.942809 2.44949
```

