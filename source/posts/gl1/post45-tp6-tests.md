---
title: GL1 TP6 - Tests 
date: 2020-05-25
---

# Tests unitaires

Un test unitaire est un "morceau de code" qui permet d'appeler une fonction sur
des paramètres prédéfinis et de vérifier que le résultat est bien le résultat
attendu. Ainsi, on peut écrire tout un ensemble de tests unitaires pour
vérifier les différentes fonctions de notre base de code, aussi bien pour les
cas d'utilisation nominaux que pour les cas d'erreurs. Les tests unitaires
peuvent être exécutés à tout moment, ce qui permet de vérifier la
non-régression. 

Les tests unitaires sont un outils très classique. Il existe de nombreuses
bibliothèques permettant de mettre en place des tests unitaires (junit, gtest,
pytest...); ici on utilisera [catch2](https://github.com/catchorg/Catch2).


## Utilisation

- Allez dans le dossier `tp-tests/fibo` et regardez le code source et la
  configuration CMake de ce projet.

- Compilez le projet et lancez le programme de test. Testez également avec
  `make test` et avec `ctest`.

- Supprimez le répertoire `build` puis lancez un `nix-build`. Vérifiez que les
  tests sont exécutés lors de la création du paquet.


## Mise en place

- Dans le projet `tp-tests/mymaths`, mettez en place quelques tests unitaires
  simples. Pour cela, mettez tout votre code de test dans un dossier `test`.


## Écriture de tests unitaires

- Dans le projet `tp-tests/myclass`, complétez les tests unitaires.


# Test-driven development 

Le TDD est une méthode de développement qui consiste à écrire les tests d'une
fonction avant la fonction elle-même. La marche à suivre est donc la suivante :
implémenter un test, implémenter la fonction de façon à passer le test,
compiler et tester, recommencer avec le test suivant.

* * *

- Allez dans le dossier `tp-tests/poivrot-detector` et terminez l'implémentation selon le TDD.

- Testez le programme principal.

```sh
$ ./build/poivrot-detector placard1.txt 
tout va bien

$ ./build/poivrot-detector placard2.txt 
veuillez consulter
```


# Les Mock Objects

Les tests unitaires sont très pratiques pour tester du code déjà utilisable (ou
en partie utilisable). Cependant, on ne peut pas forcément les utiliser si on
veut tester du code qui dépend de code non encore implémenté (ou
non-déterministe).

Un "mock" est objet qui simule un comportement attendu d'un objet réel. Ceci
permet de tester du code sans avoir l'implémentation réelle de cet objet.
Il existe différentes bibliothèques de mocking (gmock, fakeit...); ici on utilisera
[Trompeloeil](https://github.com/rollbear/trompeloeil).


## Exemple

- Regardez le code du projet `tp-tests/mymock` et testez.


## Exercice

- Dans le projet `tp-tests/tictactoe`, implémentez un programme de test du
  module `Ui`.  Pour cela, mettez votre code de test dans un dossier `test`,
"mockez" `Game` et implémentez les scénarios suivants :  

    - un appel à la fonction `Ui::step` avec une action valide
    - deux appels valides puis un appel invalide
    - un appel à `Ui::run` avec la séquence d'actions `"1 1\n0 0\n0 2\n1 0\n2 0"`


# Property-based testing

Les tests de propriétés peuvent être vus comme des tests unitaires plus
automatisés : on écrit des assertions que le résultat de notre fonction doit
respecter et le système de test vérifie ces assertions, à partir de données
qu'il génère aléatoirement. Si le système de test détecte une donnée qui
invalide une propriété, il va chercher automatiquement l'entrée la plus simple
qui invalide la propriété, pour nous simplifier le debug. Le système de test
permet également de paramétrer la générations des données, le nombre de tests à
réaliser, etc.

Les tests de propriétés ont été popularisés par QuickCheck en Haskell. Ici, on
utilisera la bibliothèque C++
[RapidCheck](https://github.com/emil-e/rapidcheck).


## Exemple

- Regardez le code du projet `tp-tests/pbt` et testez.

## Exercice

Le projet `tp-erreurs/find3` implémente un jeu passionnant : le joueur choisit
un nombre entre 0 et 9, et doit deviner le nombre 3, en 4 essais maximum.

- Implémentez des tests de propriétés pour ce projet. Il se peut que le code
  ait des erreurs, qu'il faudra donc détecter et corriger. Indication : pour
générer des actions de jeu valides, regardez la documentation sur les
[générateurs de
conteneurs](https://github.com/emil-e/rapidcheck/blob/master/doc/generators_ref.md#containers)

