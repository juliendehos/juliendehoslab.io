---
title: GL1 CM - Déroulement d'un projet
date: 2019-04-18
---

# L'approche projet

## Approche processus, approche projet

- approche processus : 
    - chaîne d'activités répétitives et certaines
    - ex : ligne de production industrielle…
- approche projet : 
    - ensemble d'activités, unitaire et incertain
    - ex : construction d'un pont, développement d'un logiciel, évolution d'un SI…
    - « outil de pilotage »

## Objectif d'un projet

- répondre à un besoin
- en respectant des coûts et des délais
- le tout généralement établi dans un contrat

## Acteurs et objets d'un projet

- maître d'ouvrage (MOA) : le client, qui a un besoin
- maître d'œuvre (MOE) : le prestataire, qui répond au besoin
- périmètre : tous les éléments concernés par le projet
- ressources : moyens (matériels, humains…) nécessaires pour réaliser le projet
- besoin : demande exprimée par le client (dans son vocabulaire métier)
- exigences : besoin formalisé  

## Avant de dérouler un projet

- étude de faisabilité
- analyse de risques 
- prévoir le cycle de vie, la conduite de projet…

* * *

PERT (Program Evaluation and Review Technique) :

![](files/cm-pert.png){width="70%"}

* * *

Diagramme de Gantt :

![](files/cm-gantt-chart-large.jpg)

## Étapes classiques de réalisation d'un projet

- planification
- spécification
- développement
- test
- déploiement
- … selon un cycle unitaire ou itératif

* * *

![](files/cm-cycles_vie.svg){width="80%"}

## Dans la réalité

- choix des projets : critères économiques, commerciaux, stratégiques
- complexité technique : perte de maîtrise, création de dette technique
- inertie des technologies
- problème de méthode ou d'organisation des équipes de développement
- évaluation parfois difficile de l'effet réel du projet fini

## Contenu d'un projet informatique

- applications, bibliothèques, base de données, fichiers de configuration, images, CSS…
- code dans plusieurs langages
- appli web, mobile, desktop, distribuée…
- déploiement : machine réelle, machine virtuelle, conteneur, serveur d'application…
- communication avec d'autres appli : protocoles, formats de données…
- …

# Planification

## Roadmap (feuille de route)

- planning des versions, avec leur date et contenu
- doit être connue par tous les acteurs du projets
- peut être établie quand on connait :
    - l'estimation des tâches
    - la backlog d'application
    - les ressources du projet

## Backlogs (carnets de produit ou de version)

- backlog : ensemble des tâches/demandes à réaliser
- backlog d'application, backlogs de version
- types de demande : fonctionnalité, amélioration, correctif, réusinage, doc…
- intervenants : utilisateurs, équipe projet
- outil principal : « board », physique ou logiciel (Trello, Github, Gitlab…)

* * *

![](files/cm-board.png)

## Ordonnancer une backlog

- objectifs : 
    - permettre de conduire le projet efficacement
    - permettre d'ordonnancer une backlog de version ou une phase de développement
- critères de choix, priorités des demandes :
    - nécessité technique 
    - valeur ajoutée pour l'utilisateur 
    - compromis valeur / effort 
- difficultés:
    - estimation de l'effort nécessaire
- outils :
    - diagramme de cas d'utilisation, user stories
    - discussion utilisateur, discussion équipe

# Spécification

## Objectifs de la spécifications

- spécifier : « indiquer quelque chose avec toutes les précisions nécessaires
  pour qu'aucune équivoque ne subsiste » (Larousse)
- granularité :
    - cycle unitaire : spécifier l'ensemble des cas d'utilisation (« spéc. fonctionnelle »)
    - cyle itératif : spécifier chaque cas d'utilisation de façon détaillée et indépendante
    - agile : spécifier les cas d'utilisation et les user stories
- compromis :
    - cycle unitaire : privilégie une structure globale cohérente
    - cycle agile : privilégie les besoins utilisateurs

## Spécifier les cas d'utilisation

- outils :
    - tableau de description des cas d'utilisation
    - diagrammes de cas d'utilisation
    - diagrammes d'activité
    - maquettes, pour les IHM
    - scénarios d'utilisation (= tests fonctionnels)

* * *

- exemple de diagramme de cas d'utilisation (Distributeur Automatique de Billets) :

![](uml/utilisation_dab.svg){width="70%"}

* * *

- exemple de description de cas d'utilisation :

+-------------------+-------------------------------------------------------+
| Cas d'utilisation | Retirer des billets                                   |
+-------------------+-------------------------------------------------------+
| Description       | Permet à l'utilisateur de retirer des billets.        | 
|                   | Le système vérifie l'identité de l'utilisateur et     | 
|                   | la disponibilité des fonds.                           | 
+-------------------+-------------------------------------------------------+
| Déclencheur       | L'acteur insère sa carte dans le DAB et demande       |
|                   | à faire un retrait.                                   | 
+-------------------+-------------------------------------------------------+
| Pré-conditions    | Le DAB est en attente                                 |
|                   |                                                       | 
+-------------------+-------------------------------------------------------+
| Post-conditions   | - Scénario nominal : le client a pris sa carte et ses |
|                   |   billets.                                            | 
|                   | - Scénario « retrait non autorisé » : le client a     | 
|                   |   repris sa carte.                                    | 
|                   | - Dans tous les cas : le DAB est en attente.          | 
+-------------------+-------------------------------------------------------+

* * *

- exemple de diagramme d'activité :

![](uml/activite_dab.svg){width="47%"}

## Spécifier une user story

- user story : scénario dans une démarche agile
- contenu :
    - libellé contenant l'acteur, l'action et les objectifs de l'user story
    - description d'un scénario de test détaillé (en lien avec l'IHM, le cas échéant)

* * *

- exemple d'user story :

+-------------+-----------------------------------------------------+
| Libellé     | Un client peut cliquer sur un bouton du panier afin |
|             | de le vider.                                        |
+-------------+-----------------------------------------------------+
| Description | - Scénario de test :                                | 
|             |     #. ajouter un article au panier                 | 
|             |     #. aller dans le panier                         | 
|             |     #. cliquer sur l'icone « vider »                | 
|             |     #. le panier est vidé                           | 
|             |     #. les articles sont dans les                   | 
|             |        « articles récemment supprimés »             | 
|             | - IHM : …                                           | 
+-------------+-----------------------------------------------------+

## Spécifier les données de l'application

- l'équipe doit avoir une vision partagée des données
- types de données :
    - données dont l'application est responsable
    - données manipulées par l'application
- outils : 
    - diagramme de classe
    - diagramme d'état

# Développement (outils)

## Mise en place du projet

- nombreux outils à décider ou à mettre en place : langages de programmation et bibliothèques, IDE, gestionnaire de code source, « board », outils de communication…

- décisions souvent imposées par : 
    - cahier des charges
    - technologies
    - équipe/entreprise…

* * *

- règles de codage :
    - organisation des fichiers
    - indentation
    - convention de nommage (variables, classes, méthodes…)
    - formatage des commentaires et de la documentation
    - organisation des tests unitaires…

## Gestion de code source, forge logicielle

- gestionnaire de version (Version Control System) :
    - git, mercurial, svn, cvs…
    - journaliser les modifications
    - partager le code
    - travailler sur plusieurs branches en parallèle
    - intégrer des modifications

* * *

- forge logicielle :
    - github, gitlab, bitbucket, gogs, redmine… + serveur d'hébergement (ou pas)
    - accès aux dépôts git, droits d'accès
    - synchronisation des dépôts (push, Pull Request / Merge Request)
    - packaging, intégration continue (CI), déploiement continu (CD)
    - gestion de projet (issue, milestone, PR/MR…)

* * *

- gestionnaire de version décentralisé

![](files/cm-dvcs.png)


* * *

- dépôt gitlab : page d'accueil 

![](files/cm-gitlab-ce-home.png)

* * *

- dépôt gitlab : board

![](files/cm-gitlab-ce-board.png)

* * *

- dépôt gitlab : milestones

![](files/cm-gitlab-ce-milestones.png)

* * *

- dépôt gitlab : tags

![](files/cm-gitlab-ce-tags.png)

* * *

- dépôt gitlab : integration continue

![](files/cm-gitlab-ce-ci.png)

## Organisation des fichiers, configuration de projet

- souvent influencée par les langages/technologies utilisés
- prendre en compte :
    - dépendances
    - compilation/construction
    - tests unitaires
    - déploiement, intégration continue…

* * *

- par exemple, en C++/cmake/gitlab :

```text
- include/
- src/
- .gitlab-ci.yml
- CMakeLists.txt
- README.md
- …
```

- si possible, faciliter la mise en place du projet  ("git clone" + "apt install" + "cmake/make")

## Documentation

- différentes docs : installation, utilisation, maintenance
- attention à garder les docs lisibles et à jour 
- documentation de code :
    - conventions de nommage (variable = nom, fonction = verbe)
    - commenter l'utilisation d'une fonction dans son entête
    - si nécessaire, commenter son fonctionnement interne dans son implémentation
- outils: 
    - doc de code (javadoc, doxygen…)
    - générateurs de doc à partir de markdown/reStructuredText (mdbook, mkdocs…)
    - …

## Gestion d'erreurs

- implémenter une gestion d'erreur correcte est difficile
- à décider pour l'ensemble du projet (règles de codage…)
- outils : 
    - assertions
    - traces, logs
    - codes de retour, variables d'erreur (style C)
    - exceptions…

## Analyse de code

- relecture de code ("code review")
- analyseur statique (cppcheck, clang-analyzer…) : détection de code valide mais suspect
- linter (cpplint, clang-tidy…) : analyse en temps-réel (formatage, syntaxe…)
- debugger (gdb, lldb…) : analyse d'une exécution (pile d'appel, contenu mémoire…)
- analyse mémoire (valgrind…) : fuites, copies excessives…
- analyse de performances (gprof, kcachegrind…) : chronométrage, profilage…

# Développement (méthodes)

## Workflow

- « workflow » : méthode de travail
- à partir de la backlog de version (ou du sprint) :
    - sélectionner un ticket (issue)
    - se placer sur la branche de code adéquate
    - implémenter le ticket et ses tests
    - vérifier la compilation et les tests
    - diffuser la modification (commit, merge/PR/MR)
    - terminer le ticket
- le workflow définit également l'utilisation du dépôt de code…

## Workflow : master + develop

![](files/cm-gitflow-master-dev.svg){width="80%"}

## Workflow : master + develop + features

![](files/cm-gitflow-master-dev-feat.svg){width="80%"}

## Workflow : gitflow

![](files/cm-gitflow.svg){width="80%"}

## Architecture de code

- architecture classique :
    - couche données (accès basique aux données)
    - couche service (manipulation « métier » des données)
    - couche contrôleur (lancement des services)
    - couche vue/UI/IHM (présentation des données et services aux utilisateurs)
- design patterns
- utilisations des diagrammes UML, diagrammes de classe détaillés, diagrammes de séquences de scénarios 

## Programmation en binôme  (peer-programming)

- principe : 
    - un secrétaire + un relecteur
    - les deux participent activement
    - les rôles changent fréquemment
- avantages : 
    - éviter des erreurs
    - confronter plusieurs réflexions
    - motivation
- inconvénients : efforts de mise en place

## Programmation dirigée par les tests

- les tests permettent de « valider » le code 
- tests précoces $\Rightarrow$ détection précoce des erreurs $\Rightarrow$ correction précoce $\Rightarrow$ conséquences faibles
- principe du développement dirigé par les tests (Test-Driven Development) :
    - écrire un test validant un comportement du code
    - écrire le code permettant de passer le test
    - recommencer jusqu'à validation complète
- nécessite un système de tests automatisés

## Programmation par pseudo-code

- principe :
    - écrire l'algorithme en pseudo-code dans le code source
    - le commenter
    - écrire le code correspondant
- intérêt : 
    - facilite l'implémentation pour les algos un peu compliqués
    - documente le code

## Méthode de correction d'erreurs

- situation : en phase de développement, on a une erreur lors d'une exécution 

* * *

- méthode de correction :
    - vérifier les « erreurs bêtes » (fichiers mal sauvegardés, exécution d'un mauvais programme…)
    - reproduire l'erreur
    - localiser le problème
    - écrire un test unitaire réalisant l'erreur
    - trouver l'origine du problème et implémenter une correction
    - valider la correction (tests unitaires, exécution du programme)
    - rechercher des éventuelles erreurs similaires dans le code

## Optimisation des performances

- **optimiser uniquement si c'est nécessaire et quand c'est nécessaire**
- le code doit d'abord être valide et propre
- pistes envisageables (dans l'ordre) :
    - optimisations du compilateur (informations de debug, options d'optimisation)
    - optimisations haut niveau (algorithmes de meilleurs complexités)
    - parallélisme (CPU multi-cœur, SIMD, GPU…)

* * *

- méthode générale :
    - coder et tester proprement
    - mesurer les performances
    - tant que les performances sont insuffisantes :
        - localiser une section de code peu performante
        - modifier le code et tester
        - mesurer le gain obtenu
        - annuler les modifications si le gain est négligeable et que le code est moins propre

# Test

## En phase de développement

- tests unitaires : 
    - granularité fine : fonction, module
    - objectif : vérifier le fonctionnement interne du code
- tests d'intégration : 
    - granularité moyenne : module, bibliothèque, application
    - objectif : vérifier que les différents composants s'intègrent correctement entre eux

## Test opérationnel

- du point de vue de l'utilisateur
- objectif : 
    - vérifier que les objectifs (fonctionnels et non-fonctionnels) de la version sont atteints
    - préparer le déploiement
    - alimenter la backlog avec les défauts constatés
- « fonctionnel » : ce que fait le système (fonctionnalités, cas d'utilisation…)
- « non-fonctionnel » : ce qu'est le système (performances…)

## Mise en œuvre de tests opérationnels

- à partir des spécifications
- effectuer tous les cas d'utilisation ou user stories
- vérifier que les résultats obtenus sont conformes aux attentes
- établir la couverture de test
- si possible : automatisation, équipe de test…

# Déploiement

## Quelques remarques sur le déploiement

- contenu d'une version d'application:
    - paquet de l'application 
    - fichiers de configuration
    - scripts SQL
    - ressources statiques (images, pdf…)…
- déployer : rendre disponible au client une version d'application (livrables, mise en production…)
- souvent en relation avec l'équipe d'exploitation (admin sys), ou selon une démarche « devops »

## DevOps

- méthodologie pour unifier le développement et l'exploitation
- généralisation des principes agiles à la production
- concrètement : méthode de travail + automatisation (intégration continue, tests, déploiement…)

![](files/cm-Devops-toolchain.svg){width="60%"}

