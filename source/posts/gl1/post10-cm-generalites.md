---
title: GL1 CM - Généralités
date: 2023-08-22
---

# Motivation

## Importance de la qualité d'un « logiciel »

- Le logiciel est très répandu dans le monde actuel :
    - applications et services web
    - systèmes d'informations des entreprises et des administrations
    - réseaux de communication
    - systèmes embarqués…
- Un logiciel peut avoir de grandes conséquences
    - économiques
    - humaines
    - sociales
    - politiques…

## Ariane 5 (1996)

- premier lancement de la fusée : explosion après 36 s
- bug informatique : dépassement d'entier dans les registres mémoires
- réutilisation du système de navigation d'Ariane 4, sans nouvelles simulations
  (pour économiser 800k francs) 
- coût estimé des corrections : entre 700 millions et 1,4 milliards de francs
- et les 4 satellites perdus lors du lancement (370 millions de dollars)

* * *

![](files/cm-ariane-v501-4juin96.jpg)

## Therac 25 (1985)

- appareil de radiothérapie
- accidents d'irradiation massive, provoquant la mort de plusieurs patients
- réutilisation de code probablement déjà erroné mais sans sécurité matérielle
- rapport d'enquête :
    - négligence de l'évaluation et des tests du code source
    - mauvaise documentation des codes d'erreur
    - dépassements de capacité provoquant des désactivations de tests de sécurité
    - accès concurrents entre le contrôle matériel et l'interface utilisateur…

* * *

![](files/cm-220px-Clinac_2rtg.jpg)

## Chaos Report, Standish Group (1995)

- étude de 8380 projets issus de 365 entreprises 
- 16% sont conformes aux prévisions initiales
- 53% sont hors budgets, hors délai et n'offrent pas toutes les fonctionnalités
  prévues
- 31% sont abandonnés pendant le développement

## Plus récemment

- Why Software Fails, IEEE Spectrum (2005)
- 2018’s IT Failures Already Have a Familiar Look, IEEE Spectrum (2018)
- Future of Work survey, Appian (2018) :
    - 20% des projets sont livrés mais ne répondent pas au besoin
    - 15% sont abandonnés avant la fin du développement
    - 15% sont abandonnés avant le début du développement
- [Panne informatique mondiale de juillet 2024 (CrowdStrike)](https://fr.wikipedia.org/wiki/Panne_informatique_mondiale_de_juillet_2024#Analyses)
- …

## À propos de "progrès" 

- [Libération 2022-05-02](https://www.liberation.fr/environnement/biodiversite/des-ong-environnementales-portent-plainte-apres-lexplosion-de-la-fusee-starship-de-spacex-20230502_WPSIPIGV5FBYXHVSLC5N7I6ZOI/) :

![](files/gl1-liberation-2022-05-02.png){width="60%"}


# Petit historique sur le logiciel

## Les débuts de l'informatique

- initialement, beaucoup de travaux sur le matériel
- mais développement logiciel assez artisanal
- puis importance grandissante du logiciel (taille, complexité… problèmes)
- « crise du logiciel » (NATO Software Engineering Conference, 1968)

## Les débuts du génie logiciel (software engineering)

- application des méthodes d'ingénierie classique au développement logiciel
- notions de produit, projet, qualité, cycles de vie, normes… 

* * *

- The Mythical Man-Month, Fred Brooks, 1975

![](files/cm-mythical_man_month.jpg)

## Évolution du génie logiciel

- modélisation, tests…
- cycles itératifs, méthodes agiles (software craftsmanship), devops…
- prise en compte des spécificités du logiciel
- évolutions des problématiques : cloud/web/mobile, sécurité des systèmes,
  protection des données, éthique, écologie…

# Quelques notions 

## Notion de génie logiciel

- étude des méthodes de travail des développeurs et administrateurs de logiciel
  (applications, services…)
- objectifs :
    - répondre au besoin des utilisateurs
    - en respectant les coûts et délais de construction initiaux
    - et avec de bonnes performances d'utilisation et un faible coût d'entretien

## Notion de produit

- « résultat d'une activité humaine sous la forme d'un bien ou d'un service »
- s'applique au logiciel
- dans le contexte classique : entreprise, cycle de vie (de la conception au
  retrait), vente…
- mais avec quelques particularités : 
    - immatériel (mais un peu quand même)
    - omniprésent et varié (ordinateur, smartphone, data-center, grille-pain…
      banques, jeux vidéo, télécom, médecine, marketing…)
    - volume (selon Gartner, marché 2017 des logiciels d'entreprise : 350
      milliards de dollars)
    - logiciel libre, open-source…

## Notion de qualité

- « aptitude d'un ensemble de caractéristiques intrinsèques d'un objet
  (produit, service…) à satisfaire des exigences »
- un manque de qualité peut avoir des conséquences graves
- un manque de qualité détecté tardivement est difficile à corriger (coût
  important, faible maîtrise)
- domaine à part entière : politique qualité, mesures, tests, normes, audits…
- également important pour du logiciel

## Notion de projet

- « ensemble d'activités entreprises pour répondre à un besoin défini par un
  contrat, dans des délais et coûts fixés »
- mode de fonctionnement classique pour un travail « inédit et unique » 
- maître d'ouvrage, maître d'œuvre, équipe projet
- gestion/conduite de projet
- étude de faisabilité, cycle de développement

## Notion de risque

- « effet de l’incertitude sur l'atteinte des objectifs »
- domaine cible, domaine projet
- probabilité d'apparition, impact sur le projet
- actions préventives, actions correctives
- détection, maitrise
- stratégie de développement : 

+-----------+------------------------+---------------------+
| projet    | certain                | incertain           |
+-----------+------------------------+---------------------+
| simple    | stratégie unitaire     | stratégie itérative |
+-----------+------------------------+---------------------+
| complexe  | stratégie incrémentale | stratégie itérative |
+-----------+------------------------+---------------------+

# Cycles de développement d'un projet logiciel

## Modèles en cascade

- approche séquentielle rigide
- facilite la cohérence globale du logiciel 
- mais peu adapté aux changements et aux incertitudes

![](files/cm-Waterfall_model.png)

## Modèles en V

- extension du modèle en cascade avec intégration de phases de test
- incite à la rigueur et à la validation
- mais peu flexible 

![](files/cm-Cycle_de_developpement_en_v.svg){width="80%"}

## Modèles incrémentaux ou itératifs 

- organisation du projet en itérations/incréments (qui peuvent eux-même suivre
  un modèle en cascade ou en V)
- validation client plus fréquente, adaptation au changement
- mais ne garantit pas une cohérence et une efficacité globale 

![](files/cm-Iterative_development_model.png)

* * *

![](files/cm-Development-iterative.png)

## Modèles « agiles »

- approches favorisant la flexibilité : notion forte d'équipe, intégration du
  client, itérations courtes (et déployées), adaptation au changement…
- mais pas forcément adapté à tout type de projet 

![](files/cm-agile_methodology.jpg)

## Remarques

- différentes approches/visions du développement logiciel
- pas forcément incompatibles
- à envisager selon le projet/contexte
- attention aux effets de mode, marketing…

