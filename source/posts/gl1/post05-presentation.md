---
title: GL1 - Présentation du module
date: 2023-08-22
---

## Objectifs du module

- connaitre quelques généralités sur le génie logiciel et la gestion de projet
- savoir utiliser quelques outils de base
- mettre en pratique sur quelques mini-projets

## Pré-requis 

- notions de base d'informatique
- notions de programmation (en C++)

## Organisation

- 3h de CM
- 24h de TP

## Évaluation

- session 1 : contrôle continu 
- session 2 : devoir sur feuille (aucun document autorisé)
- voir la section [Remarques sur le contrôle continu](../env/post40-cc.html)

## Travaux pratiques

- VM fournie (voir la section [Environnement de travail, Machine Virtuelle](../env/post10-vm.html))
- [dépôt gitlab](https://gitlab.dpt-info.univ-littoral.fr/julien.dehos/ulco-gl1-etudiant) à forker (voir la section [Environnement de travail, GIT](../env/post20-git.html))
- [flash-cards](https://gitlab.com/juliendehos/juliendehos.gitlab.io/-/raw/master/source/posts/gl1/files/GL1.apkg?ref_type=heads&inline=false) au format [Anki](https://apps.ankiweb.net/)

