---
title: GL2 - Présentation du module
date: 2023-08-24
---

## Objectifs du module

- connaitre quelques généralités sur la conception logicielle
- connaitre quelques méthodes et architectures classiques
- savoir utiliser quelques outils de base
- mettre en pratique sur quelques mini-projets

## Pré-requis 

- module GL1
- notions de programmation orientée objet (en C++)
- notions de programmation fonctionnelle (en Haskell)

## Organisation

- 3h de CM
- 24h de TP

## Évaluation

- session 1 : contrôle continu 
- session 2 : devoir sur feuille (aucun document autorisé)
- voir la section [Remarques sur le contrôle continu](../env/post40-cc.html)

## Travaux pratiques

- VM fournie (voir la section [Environnement de travail, Machine Virtuelle](../env/post10-vm.html))
- [dépôt gitlab](https://gitlab.dpt-info.univ-littoral.fr/julien.dehos/ulco-gl2-etudiant) à forker (voir la section [Environnement de travail, GIT](../env/post20-git.html))

