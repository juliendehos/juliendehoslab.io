---
title: GL2 TP2 - Intégration continue avec GitLab
date: 2023-10-10
---

L'intégration continue est un ensemble de pratiques et d'outils permettant de
collaborer sur une base de code et d'automatiser des opérations : intégrer des
modifications de code, vérifier la compilation et les tests, packager et
déployer une nouvelle version de l'application...  Ces services sont
disponibles sur les plateformes de gestion de code : GitLab CI/CD, GitHub
Actions...

* * *

Pour ce TP, on utilisera le GitLab de la fac. Pour mettre en place
l'intégration continue dans un projet, il suffit d'ajouter un fichier
`.gitlab-ci.yml` dans le dépôt git et d'y décrire les "jobs" du "pipeline" à
exécuter lors des modifications de code. Sur l'interface web du dépôt, on peut
suivre l'exécution des jobs dans l'onglet "build".

* * *

> Dans ce TP, vous allez créer des dépôts. Le but est que vous testiez les
> fonctionnalités d'intégration continue. __N'ajoutez pas votre encadrant
> de TP parmi les membres de ces projets.__


## Jobs, artefacts, déploiements

Un "job" est une étape d'intégration continue : chargement et configuration
d'un système puis copie du dépôt git puis exécution des commandes demandées
(compilation, génération de documentation...). Un "artefact" est un ensemble de
données que l'on peut récupérer à la fin d'un job (application compilée,
documentation générée...). Un "déploiement" consiste à rendre un artefact
disponible quelque part (lancer une application sur un serveur applicatif,
copier une documentation sur un server web...)

Ceci permet, par exemple, de faire des sites web statiques : on copie/génère
des pages HTML puis on récupère cet artefact et on le déploie sur un serveur
web.

* * *

- Créez un dépôt vide `masuperdoc` sur GitLab, clonez-le (par exemple, dans
  `~/Documents/`) et mettez-y les fichiers de `tp-ci-gitlab/masuperdoc`.

- Copiez/ajoutez également le fichier `.gitlab-ci.yml`. Committez/pushez et
  vérifiez que l'intégration continue se déclenche.

- Regardez le contenu du fichier  `.gitlab-ci.yml` et vérifiez que c'est
  cohérent avec ce qu'affiche l'interface web (logs, artefacts). GitLab permet
de déployer un artefact directement sur un serveur web mais cette
fonctionnalité n'est pas activée sur le GitLab de la fac, pour des raisons de
sécurité.

* * *

![](files/gl2-tp-ci-masuperdoc1.png){width="90%"}

* * *

- On veut maintenant que l'intégration continue génère et déploie une doc
  "mdBook". Générez et vérifiez la doc localement, avec les commandes `mdbook`
classiques.

- En fait, le fichier `default.nix` configure déjà le projet. Générez et
  vérifiez la doc localement, avec la commande `nix-build`.

- Modifiez votre intégration continue de façon à générer et déployer la doc, en
  utilisant `nix-build`.  Vérifiez sur l'interface web.


## Pipelines, stages

L'intégration continue d'un projet est organisée selon un "pipeline". Un
pipeline est une succession de "stages" (`build`, `test`, `deploy`). Un stage
regroupe des jobs qui peuvent être réalisés en parallèle. Ceci permet
d'optimiser le processus d'intégration continue : implémenter des
configurations différentes, réaliser des jobs en parallèle, interrompre le
pipeline si des jobs pré-requis échouent, réutiliser le résultat de jobs
précédents...

* * *

- Créez un dépôt vide `masuperlib` sur GitLab, clonez-le (par exemple, dans
  `~/Documents/`) et mettez-y les fichiers de `tp-ci-gitlab/masuperlib`.

- Copiez/ajoutez également le fichier `.gitlab-ci.yml`. Committez/pushez et
  vérifiez que l'intégration continue se déclenche.

- Ajoutez un job `build-debian` de stage `build`, qui compile le projet via
  CMake. Committez/pushez et regardez le pipeline déclenché sur l'interface
web.

* * *

- Modifiez votre intégration continue de façon à ajouter un job `test` de stage
  `test`, qui reprend les programmes compilés par `build-debian` et exécute les
tests unitaires (indication : ajoutez les champs `artifacts` et `dependencies`
qui vont bien). Vérifiez que les tests unitaires échouent et corrigez le code.

- Ajoutez un job `build-nixos` de stage `build`, qui compile le projet avec la
  commande `nix-build`. Committez/pushez et regardez le pipeline déclenché sur
l'interface web.


## Tags, dépendances

Git permet de spécifier des tags sur un commit. Ceci permet de fixer une
version du projet, avec l'archive de code correspondante, et ainsi de clarifier
les dépendances entre projets.

- Dans votre projet `masuperlib`, ajoutez un tag de version avec les commandes
  suivantes.

    ```sh
    git tag v0.1

    git push --tags
    ```

- Sur l'interface web, vérifiez que le tag "v0.1" est visible et qu'on peut
  télécharger une archive `.tar.gz` du code à cette version.

* * *

On veut maintenant créer un projet `masuperapp` qui utilise le projet
`masuperlib` à la version "v0.1".

- Créez un dépôt vide `masuperapp` sur GitLab, clonez-le (par exemple, dans
  `~/Documents/`) et mettez-y les fichiers de `tp-ci-gitlab/masuperapp`.

- Lancez la commande `nix-build` et vérifier que la compilation fonctionne
  localement.

- Ajoutez/committez/pushez les fichiers et regardez le pipeline d'intégration
  continue déclenché. Expliquez ce qu'il se passe.

- Modifiez le fichier `release.nix` de façon à utiliser l'archive de code de la
  version "v0.1" de votre projet `masuperlib` sur le serveur GitLab. Modifiez
l'intégration continue de façon à utiliser le fichier `release.nix` et vérifiez
que tout fonctionne désormais.


## Container registry

GitLab permet de générer et de stocker des images Docker. Ceci permet de
construire des applications que l'on peut déployer ou télécharger facilement.

- Créez un dépôt vide `monsuperserveur` sur GitLab, clonez-le (par exemple, dans
  `~/Documents/`) et mettez-y les fichiers de `tp-ci-gitlab/monsuperserveur`.


* * *

- Compilez le projet avec un `nix-build`, lancez le programme généré et
  testez-le en allant à l'URL <http://localhost:3000>.

```text
$ nix-build
this derivation will be built:
  /nix/store/w6aw5zr67idxym0mjsq3bw9jmv3vci02-monsuperserveur-1.0.drv
...

$ ./result/bin/monsuperserveur
Setting phasers to stun... (port 3000) (ctrl-c to quit)
```

- Dans l'intégration continue, ajoutez un job `build` qui compile le projet.

* * *

- Le projet fourni permet de construire une image Docker contenant le programme
  implémenté. Testez-le avec les commandes suivantes.

```text
$ nix-build docker.nix
these 11 derivations will be built:
...

$ docker load -i result 
4ee3af161ea5: Loading layer [==============>]  1.812MB/1.812MB
...

$ docker images
REPOSITORY        TAG       IMAGE ID       CREATED        SIZE
monsuperserveur   latest    b407362254f4   52 years ago   44MB

$ docker run --rm -it -p 3000:3000 monsuperserveur:latest
Setting phasers to stun... (port 3000) (ctrl-c to quit)

$ docker rmi -f monsuperserveur
```

* * *

- Dans votre intégration continue, ajoutez un job `build-docker` et un job
  `deploy-docker` qui construit l'image docker et la charge dans le "container
registry" du serveur GitLab.

- Via l'interface web, vérifiez que l'image docker est bien créée et
  disponible dans le "container registry".

- Testez l'image Docker construite par GitLab, avec un `docker run` sur votre
  machine.

- Modifiez le code de façon à ajouter une route `add` qui retourne la somme de
  deux nombres donnés en paramètre, et vérifiez que l'image Docker construite
par GitLab est bien mise à jour.


## S'il vous reste du temps

Mettez en place un projet C++ classique avec intégration continue : compilation
avec CMake, tests unitaires avec Catch2 et documentation avec Doxygen.

