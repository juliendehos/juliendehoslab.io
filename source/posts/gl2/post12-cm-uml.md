---
title: GL2 CM - UML
date: 2019-04-18
---

# Modélisation et approche objet

## Notion de modèle

- représentation abstraite (et simplifiée) d'une entité réelle 
- utilisations :
    - décrire et analyser une entité existante
    - décrire et analyser une entité à construire
    - simuler le résultat d'une entité, pour un contexte donné
- exemples :
    - modèle météorologique
    - modèle de réseau de neurones profond
    - modèle de système d'information 
- intérêts de la modélisation dans un projet informatique :
    - comprendre et gérer la complexité du système à réaliser
    - gérer le projet (communication, planification…)

* * *

> Réaliser un projet logiciel, ce n'est pas que coder; c'est aussi spécifier,
> tester, documenter… le tout selon une conduite de projet.

## Langage de modélisation

- formalisme permettant de décrire un modèle
- intérêts d'un langage standardisé :
    - moins d'ambiguïtés
    - compétence répandue
    - communication (MOA/MOE, équipe projet…)
    - outils de modélisation compatibles avec le langage
- différents formats : texte, graphique
- différentes approches : traitement, objet

## L'approche objet 

- logiciel = ensemble d'objets en interaction (données + traitements)
- objet : entité avec un nom et un périmètre (attributs et méthodes)
- message : moyen de communication entre objets (appel de méthode)
- encapsulation : principe de regrouper les attributs et méthodes d'un objet (+
  masquage d'implémentation, droits d'accès) 
- instanciation : création d'un objet à partir d'un modèle (classe, objet…)

* * *

![](uml/approche_objet2.svg){width="20%"}

![](uml/approche_objet.svg){width="40%"}

## Définir des objets « composés »

- agrégation : 
    - un objet contient d'autres objets
    - composition : agrégation forte (définit la durée de vie des objets
      contenus)
- héritage : 
    - un objet hérite du même périmètre qu'un autre objet
    - et ajoute des élements spécifiques
    - notion de polymorphisme

* * *

![](uml/objets_composes.svg){width="40%"}

## L'approche objet basée classe

- classe : type de données définissant les attributs et méthodes que possèdent
  les objets de la classe
- instanciation de classe, héritage de classe…

Il existe également une approche objet basée prototype.

# Généralités sur UML

## Motivation d'UML

- avoir un langage de modélisation commun
- pour développer, échanger des spécifications…
- visuel et expressif

## Historique
 
![](files/cm-ml-history.jpg)

## UML

- = Unified Modeling Language 
- est un langage graphique de modélisation objet
- convient pour tout langage objet
- est un standard ISO adopté par l'OMG (Object Managment Group)
- est souvent utilisé dans l'industrie logicielle
- **n'est pas une méthode**
- **n'est pas un langage de programmation**

## Concrètement

- UML définit des éléments de langage (objet, classe, relations…) et des
  diagrammes (de classes, de séquence…), ainsi que leur représentation
- un diagramme permet de définir une partie et un aspect du modèle

* * *

![](files/cm-UMLDiagrammhierarchie.svg){width="90%"}

## Utilisation

- faire un ensemble de diagrammes complémentaires, de façon à décrire les
  parties et aspects importants du modèle
- utilisation dans le cycle de vie de l'application :
    - phases de spécification, analyse… (système boite-noire)
    - phases de conception, développement… (système connu)
- certaines méthodes favorisent certains types de diagramme :
    - méthodes centrées sur la structure : diagramme de classes
    - méthodes centrées sur l'utilisateur : diagramme de cas d'utilisation

**Les diagrammes UML ne sont pas auto-suffisants; ils ne sont qu'une partie des
documents de spécification, de la documentation de code…**

## Dans la suite du cours

- diagrammes de comportement :
    - diagramme d'activité
    - diagramme de cas d'utilisation
    - diagramme d'états-transitions
- diagramme statique :
    - diagramme de classes
- diagramme dynamique :
    - diagramme de séquence

# Diagramme d'activité

## Objectifs

- représente le déroulement d'un *processus* sous forme d'un enchainement
  d'*actions*
- utilisations :
    - formaliser des processus existants
    - concevoir des processus
    - analyser des cas d'utilisation…

## Notations

- état initial : le début du processus
- état final : fin du processus (plusieurs états finaux possibles)
- activité : action à réaliser pour avancer dans le processus
- transition : 
    - passage à l'activité suivante
    - déclenchée automatiquement à la fin de l'activité
    - peut transmettre des informations…

* * *

![](uml/activite_1.svg){width="25%"}

## Transitions conditionnelles

- sélection de l'activité suivante en fonction d'une condition

![](uml/activite_2.svg){width="40%"}

## Parallélisme et synchronisation

- barres de synchronisation
- permettent d'ouvrir et de fermer des branches d'activités parallèles
- à l'ouverture, les branches sont déclenchées en même temps
- à la fermeture, on attend que toutes les branches soient terminées pour
  passer à l'activité suivante

* * *

![](uml/activite_3.svg){width="40%"}

## Couloirs d'activités (swimlane)

- permettent de représenter les responsables des différentes activités

![](uml/activite_4.svg){width="70%"}

# Diagramme de cas d'utilisation

## Objectifs

- définit des fonctions du système, d'un point de vue utilisateur (système
  boite-noire)
- doit être compréhensible par tout le monde (notamment les utilisateurs)
- utilisations :
    - définir les besoins des utilisateurs
    - spécifier les fonctionnalités du système
    - structurer les phases de conception, développement, test…
    - guider la conduite de projet (méthodes agiles…)

## Acteur, cas d'utilisation, relation d'association

- acteur : rôle joué par un objet (personne ou système) qui interagit avec le
  système
- cas d'utilisation : ensemble d'actions réalisées par le système, en réponse à
  une action d'un acteur
- relation d'association : indique que l'acteur participe au cas d'utilisation
- exemple avec 1 acteur et 2 cas d'utilisation :

![](uml/utilisation_1.svg){width="40%"}

## Paquetage, note de commentaire

- paquetage : regroupement de cas d'utilisation
- note de commentaire : pour commenter…
- exemple précédent avec un paquetage et une note de commentaire :

![](uml/utilisation_2.svg){width="70%"}

## Relation d'héritage

- notion d'héritage classique : l'objet hérite de toutes les capacités de
  l'objet initial 
- héritage d'acteur
- (héritage de cas d'utilisation)

![](uml/utilisation_3.svg){width="60%"}

## Relation d'inclusion, relation d'extension

- inclusion : le cas pointé doit être inclus dans le cas courant 
- extension : le cas pointé peut être étendu par le cas courant
- attention au sens des flêches

![](uml/utilisation_4.svg){width="60%"}

## Exemple plus complet

![](uml/utilisation_5.svg){width="80%"}

# Diagramme d'états-transitions (ou pas)

## Objectifs

- décrit le comportement des objets
- automate à états finis
- utilisations :
    - vérifier le déroulement de cas d'utilisation
    - spécification, conception…

## État

- état : situation stable d'un objet ou composant
- état initial
- état intermédiaire
- état final

![](uml/etats_1.svg){width="20%"}

## Transition

- passage d'un état à un autre
- déclenchée automatiquement ou par un événement
- peut être conditionnée par des gardes

![](uml/etats_2.svg){width="60%"}

## État composite

![](uml/etats_3.svg){width="60%"}

# Diagramme de classes

## Objectifs

- définit les concepts manipulés par l'application
- et les relations entre les concepts
- utilisations :
    - conception
    - développement
    - documentation technique…

## Classe, attribut, méthode

- classe : caractéristiques d'un ensemble d'objets, description abstraite, type
  de données
- instance d'une classe : objet appartenant à la classe
- attribut : donnée (définie par la classe et pour lequel chaque instance a une
  valeur)
- méthode : fonction (définie par la classe et s'appliquant sur une instance)

![](uml/classes_1.svg){width="25%"}

## Visibilité

- droit d'accès aux membres (attributs et méthodes) depuis l'extérieur de la
  classe
- privé : accès depuis la classe uniquement
- protégé : accès depuis la classe et ses classes dérivées
- publique : accès par tout le monde

![](uml/classes_2b.svg){width="25%"}
![](uml/classes_2.svg){width="25%"}

## Héritage

- définit une nouvelle classe à partir d'une classe existante
- la classe fille possède les membres de la classe mère
- et peut les redéfinir ou en ajouter

![](uml/classes_3.svg){width="25%"}

## Classe abstraite

- certaines méthodes ne sont pas définies/définissables
- ces méthodes sont à définir dans les classes dérivées 
- notion de polymorphisme
- une classe abstraite n'est pas instanciable

![](uml/objets_composes.svg){width="40%"}

## Association, multiplicité

- association : relation sémantique entre classes
- multiplicité : nombre min et max d'instances de chaque classe dans
  l'association
- classe d'association

![](uml/classes_5.svg){width="50%"}

## Agrégation, composition

- agrégation (losange blanc) : association avec relation de subordination
- composition (losange noir) : agrégation avec cycle de vie dépendant

![](uml/classes_4.svg){width="30%"}

## Héritage ou composition ?

- héritage : « est un »
- composition : « a un »
- l'héritage indique une relation forte donc attention de ne pas en abuser :
  [composition over inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)

## Exemple plus complet

![](uml/classes_6.svg){width="100%"}

# Diagramme de séquence

## Objectifs

- représente des interactions entre des objets au cours du temps
- utilisations :
    - illustrer un cas d'utilisation
    - vérifier une conception…

## Objet, ligne de vie, message

- chaque objet a une ligne de vie
- le temps se déroule de haut en bas
- les objets s'envoient des messages au cours du temps

![](uml/sequence_1.svg){width="40%"}

## Message synchrone/asynchrone

- on peut représenter une synchronisation avec des zones d'activation

![](uml/sequence_2.svg){width="40%"}

## Frames

- permettent de représenter des boucles (loop), alternatives (alt)…
- mais complique le diagramme
- faire plusieurs diagrammes, plutôt 

![](uml/sequence_3.svg){width="40%"}

