---
title: GL2 TP4 - SOLID
date: 2020-05-25
---


# Exemple de base

Dans cet exercice, vous allez partir du projet `tp-solid/board` et le modifier
ou l'étendre, en suivant les principes SOLID. L'objectif du projet `board` est
de gérer les tickets d'un projet (ajouter des tickets, afficher des rapports,
etc).


## 1. Single responsibility principle (responsabilité unique)

- En quoi la classe `Board` ne respecte pas le principe de responsabilité unique ?

- Refactorez le code de façon à corriger cela. Indication : créez deux classes
  `ReportStdout` et `ReportFile`.

- Testez en passant un objet de chacune de ces deux classes en paramètre de
  `testBoard`.


## 2. Open/closed principle (ouvert/fermé)

On veut également un board qui numérote automatiquement les tickets (par
exemple, en préfixant le premier ticket par "1. ", etc).

- Implémentez cette fonctionnalité en respectant le principe ouvert/fermé.
  Indication : créez une classe `NumBoard` contenant une méthode `addNum`.

- Dans le `main`, écrivez une fonction `testNumBoard` pour tester.


## 3. Liskov substitution principle (substitution de Liskov)

- Dans le `main`, vérifiez que vos classes "Board" respectent le principe de
  substitution de Liskov.


## 4. Interface segregation principle (ségrégation des interfaces)

- En quoi peut-on considérer que l'interface `Itemable` ne respecte pas le
  principe de ségrégation des interfaces ?

- Refactorez le code pour corriger cela. Indication : créez une interface
  `Titleable`.


## 5. Dependency inversion principle (inversion des dépendances)

- Quelles sont les dépendances entre vos fonctions "testBoard" et vos classes
  "Report" ? Quels sont les inconvénients de cette implémentation ?

- Refactorez votre code, en respectant le principe de l'inversion des
  dépendances. Indication : créez une interface `Reportable`.


# Exercice d'application

- Regardez le projet `canevas`. Faites-en un diagramme de classes et expliquez
  pourquoi ce code est tout pourri.

- Refactorez le projet de façon à mieux respecter les principes SOLID.  Pour
  cela, refactorez **progressivement** (identifiez un problème, implémentez une
  solution, compilez/testez puis passez au problème suivant). Vous pouvez
  utiliser le diagramme suivant.

* * *

![](uml/gl2-tp-solid-canevas1.svg){width="100%"}


