---
title: GL2 CM - Méthodologie
date: 2020-07-20
---

# Motivation

## Rappels sur le GL

- Objectif : concevoir et réaliser un logiciel en respectant les
  spécifications, les coûts et les délais (et facile à maintenir ou à faire
évoluer)

- Programmation Orientée Objet : "paradigme", idée générale de l'approche

- UML : outil pour formaliser/représenter une conception orientée objet

- Mais : comment mettre au point une conception OO ?
    - pas de méthode miracle
    - modèles de conception
    - principes généraux

## Modèles de conception (design patterns)

- "Recettes" pour des situations classiques.

- Exemples : factory, singleton, adapter, visitor...

- Documentations :
    - [Design Patterns: Elements of Reusable Object-Oriented Software](https://en.wikipedia.org/wiki/Design_Patterns);
    - [GRASP: General responsibility assignment software patterns](https://en.wikipedia.org/wiki/GRASP_(object-oriented_design)).

- Critiques :
    - risque de sur-ingénierie, voire d'anti-pattern;
    - palliatif à des [inconvénients du langage](http://www.norvig.com/design-patterns/).

- Cf le cours de "POO Avancée" en master.


## Principes généraux

- "Règles à suivre" pour éviter des problèmes classiques.

- Exemples :
    - [SOLID](https://en.wikipedia.org/wiki/SOLID)
    - [composition over inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)
    - [const-correctness](https://isocpp.org/wiki/faq/const-correctness), [rule of three/five](https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming))...

- Influence du langage de programmation.

## Paradigmes

![[\@IsolaMonte](https://twitter.com/IsolaMonte/status/1306684829233229824/photo/1)](files/oo-vs-fp.jpg)

# SOLID

- 5 principes classiques de conception orientée objet.

- Attention : ce ne sont pas des règles absolues ni une garantie que
  le logiciel final sera correct.

- Notions de réutilisation, couplage, cohésion, niveau (module, classe,
  fonction)...

- Voir la [page wikipedia](https://en.wikipedia.org/wiki/SOLID) et les TP.


## Single responsibility principle

- Une classe ne doit être responsable que d'une partie bien
  définie des fonctionnalités et cette responsabilité doit être encapsulée
complètement dans cette classe.

- Autrement dit : une classe implémente un "aspect" du logiciel.


## Open/closed principle

- Une classe doit être ouverte à l'extension mais fermée à la
  modification.

- Autrement dit : on peut ajouter des fonctionnalités, sans avoir à modifier le
  code initial.


## Liskov substitution principle

- Si T est un type et S un sous-type de T, tout objet de type T doit pouvoir
  être remplacé par un objet de type S sans changer les propriétés désirables
du logiciel. 

- Autrement dit : on peut remplacer n'importe quel objet par un objet d'une
  classe dérivée.


## Interface segregation principle

- Les interfaces doivent être petites et spécifiques plutôt que
  larges et générales.

- Autrement dit : un client n'est pas forcé de dépendre de méthode qu'il
  n'utilise pas.


## Dependency inversion principle

- Les interfaces ne doivent pas dépendre des implémentations, ce
  sont les implémentations qui doivent dépendre des interfaces.



