---
title: GL2 TP1 - Interfacer des langages
date: 2023-08-24
---

Un logiciel utilise parfois plusieurs langages. Ceci peut être intéressant pour
réutiliser du code existant ou pour optimiser certaines parties du code. Par
exemple, il arrive souvent qu'on implémente les modules nécessitant beaucoup de
performances en Fortran, C ou C++, et le reste du code dans un langage de plus
haut-niveau comme JavaScript ou Python.  Dans ce TP, on va voir comment
réutiliser du code C++ dans du code Python, via la bibliothèque pybind11.

Quelques docs :

- [Python](https://www.python.org/doc/)
- [pybind11](https://pybind11.readthedocs.io/en/stable/index.html)


# Python

Les projets Python fournis sont configurés avec les Setuptools et Nix. Ainsi,
pour lancer un script `scripts/main.py`, il suffit de lancer la commande :

```sh
nix-shell --run "python scripts/main.py"
```

Et pour lancer un interpréteur Python qui a accès aux modules/dépendances du
projet, il suffit de lancer la commande (Ctrl-d pour quitter) :

```sh
nix-shell --run python
```


## Organisation d'un projet Python

Le dossier `tp-ffi/py-fibo1` contient un projet Python, avec un package et un
script principal.

- Regardez l'organisation du projet (package, module, script...) et lancez le
  script principal.

* * *

- Modifiez le script principal de façon à afficher la suite de Fibonacci de 0 à
  10 :

```text
fibo_naive(0) = 0
fibo_naive(1) = 1
fibo_naive(2) = 1
fibo_naive(3) = 2
fibo_naive(4) = 3
fibo_naive(5) = 5
fibo_naive(6) = 8
fibo_naive(7) = 13
fibo_naive(8) = 21
fibo_naive(9) = 34
fibo_naive(10) = 55
```

* * *

- Dans le module `myfibo`, implémentez une version itérative du calcul et
  testez avec le script principal :

```text
fibo_iterative(0) = 0
fibo_iterative(1) = 1
fibo_iterative(2) = 1
fibo_iterative(3) = 2
fibo_iterative(4) = 3
fibo_iterative(5) = 5
fibo_iterative(6) = 8
fibo_iterative(7) = 13
fibo_iterative(8) = 21
fibo_iterative(9) = 34
fibo_iterative(10) = 55
```


## Intégrer du code C++ dans un projet Python

Le dossier `tp-ffi/py-fibo2` contient un projet Python, avec un module C++.

- Regardez l'organisation du projet et lancez le script `main1.py`.

- Modifiez le fichier `main1.py` de façon afficher la suite de Fibonacci de 0 à
  10 en utilisant la fonction `fibo_naive` exportée dans le module C++.

- Implémentez une version itérative en C++ et testez-la via `main1.py`.

* * *

- Testez le script `main2.py` puis modifiez-le de façon à tracer la suite de
  Fibonacci de 0 à 10.

![](files/gl2-py-fibo2-main2.png){width="70%"}

* * *

- Testez le script `main3.py` puis modifiez-le de façon à tracer la suite de
  Fibonacci de 0 à la valeur du SpinButton.

![](files/gl2-py-fibo2-main3.png){width="70%"}


## Programmation orientée objet

- Regardez le projet `tp-ffi/py-logger`.

- Implémentez le binding de la classe `Logger` (exportez les méthodes en "snake
  case").

- Testez avec le script `main1.py` (contruisez un `Logger` et appellez la
  méthode `report_by_added`).

* * *

- Terminez l'implémentation du code C++ (`addItem`, `report_by_added`,
  `report_by_level`).

```text
**** report_by_added ****
[I] une info
[E] une erreur
[I] une autre info
[W] un avertissement

**** report_by_level ****
[I] une autre info
[I] une info
[W] un avertissement
[E] une erreur
```

* * *

- Modifiez votre code de façon à retenir l'horaire de chaque `addItem`. 

```text
**** report_by_added ****
2022-07-19 23:37:17 [I] une info
2022-07-19 23:37:17 [E] une erreur
2022-07-19 23:37:17 [I] une autre info
2022-07-19 23:37:17 [W] un avertissement

**** report_by_level ****
2022-07-19 23:37:17 [I] une autre info
2022-07-19 23:37:17 [I] une info
2022-07-19 23:37:17 [W] un avertissement
2022-07-19 23:37:17 [E] une erreur
```


# Haskell

## Appeler du code C dans un programme Haskell

Haskell permet d'appeler du code C via une FFI (Foreign Function Interface).
Ceci est généralement utilisé pour implémenter une interface Haskell à une
bibliothèque C existante.

* * *

- Regardez le projet `hs-c`, notamment l'utilisation de la FFI de Haskell.

- Compilez et testez.

- Ajoutez une fonction `add3` dans le code C et appelez-là depuis le code
  Haskell.


## Transpiler un programme Haskell vers JavaScript

(ne pas faire : pas à jour)

Le compilateur GHC produit du code natif, exécutable via le système
d'exploitation. Il existe également le compilateur GHCJS, équivalent mais qui
produit du code JavaScript, exécutable via un navigateur web. Attention, le
compilateur GHCJS est moins mature que GHC, ce qui peut parfois nécessiter de
réparer ou de recompiler des paquets.

* * *

- Regardez le projet `hs-js1`.

- Lancez un `nix-shell` puis un `cabal build`. Ceci produit du code JavaScript
  et une page web permettant de l'appeler. Ouvrez cette page et regardez la
console JavaScript.

- GHCJS fournit également une FFI pour interagir avec le code JavaScript.
  Regardez le projet `hs-js2`. Compilez et testez.


