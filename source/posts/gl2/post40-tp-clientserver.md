---
title: GL2 TP6 - Architecture client-serveur
date: 2023-08-25
---


L'[architecture
client-serveur](https://en.wikipedia.org/wiki/Client%E2%80%93server_model) est
une organisation très classique des logiciels. Elle est particulièrement
adaptée aux réseaux : services web, architecture 3-tiers (client + serveur
applicatif + serveur de données)... Cette architecture met en oeuvre de
nombreux concepts : protocole de communication, format de données, sécurité,
multi-threading, accès concurrents, etc.

L'objectif de ce TP est d'étudier des architectures client-serveur simples,
pour implémenter différentes fonctionnalités. On utilisera les WebSockets en
Haskell ou en C++.


## Echo

Un programme "echo" est un exemple simple d'application réseau : le serveur
attend que des clients se connectent et lorsqu'un client lui envoie un message,
le serveur lui répond par un message identique. Avec les WebSockets, il existe
des clients et serveurs "echo" disponibles en ligne.

- Allez dans le projet `echo`. Compilez et vérifiez que les différents clients
  et serveurs fonctionnent entre eux et avec
[ws.ifelse.io](https://ws.ifelse.io/) (les serveurs sont accessibles aux
adresses `wss://ws.ifelse.io` et `ws://localhost:9000`).

* * *

- Faites un diagramme de séquence du scénario réalisé dans la vidéo suivante.

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-echo1.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-echo1.png){width="80%"}

</video>

* * *

- Idem pour le scénario suivant.

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-echo2.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-echo2.png){width="80%"}

</video>

* * *

- Regardez le code, notamment :

    - Identifiez les échanges réseaux.
    - Où a-t-on de l'injection de dépendance ? 
    - Comment sont gérées les connexions multiples au serveur ?


## Broadcaster

Le projet `broadcaster` permet à un serveur de diffuser du texte à ses clients.

- Compilez et testez le projet.

- Faites un diagramme de séquence du scénario suivant.

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-broadcaster1.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-broadcaster1.png){width="80%"}

</video>

* * *

- Regardez le code fourni. Le module `Net` permet de gérer un ensemble de
  connexions clients mais ne vous en préoccupez pas trop pour l'instant.

- Comment sont gérés le multi-threading et/ou les accès concurrents ?


## Chat

Le projet `chat` est une copie du projet `broadcaster` précédent. Modifiez-le
de façon à implémenter un logiciel de chat basique.

<video preload="metadata" controls>
<source src="files/gl2-tp-clientserver-chat1.mp4" type="video/mp4" />
![](files/gl2-tp-clientserver-chat1.png){width="70%"}

</video>


* * *

S'il vous reste du temps :

- Modifiez le projet de façon à gérer le nom des clients : au début de la
  connexion, le serveur demande un nom (qui doit être unique) puis affiche,
devant chaque message, le nom du client qui parle.

- Implémentez un client en interface graphique.

