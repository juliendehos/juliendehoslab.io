---
title: GL2 TP5 - Architecture MVC
date: 2023-08-25
---

L'architecture
[MVC](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur)
(Model-View-Controller) est une façon classique d'organiser un code source. La
partie Model représente les données métiers de l'application, la partie View
permet de présenter les données à l'utilisateur et la partie Controller permet
de gérer les interactions entre les deux autres parties.

Il existe de nombreuses variantes du MVC : MVP, MVVM, Document-View...
L'objectif de ce TP n'est pas d'analyser rigoureusement les différentes
variantes mais de voir en pratique comment et pourquoi il est important
d'architecturer le code selon différentes parties.

# En Haskell

## Petit complément de Haskell

En Haskell, les types algébriques (mot-clé `data`) sont un outil très efficace
pour organiser le code. Ils seront abordés en détail dans le module PFA mais
pour l'instant, vous pouvez considérer qu'ils peuvent être utilisés comme les
`enum` et `struct` du C.

Exemple d'énumération :

```haskell
data Couleur = Rouge | Vert | Bleu
```

Ceci définit le nouveau type `Couleur` qui peut prendre les (nouvelles) valeurs
`Rouge`, `Vert` et `Bleu`.


* * *

Exemple de structure :

```haskell
data Personne = Personne
    { _nom :: String
    , _age :: Int
    }
```

Ceci définit un nouveau type `Personne` qui comporte deux champs. On peut alors
créer une variable de la façon suivante :

```haskell
haskell = Personne "Haskell Curry" 120
```

* * *

faire du pattern matching sur cette variable `haskell` :

```haskell
(Personne nomHaskell ageHaskell) = haskell
```

utiliser les accesseurs :

```haskell
nomHaskell' = _nom haskell
```

ou enfin, créer une nouvelle variable en changeant juste certains champs :

```haskell
husky = haskell { _nom = "Husky Carrell" }
```

## Projet todolist-hs

On veut implémenter un programme permettant de gérer des tâches à faire. Pour
cela, on propose l'architecture suivante :

- la "structure" `Task` modélise une tâche
- le module `Board` permet de modéliser les tâches à faire et les tâches
  faites, ainsi que d'ajouter des tâches et de passer une tâche à faire en
  tâche faite
- le module `View` permet d'afficher les données manipulées
- l'application principale regroupe le tout et implémente un menu principal

* * *

Regardez le projet fourni et terminez son implémentation.

<video preload="metadata" controls>
<source src="files/gl2-tp-mvc-todolist-hs.mp4" type="video/mp4" />
![](files/gl2-tp-mvc-todolist-hs.png){width="70%"}

</video>

* * *

Implémentez également une version qui n'affiche pas les tâches faites. Pour
cela, ajoutez un exécutable `todolist2` dans le fichier `cabal` et écrivez deux
fichiers `Main2.hs` et `View2`.


## Projet naze-hs

On veut implémenter un jeu de labyrinthe un peu nul. Le jeu doit être
utilisable en mode texte et en mode graphique (en utilisant la bibliothèque
`gloss`).

- Dans le projet `naze-hs`, complétez l'implémentation du modèle et
  des tests-unitaires.

- Complétez l'implémentation de l'application graphique.

- Complétez l'implémentation de l'application texte.

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-mvc-naze-hs.mp4" type="video/mp4" />
![](files/gl2-tp-mvc-naze-hs.png){width="60%"}

</video>


# En C++

## Projet todolist-cpp

On veut implémenter un programme permettant de gérer des tâches à faire. Pour
cela, on propose l'architecture suivante :

- la "structure" `Task` modélise une tâche
- le module `Board` permet de modéliser les tâches à faire et les tâches
  faites, ainsi que d'ajouter des tâches et de passer une tâche à faire en
  tâche faite
- le module `View` permet d'afficher les données manipulées
- l'application principale regroupe le tout et implémente un menu principal

* * *

![](uml/todolist-cpp.svg){width="40%"}

* * *

Complétez le projet de base `todolist-cpp`. Suivez le planning proposé dans le
`README.md` en faisant des tests unitaires et des commits réguliers.

Indications : `list`, `find_if`, `list::erase`, `std::getline`, `string::find`, `string::substr`...


## Projet naze-cpp

On veut implémenter un jeu de labyrinthe un peu nul. Le jeu doit être
utilisable en mode texte et en mode graphique (en utilisant la bibliothèque
`SDL`).

- Terminez l'implémentation du modèle. Complétez les tests unitaires.

- Terminez l'implémentation de l'interface texte.

- Terminez l'implémentation de l'interface graphique.

* * *

<video preload="metadata" controls>
<source src="files/gl2-tp-mvc-naze-cpp.mp4" type="video/mp4" />
![](files/gl2-tp-mvc-naze-cpp.png){width="70%"}

</video>

