---
title: GL2 TP3 - Refactoring
date: 2020-05-25
---

Le "refactoring" (ou "réusinage de code") consiste à faire des modifications,
dans le code source, qui ne seront généralement pas perceptibles par
l'utilisateur final. Le refactoring peut aller du simple renommage de variables
jusqu'à une refonte complète de l'architecture de classes. L'objectif du
refactoring est d'améliorer la lisibilité du code et de faciliter sa
maintenance ou son évolution. 

Attention : le refactoring peut "faire gagner du temps plus tard" mais
n'apporte pas "d'améliorations vendables". Il y a donc généralement un
compromis à faire.

Enfin, le refactoring peut être facilité par le langage (typage statique
fort), l'IDE (fonctionnalités de refactoring), le projet (tests unitaires)...


## Exemple en JavaScript

- Regardez le projet, `tp-refactoring/mul2js`.

- Ouvrez la page web dans un navigateur. Pensez à ouvrir la console JS pour
  voir les éventuels messages.

![](files/gl2-refactoring-index1.png){width="50%"}

* * *

- Dans le code de la page web, changez l'`id` de l'`input`. Rafraichissez la
  page dans le navigateur et regardez quand et comment se produit l'erreur.

- Quel est l'inconvénient de la fonction `handle` du script JS ?

* * *

- Copiez les deux fichiers et refactorez le code de façon à réduire le couplage
  entre ces fichiers.  Indication : supprimez la fonction `handle` du fichier
JS et définissez cette fonction "handler" directement dans le fichier HTML.

* * *

- Implémentez une troisième version où la page contient deux `button` et deux
  `span`. Pour cela, dans le fichier JS, implémentez une fonction
`make_handler` qui permet de créer un handler (qui récupère la valeur d'un
`input` et la met dans un `span`).

![](files/gl2-refactoring-index3.png){width="50%"}


## Exemple en Haskell

- Regardez le projet, `tp-refactoring/sqrt-hs`. Compilez, exécutez le programme
  et les tests.

- Modifiez la fonction `mysqrt` de façon a retourner un `Maybe Double`. Quand
  et comment sont détectées les erreurs dans le reste du code ?

- Mettez à jour le reste du code. Vérifiez


## Injection de dépendances

L'injection de dépendances est une technique classique pour gérer des
dépendances de façon générique et dynamique. 

Par exemple, si on veut afficher des messages de log lors du déroulement d'un
programme, on peut utiliser une fonction globale de logging. Si maintenant, on
veut écrire les messages de log dans un fichier, il faut changer la fonction de
logging ou réécrire les appels à la fonction de logging.

Une autre solution (par injection de dépendances) consiste à passer la fonction
de logging à utiliser aux fonctions qui en ont besoin.

- Regardez le projet `log-hs`. Compilez et testez.

* * *

- Implémentez une fonction `mycomputeFile` équivalente à `mycompute` mais qui
  écrit ses messages dans un fichier. Indications : voir le module
[System.IO](https://hackage.haskell.org/package/base/docs/System-IO.html)
(`hPutStrLn`, `openFile`, `hClose`...). Testez les deux fonctions dans le main.

- Refactorez votre code en utilisant l'injection de dépendance, c'est-à-dire
  en passant la fonction de logging en paramètre.

* * *

- Idem en C++, dans le projet `log-cpp` :

    - dans `main1.cpp`, ajoutez une fonction `mycomputeFile` permettant de logger dans un fichier
    - dans `main2.cpp`, implémentez une version où on passe un flux C++ en paramètre
    - dans `main3.cpp`, implémentez une version où on passe une fonction en paramètre
    - dans `main4.cpp`, implémentez une version où on passe un objet en paramètre
    - dans `main5.cpp`, implémentez une version avec des template C++


## Exemple en C++

- Regardez le projet `matrix1`. Compilez et testez.

- Refactorez la classe `Matrix` de façon à stocker les données dans un seul
  `vector` (1D). 


## Composition vs héritage (1)

La composition et l'héritage permettent d'exprimer des relations entre classes
(respectivement "a un" et "est un"). En pratique, il est souvent possible
d'utiliser l'une ou l'autre. La composition a l'avantage d'être plus flexible
et de réduire le couplage mais nécessite plus de "boilerplate code".

- Regardez le projet `store1` et comprenez pourquoi l'héritage public n'est pas
  toujours une bonne idée. 

- Implémentez une version en utilisant de l'héritage privé (et en changeant les
  fonctions en méthodes).

- Implémentez une version en utilisant de la composition.


## Composition vs héritage (2)

- Regardez le projet `board1` et faites le diagramme de classes correspondant.

[//]: ![](uml/gl2-tp-refactoring-board1.svg){width="100%"}

- Implémentez une version utilisant de la composition (voir le diagramme
  suivant).

* * *

![](uml/gl2-tp-refactoring-board2.svg){width="100%"}


