---
title: GL2 TP2 - Intégration continue avec GitHub
date: 2024-01-12
---

L'intégration continue est un ensemble de pratiques et d'outils permettant de
collaborer sur une base de code et d'automatiser des opérations : intégrer des
modifications de code, vérifier la compilation et les tests, packager et
déployer une nouvelle version de l'application...  Ces services sont
disponibles sur les plateformes de gestion de code : GitLab CI/CD, GitHub
Actions...

* * *

> Dans ce TP, vous allez créer des dépôts. Le but est que vous testiez les
> fonctionnalités d'intégration continue. __N'ajoutez pas votre encadrant
> de TP parmi les membres de ces projets.__


## Configuration GitHub

> Pour ce TP, on utilisera [GitHub](https://github.com).
> Créez-vous un compte, si vous n'en avez pas déjà un.

* * *

- Créez un token d'accès : 
    - allez dans les `Settings` de votre compte GitHub
    - allez dans le menu : `Developper settings / Personal access tokens / Tokens (classic)`
    - cliquez sur `Generate new token (classic)`,
    - indiquez un nom de token, activez tous les droits et cliquez sur `Generate token`
    - copiez-collez le token généré dans un ficher (par exemple `~/Documents/montoken.txt`)

* * *

![](files/gl2-ci-github-token1.png){width="50%"}

* * *

- La première fois que vous pusherez vers GitHub, git vous demandera un login/mdp : 
    - cliquez sur annuler pour fermer la fenêtre graphique
    - collez votre token dans le terminal
    - pour le mot de passe, il n'y a rien à mettre : appuyez directement sur `Entrée`


## GitHub Pages

GitHub permet de générer et déployer très facilement des pages web, à partir de
fichiers Markdown.

* * *

- Créez un dépôt vide sur GitHub, de nom `monsupersite`, et clonez-le sur votre
  machine locale.

- Copiez-y les fichiers fournis dans
  `ulco-gl2-etudiant/tp-ci-github/monsupersite` puis faites un add/commit/push.

- Vérifiez que les fichiers apparaissent sur GitHub mais qu'aucune action n'est
  déclenchée.

* * *

- Allez dans l'onglet "Settings", menu "Pages", section "Build and
  deployment". Sélectionnez la branche `main` et cliquez sur "Save".

- GitHub crée automatiquement un workflow qui génère des pages web à partir des
  fichiers Markdown du projet, en utilisant le programme
[Jekyll](https://jekyllrb.com/). Allez dans l'onglet "Actions", regardez les
actions déclenchées et le site web déployé.

* * *

- Ajoutez deux fichiers `intro.md` et `conclu.md` à votre site web, avec des
  liens entre les pages. Vérifiez qu'un workflow est déclenché et le site mis à
jour, après chaque push.

![](files/gl2-ci-github-monsupersite2.png){width="50%"}

![](files/gl2-ci-github-monsupersite3.png){width="50%"}


## Actions, Jobs, Workflows

Le système d'intégration continue de GitHub permet d'exécuter des traitements,
sur leurs serveurs, qui sont déclenchés automatiquement à chaque push.

En simplifiant, une Action est un ensemble de commandes, un Job est un ensemble
d'actions et un Workflow est un ensemble de jobs. Concrètement, un workflow est
décrit par un fichier `.yml` dans le dossier `.github/workflows/`. Ce fichier
contient une section `jobs` décrivant les différents jobs et les actions à
réaliser pour chacun d'entre eux.

* * *

On veut utiliser l'intégration continue pour générer et déployer une
documentation avec mdBook.

- Dans le dossier `ulco-gl2-etudiant/tp-ci-github/masuperdoc`, lancez la
  commande `mdbook serve` pour générer la documentation localement et regardez
le résultat produit dans un navigateur.

- Interrompez la commande `mdbook` précédente et supprimez le dossier `book`
  qu'elle a généré.

* * *

- Créez un dépôt vide `masuperdoc` sur GitHub et clonez-le sur votre machine
  locale.

- Copiez-y les fichiers fournis dans
  `ulco-gl2-etudiant/tp-ci-github/masuperdoc` puis faites un add/commit/push.

- Vérifiez que les fichiers apparaissent sur GitHub mais qu'aucune action n'est
  déclenchée.

* * *

- Allez dans les "Settings", "Pages", "Build and deployment". Sélectionnez la
  source "GitHub Actions". Appuyez sur le bouton "Configure" du workflow
"mdBook".

- GitHub vous propose alors d'éditer un fichier de configuration. Regardez la
  section "jobs" du fichier, pour information, puis cliquez sur "Commit
Changes..." pour ajouter le fichier au dépôt git.

- Vérifiez que le workflow est déclenché et que vous retrouvez bien les jobs et
  actions décrits dans le fichier de configuration.

- Quand le workflow est terminé, vérifiez la page web déployée.


## Construire et tester un projet

Très souvent, on utilise l'intégration continue dans un projet de code pour
vérifier la compilation, les tests unitaires et générer/déployer la
documentation.

* * *

- Créez un dépôt vide `masuperlib` sur GitHub.

- Clonez votre dépôt, copiez-y __tous__ les fichiers du projet
  `ulco-gl2-etudiant/tp-ci/masuperlib` __sauf__ le dossier `.github`. Faites un
add/commit/push. Vérifiez que les fichiers sont visibles sur GitHub.

* * *

- On va d'abord générer et déployer la doc : créez une branche `gh-pages` sur
  GitHub. Vérifiez si des actions sont déclenchées.

- Ajoutez le fichier `.github/workflows/doc.yml`. Vérifiez les actions
  déclenchées et le site web généré.

![](files/gl2-ci-github-masuperlib4.png){width="60%"}

* * *

- On veut maintenant que l'intégration continue compile et teste le projet.
  Vérifiez, en local dans un terminal, que le projet compile mais que les tests
unitaires échouent.

- Ajoutez le fichier `.github/workflows/main.yml` et vérifiez qu'un workflow
  correspondant est déclenché.

- Modifiez le workflow de façon à avoir une action qui compile le projet (via
  CMake) puis une action qui lance les tests unitaires. Vérifiez dans les logs
que les tests unitaires échouent.

- Corrigez le code en local, pushez puis vérifiez que l'intégration continue
  passe, désormais.

* * *

- Ajoutez le tag `v0.1` à votre projet et vérifiez qu'il apparait dans les
  releases/tags de GitHub.

    ```text
    $ git tag v0.1

    $ git push --tags
    Total 0 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
    To https://github.com/juliendehos-test/masuperlib.git
     * [new tag]         v0.1 -> v0.1
    ```

![](files/gl2-ci-github-masuperlib5.png){width="70%"}


## Gestion de dépendances avec Nix

Nix permet de gérer les dépendances logiciels, par exemple pour intégrer une
bibliothèque dans une application, ce qui simplifie l'intégration continue.

* * *

- Dans le dossier `ulco-gl2-etudiant/tp-ci/masuperapp`, lancez la commande
  `nix-build` et vérifiez que le projet compile (pensez à supprimer le dossier
`build` avant, s'il existe).

    ```text
    $ nix-build 
    these 2 derivations will be built:
      /nix/store/9dwyj1hm1ikn4rmjb9nclj3yc3n494d3-masuperlib.drv
      /nix/store/lvi2941yicgjm0jkfsixgn59v8vxypbq-masuperpp.drv
    ...

    $ ./result/bin/masuperapp 
    v0.1
    42
    ```

- Regardez, dans les fichiers `CMakeLists.txt` et `default.nix`, comment est
  fait le lien avec la bibliothèque `masuperlib`.

* * *

- Créez un dépôt vide `masuperapp` sur GitHub, clonez-le et copiez-y tous les
  fichiers de `ulco-gl2-etudiant/tp-ci/masuperapp`. Vérifiez que l'intégration
continue échoue.

- Modifiez le fichier `default.nix` de façon à utiliser votre projet
  `masuperlib` sur GitHub (pour cela, utilisez l'URL de l'archive tar.gz du tag
v0.1). Vérifiez que la compilation fonctionne en local et dans l'intégration
continue de GitHub.

* * *

- On veut ajouter une page web de documentation, générée à partir du fichier
  `index.md`. Dans la configuration `GitHub Pages` de votre projet, activer la
génération des pages à partir de la branche `main`. Vérifiez le workflow
déclenché et le site web déployé.

- Ajoutez le fichier `_config.yml` suivant et vérifiez que Jekyll a bien pris
  en compte votre configuration lors de la génération du site web :

    ```yaml
    theme: jekyll-theme-midnight
    ```


## GitHub Container Registry

GitHub permet de générer et de stocker des packages. Par exemple, on peut
générer une image Docker automatiquement, pour ensuite la télécharger ou la
déployer sur un serveur.

* * *

- Créez un dépôt vide `monsuperserveur` sur GitHub, clonez-le, copiez-y les
  fichiers de `ulco-gl2-etudiant/tp-ci/monsuperserveur` et faites un
add/commit/push.

- Compilez le projet avec un `nix-build`, lancez le programme généré et
  testez-le en allant à l'URL <http://localhost:3000>.

    ```text
    $ nix-build
    this derivation will be built:
      /nix/store/w6aw5zr67idxym0mjsq3bw9jmv3vci02-monsuperserveur-1.0.drv
    ...

    $ ./result/bin/monsuperserveur
    Setting phasers to stun... (port 3000) (ctrl-c to quit)
    ```

* * *

- Ajoutez un workflow `.github/workflow/main.yml` pour vérifier que le projet
  compile et passe les tests (inspirez-vous de `masuperapp`).  

* * *

- Le projet fourni permet de construire une image Docker contenant le programme
  implémenté. Testez-le avec les commandes suivantes.

    ```text
    $ nix-build docker.nix
    these 11 derivations will be built:
    ...

    $ docker load -i result 
    4ee3af161ea5: Loading layer [==============>]  1.812MB/1.812MB
    ...

    $ docker images
    REPOSITORY        TAG       IMAGE ID       CREATED        SIZE
    monsuperserveur   latest    b407362254f4   52 years ago   44MB

    $ docker run --rm -it -p 3000:3000 monsuperserveur:latest
    Setting phasers to stun... (port 3000) (ctrl-c to quit)

    $ docker rmi -f monsuperserveur
    ```

* * *

- Regardez le workflow `.github/workflows/docker.yml` puis faites ce qu'il faut
  pour le déclencher sur GitHub. Vérifiez que le package est bien créé.

![](files/gl2-ci-github-monsuperserveur2.png){width="80%"}

* * *

- Testez l'image Docker construite par GitHub, avec un `docker run` sur votre
  machine.

- S'il vous reste du temps, modifiez le code de façon à ajouter une route `add`
  qui retourne la somme de deux nombres donnés en paramètre, et vérifiez que
  l'image Docker construite par GitHub est bien mise à jour.


