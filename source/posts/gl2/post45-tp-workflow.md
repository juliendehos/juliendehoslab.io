---
title: GL2 TP7/8 - Workflow
date: 2023-08-25
---

> Vous disposez de 2 séances pour réaliser ce TP.
> Prenez donc le temps de bien comprendre le code 
> fourni et d'utiliser correctement les méthodes
> et outils demandés.

## Introduction

L'objectif de ce TP est de réaliser un projet de développement logiciel
en utilisant des outils et méthodes classiques : cycle itératif,
git, doxygen, tests unitaires, intégration/déploiement continu…

Le projet à réaliser est un jeu de tictactoe avec une interface texte et une
interface graphique. Voir le 
[travail de spécification réalisé en TD](post15-td-uml.html#étude-de-cas).

* * *

<video preload="metadata" controls>
<source src="files/gl-workflow-tictactoe.mp4" type="video/mp4" />
![](files/gl-workflow-tictactoe.png)

</video>

## Mise en place du projet

Module C++ :

- Allez dans le dossier `ulco-gl2-etudiant/tp-workflow/cpp`
- Regardez les fichiers fournis (code C++, configuration CMake...)
- Lancez VSCode, compilez, lancez les tests et l'application
- Générez la doc et regardez le résultat produit

* * *

Module Python :

- Allez dans le dossier `ulco-gl2-etudiant/tp-workflow`
- Regardez les fichiers fournis (code Python, `setup.py`...)
- Testez l'application, avec la commande `nix-shell --run "python scripts/tictactoe_gui.py"`

* * *


[//]: # (
Intégration continue (si vous avez un compte gitlab validé) :
- Déplacez le fichier `ulco-gl2-etudiant/tp-workflow/.gitlab-ci.yml` vers le
  dossier `ulco-gl2-etudiant/`
- Committez et pushez
- Allez sur l'interface web de gitlab et vérifiez qu'un pipeline se déroule
  correctement
- Vérifiez que la documentation est bien déployée sur l'URL en `gitlab.io`
* * *
)

## Réalisation du projet

Pour réaliser le projet, on propose la planification suivante :

- Milestone 1 : moteur de jeu
    - Issue 1 : affichage de la grille (validation : manuelle + Tests Unitaires)
    - Issue 2 : jouer des coups, avec détection de case libre mais sans détection de victoire/égalité (validation : TU)
    - Issue 3 : détection de victoire (validation : TU)
    - Issue 4 : détection d'égalité (validation : TU)

* * *

- Milestone 2 : interface texte
    - Issue 5 : saisir un coup (validation : exécution manuelle)
    - Issue 6 : dérouler une partie de jeu complète (validation : exécution manuelle)

* * *

- Milestone 3 : interface graphique (Python)
    - Issue 7 : intégration du moteur de jeu (validation : manuelle)
    - Issue 8 : boutons rejouer/quitter + message de status (validation : manuelle)
    - Issue 9 : dessin du jeu (validation : manuelle)
    - Issue 10 : gestion du clic + jouer le coup (validation : manuelle)

* * *

Dans l'interface web de gitlab, ajoutez les milestones et issues prévus, puis
réalisez le projet selon la démarche suivante : 

- choisissez l'issue à traiter
- écrivez les code/tests/doc correspondant à l'issue (pas plus, pas moins) 
- effectuez la validation (tests unitaires, tests manuels…)
- committez, pushez, vérifiez l'intégration continue et fermez l'issue
- si l'issue termine un milestone, ajoutez un tag
- recommencez avec l'issue suivante

## Bonus

Si vous avez le temps, implémentez des bots (random, monte-carlo...) pour
controler le joueur Vert.


