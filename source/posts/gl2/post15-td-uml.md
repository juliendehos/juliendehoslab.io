---
title: GL2 TD - UML
date: 2022-07-15
---

> Sauf mention explicite, les exercices sont indépendants.

# Diagrammes d'activité

## Faire le café

Voici comment faire du café avec une cafetière à piston :

1. Faire chauffer l'eau.
2. Mettre du café dans la cafetière.
3. Verser l'eau chaude dans la cafetière et fixer le piston.
4. Laisser infuser 4 minutes.
5. Presser le piston puis servir.

Réalisez le diagramme d'activité correspondant.

## Recette de cuisine

Réalisez un diagramme d'activité de la recette de mousse au chocolat suivante :

1. Faites fondre le chocolat au bain-marie. 
2. Pendant que le chocolat fond. Cassez les œufs en séparant les jaunes des
   blancs.
3. Battez les blancs en neige, avec une pincée de sel.
4. Une fois le chocolat fondu, versez-le doucement sur les jaunes, en remuant à
   la spatule.
5. Incorporez délicatement et progressivement les blancs d'œufs battus.
6. Faites prendre la mousse 3 heures minimum au réfrigérateur.
7. Servez bien frais.

## Traiter ses mails professionnels

Voici une méthode classique pour traiter ses mails professionnels :

- Prendre un café pour se donner du courage.
- Ouvrir la boite mail.
- S'il y a des mails, en ouvrir un au hasard.
- Si le sujet contient au moins 3 fois le mot "urgent", traiter le mail puis le
  mettre à la poubelle.
- Sinon mettre le mail directement à la poubelle.
- Recommencer tant qu'il reste des mails.
- Fermer la boite mail.
- Prendre un café pour s'en remettre.

Réalisez le diagramme d'activité correspondant.

## Livrer une pizza

Une pizzeria livre des commandes effectuées par téléphone, avec paiement à la
livraison. Elle est organisée en trois postes :

- le gestionnaire enregistre les commandes par téléphone et encaisse le
  paiement final;
- le cuisinier fabrique les pizzas enregistrées et les conditionne;
- le livreur livre les pizzas conditionnées et prend le paiement du client.

Réalisez le diagramme d'activité correspondant.

## Enregistrer une réparation dans un garage automobile

Au garage *Repar'toto*, le chef d'atelier doit enregistrer une fiche de
réparation avant de prendre en charge une auto. S'il s'agit d'un nouveau
véhicule, le logiciel demande de saisir ses informations et l'enregistre dans
sa base de données.  Sinon, il affiche la liste des véhicules et demande de
sélectionner celui qui convient. Le logiciel demande ensuite d'indiquer la date
de la demande de réparation pour vérifier si le véhicule est sous garantie. Si
le véhicule n'est plus sous garantie, le logiciel demande si la réparation est
prise en charge par l'assurance et, le cas échéant, il demande de sélectionner
l'assurance.

Réalisez le diagramme d'activité correspondant.

# Diagrammes de cas d'utilisation

## Cabinet médical

Plusieurs médecins se sont regroupés dans un cabinet médical et partagent une
secrétaire et un comptable, ainsi qu'un système d'information.  Le système gère
les rendez-vous (ajouter, annuler, consulter), les dossiers médicaux
(consulter, modifier) et le bilan comptable (consulter, modifier). En plus
d'avoir accès à la partie médicale, les médecins peuvent également consulter
l'ensemble du système d'information.

1. Identifiez les acteurs.

2. Faites un diagramme de cas d'utilisation.

## Station-essence

Dans une station-essence, les clients se servent à la pompe et le pompiste
remplit les cuves.

1. Identifiez les acteurs puis faites un diagramme de cas d'utilisation.

2. À la fin de son service, un pompiste fait le plein de sa voiture
   personnelle. Que faut-il modifier dans le diagramme pour prendre en compte
   ce scénario ?

3. Il existe également des techniciens : ce sont sont des pompistes qui, en
   plus, effectuent des opérations de maintenance.  Que faut-il modifier dans
   le diagramme pour prendre en compte ce scénario ?

## Distributeur Automatique de Billets

Faites un diagramme de cas d'utilisation du DAB suivant.

Le distributeur permet à un client de retirer des billets et de consulter le
solde du compte. Pour cela, il doit s'identifier et peut imprimer un ticket.
L'identification est limitée à trois essais. Enfin, le distributeur permet de
« retirer rapidement », c'est-à-dire de retirer des billets mais sans ticket et
pour un montant prédéterminé.

## Caisse enregistreuse

Réalisez un diagramme de cas d'utilisation du système de caisses enregistreuses
suivant, décrit par la société qui l'utilise.

* * *

1. Un client arrive à la caisse avec des articles.
2. Pour chaque article :
    1. le caissier enregistre le numéro d'identification
    2. le caissier indique la quantité, si celle-ci est supérieur à 1
3. La caisse affiche le prix et le libellé de chaque article.
4. Lorsque tous les articles ont été passés, le caissier signale la fin de la
   vente.
5. La caisse affiche le montant total.
6. La caisse transmet les informations de la vente au système de gestion de
   stock.
7. Le client réalise le paiement via un terminal bancaire qui confirme le
   paiement au système.
8. Tous les matins, les caisses sont initialisées par le responsable de
   magasin.

# Diagrammes de classes

## Réparation automobile

On veut modéliser les réparations dans un garage automobile.  Une réparation
est définie par sa date d'enregistrement, sa date de fin et une description
textuelle. Une réparation met en jeu un véhicule (avec une immatriculation), un
travail (avec un nombre d'heures) et éventuellement des pièces de rechange
(avec des références). Le système contient des véhicules concernés par une ou
plusieurs réparations. Une réparation peut nécessiter plusieurs travaux ou ne
pas encore avoir commencé.

## Conception automobile

Une voiture a un chassis (avec un numéro), quatre roues (avec des dimensions),
un moteur (avec un nombre de CV) et utilise du carburant (d'un certain type).
On peut changer les roues et le moteur d'une voiture.

## Classification des mammifères

Les *Mammalia* sont divisés en deux groupes : les *Prototheria* (par exemple,
l'Échidné Australien) et les *Theria*. Ces derniers sont décomposés en
*Metatheria* (par exemple, le Kangourou et le Koala) et en *Eutheria* (par
exemple, l'Éléphant et le Renard).

## Consultation médicale

On veut gérer des consultations médicales. Un patient a un numéro médical et
une date de naissance. Un médecin a un numéro d'ordre. Les patients et les
médecins sont des personnes et ont un nom et un prénom. Un médecin donne une
consultation à un patient à une date donnée. Une consultation peut donner lieu
à une ordonnance (avec un numéro et un contenu).

# Diagrammes d'états

## Distributeur Automatique de Billets

Faites un diagramme d'états du DAB suivant.

Quand le distributeur reçoit la carte, il demande le code puis le montant du
retrait puis termine.  Chaque saisie est vérifiée et redemandée si invalide. La
saisie du code est limitée à trois tentatives.

## Boite mail

Après identification, on arrive dans la boite de réception. Celle-ci permet
d'afficher un mail, de supprimer un mail et de quitter.  Depuis la boite mail,
on peut également lancer l'éditeur de mail pour écrire un mail.  Une fois le
mail envoyé, on revient dans la boite de réception.

# Diagrammes de séquence

## Écrire un mail

On considère la boite mail de la section sur les diagrammes d'états.  Un
utilisateur se connecte, écrit puis envoie un mail et quitte. Il s'est trompé
une fois en se connectant à sa boite mail.  Faites un diagramme de séquence
correspondant.

## Lire un mail

Cette fois, l'utilisateur se connecte immédiatement, lit un mail, le supprime
et quitte.  Faites un diagramme de séquence correspondant.

## Faire le café

Faites un diagramme de séquence correspondant à l'exercice de même nom donné
dans la section sur les diagrammes d'activité.

# Étude de cas

## Jeu de Tictactoe

On veut une application de Tictactoe simple. L'utilisateur doit pouvoir jouer
sur son ordinateur en entrant successivement les coups à jouer (joueur rouge et
joueur vert). À chaque coup, l'application affiche l'état du jeu et détecte la
fin de partie.  On veut pouvoir jouer via une interface texte (saisie des coups
au clavier) et via une interface graphique (saisie des coups à la souris).
L'interface graphique doit proposer un bouton pour recommencer une partie (à la
fin ou au cours d'une partie).

[Vous implémenterez ce projet en TP](post45-tp-workflow.html) mais dans un
premier temps, on veut réfléchir à sa spécification et à sa conception, via des
diagrammes UML.

* * *

- Faites un diagramme de cas d'utilisation illustrant les principales
  interfaces et actions de l'application.

- Faites un diagramme d'activité de l'application texte et un pour
  l'application graphique.

- Faites un diagramme de la séquence suivante : l'utilisateur lance
  l'application graphique, joue un coup puis recommence la partie.

* * *

- Faites un diagramme de classes simple et écrivez un pseudo-code du programme
  principal de l'application texte.

- Écrivez un pseudo-code des principales fonctions de l'application graphique
  (clic souris, bouton…) et complétez votre diagramme de classes.

- Faites un diagramme de la séquence suivante : l'utilisateur lance
  l'application graphique (qui construit et réinitialise le jeu) puis joue un
  coup en cliquant puis recommence la partie.

