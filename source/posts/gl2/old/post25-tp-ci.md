---
title: GL2 TP2 - Intégration continue
date: 2020-05-25
---

L'intégration continue est un ensemble de pratiques permettant de fusionner le
travail des développeurs dans la "branche principale" du logiciel et de
vérifier que le code est toujours valide.  Concrêtement, il s'agit de réaliser
automatiquement une suite d'opérations, notamment d'intégrer les modifications
de code, de revérifier la compilation et les tests, voire même de déployer une
nouvelle version de l'application en production (livraison continue).

Gitlab permet de faire facilement de l'intégration continue. Il suffit
d'ajouter, au dépôt Git, un fichier `.gitlab-ci.yml` décrivant les "jobs" du
"pipeline" à exécuter lors des modifications de code. Voir la doc sur [GitLab
CI/CD](https://gitlab.com/help/ci/quick_start/README.md).


## Intégration continue simple

- Connectez-vous sur Gitlab et créez un nouveau dépôt `masuperlib`.

- Clonez votre dèpôt et copiez-y les fichiers du projet `tp-ci/masuperlib`.
  Vérifiez que vous voyez bien vos fichiers sur Gitlab.

- En vous inspirant du [template
  C++](https://gitlab.com/help/ci/examples/README.md#cicd-templates), ajoutez
  de l'intégration continue à votre projet, pour vérifier qu'il compile.
  Indication : utilisez une image `debian:buster` avec un job `build`
  uniquement et sans artefact.

* * *

- lancez un `nix-build` dans votre projet et regardez les étapes réalisées.
  Normalement, une erreur est détectée et les tests ne passent pas. Laissez
  comme ça pour l'instant.

- Réécrivez votre intégration continue, avec cette fois l'image `nixos/nix`.
  Vérifiez que la compilation et les tests sont bien lancés.

- Corrigez l'erreur dans le code, pushez et vérifiez que l'intégration continue
  passe désormais.

* * *

![](files/gl2-tp-ci-masuperlib1.png){width="100%"}


## Archives d'un dépôt Gitlab

Gitlab permet de télécharger une archive du code (sans l'historique Git) et de
fixer des tags de version. Ceci peut être utile pour réutiliser des projets, en
alternative aux modules Git.

* * *

![](files/gl2-tp-ci-masuperlib2.png){width="90%"}

* * *

- Dans votre projet Gitlab `masuperlib`, ajoutez un tag de version avec les
  commandes suivantes.

    ```sh
    git tag v0.1

    git push --tags
    ```

- Sur Gitlab, vérifiez que le tag apparait et que vous pouvez télécharger une
  archive `tar.gz` de cette version.

- Allez dans le projet `tp-ci/masuperapp` et regardez les fichiers fournis.

- Modifiez le fichier `default.nix` de façon à télécharger une archive de votre
  projet `masuperlib` depuis Gitlab. 


## Gitlab Pages

Gitlab permet de faire des sites web statiques, par exemple pour héberger une
page perso, la documentation d'un projet, une application côté-client...  Pour
cela, il suffit d'ajouter un job `pages` dans l'intégration continue et de
copier les fichiers à servir dans un dossier `public` (et indiquer ce dossier
dans les `artifact`). Les fichiers sont alors accessibles à l'adresse
`https://<login>.gitlab.io/<projet>`.  Voir la doc de [Gitlab
Pages](https://docs.gitlab.com/ee/user/project/pages/).

* * *

- Sur Gitlab, créez un nouveau projet `masuperdoc` et mettez-y les fichiers de
  `tp-ci/masuperdoc`.

- Implémentez une intégration continue déployant la page web (indication :
  copiez juste le fichier HTML, pour l'instant). Sur Gitlab, vérifiez que
  l'artefact est bien produit à la fin du pipeline. Enfin, vérifiez que votre
  page est accessible sur son adresse "gitlab.io".

- On veut maintenant que l'intégration continue génère et déploie une doc
  Sphinx. Initialisez une doc Sphinx avec la commande `sphinx-quickstart`
  (demandez de séparer les sources et le build).

* * *

- Lancez un `nix-build` et regardez les fichiers produits.

- Modifiez votre intégration continue de façon à générer et déployer votre doc
  (et, bien-sûr, ne versionnez pas le dossier de build). Vérifiez l'artefact et
  la page web.

* * *


![](files/gl2-tp-ci-masuperdoc1.png){width="90%"}

* * *

![](files/gl2-tp-ci-masuperdoc2.png){width="90%"}


## Pipelines

L'intégration continue d'un projet est organisée selon un "pipeline". Un
pipeline est une succession de "stages" (`build`, `test`, `deploy`). Un stage
regroupe des "jobs" qui peuvent être réalisés en parallèle. Ceci permet
d'optimiser le processus d'intégration continue : implémenter des
configurations différentes, réaliser des jobs en parallèle, interrompre le
pipeline si des jobs pré-requis échouent, réutiliser le résultat de jobs
précédents...

* * *

- Le projet `tp-ci/multisdl` contient une application graphique utilisant la
  bibliothèque SDL. Il implémente une version native et une version Wasm, ainsi
  que des tests unitaires. Regardez rapidement comment est organisé le projet,
  notamment les fichiers `.nix` et le `Makefile`. 

- Testez les différentes cibles du projet en utilisant des commandes
  `nix-shell`. Par exemple, pour construire et lancer les tests :

    ```sh
    $ nix-shell shell-native.nix --run "make test"
    ...

    $ nix-shell shell-wasm.nix --run "make wasm"
    ...
    ```

* * *

- "Mettez le projet sur Gitlab" et ajoutez une intégration continue
  (compilations `app` et `wasm`, test, déploiement de la version Wasm).
  Indication : utilisez l'image `trzeci/emscripten:1.38.48` pour construire la
  version Wasm (plus rapide) et réutilisez les fichiers produits pour le
  déploiement. Vérifiez que l'application est disponible sur l'URL de
  déploiement.

* * *

![](files/gl2-tp-ci-multisdl1.png){width="70%"}

* * *

![](files/gl2-tp-ci-multisdl2.png){width="70%"}


## Construction et déploiement d'une image Docker

Les Gitlab Pages permettent d'héberger des fichiers statiques. Pour des
applications web plus complexes, on a souvent besoin d'implémenter notre propre
application serveur, qu'il faut ensuite exécuter sur un système d'exploitation,
et finalement sur une machine réelle.

Dans cet exercice, vous allez prendre une application serveur codée en Haskell,
en générer une image Docker (un "système d'exploitation léger" contenant votre
application), et la déployer sur Heroku (un service de cloud permettant
d'exécuter des images Docker).

* * *

- Regardez rapidement le code du projet `tp-ci/remotemul2`, notamment le code
  Haskell et le fichier `.cabal`.

- Lancez un `nix-shell`. Vérifiez les tests unitaires avec `cabal test`.
  Lancez l'application avec `cabal run remotemul2` et testez-la dans un
  navigateur (pour l'instant seule la page principale fonctionne). Enfin,
  quittez l'application puis le `nix-shell`.

![](files/gl2-tp-ci-remotemul2-1.png){width="60%"}

* * *

- Regardez rapidement le fichier `docker.nix` puis générez et testez l'image
  Docker avec les commandes suivantes (ici, on utilise Nix mais une méthode
  plus classique serait d'utiliser un `Dockerfile`). 

    ```sh
    nix-build docker.nix
    docker load < result
    docker images
    docker run --rm -it -p 3000:3000 remotemul2
    ```

* * *

- Vous avez maintenant une image Docker de votre application, avec toutes ses
  dépendances. Vous allez maintenant envoyez cette image dans un service de
  cloud qui lancera votre image et la rendra disponible sur Internet. Pour
  cela, créez-vous un compte gratuit sur
  [Heroku](https://signup.heroku.com/login).

- Dans un terminal, connectez-vous à Heroku et créez une application sur
  Heroku, en utilisant les commandes suivantes (remplacez `<monlogin>` par
  votre login Heroku, pour éviter les conflits de noms).

    ```sh
    heroku container:login
    export APP_NAME=remotemul2-<monlogin>
    heroku create $APP_NAME
    ```

* * *

- Lancez les commandes suivantes pour envoyer l'image Docker de votre machine
  vers les serveurs Heroku et pour la lancer.

    ```sh
    docker tag remotemul2 registry.heroku.com/$APP_NAME/web
    docker push registry.heroku.com/$APP_NAME/web
    heroku container:release web --app $APP_NAME
    heroku open --app $APP_NAME
    ```

![](files/gl2-tp-ci-remotemul2-2.png){width="60%"}

* * *

- Votre application est maintenant accessible en ligne. Cependant si vous
  modifiez le code, il faut regénérer l'image Docker et la réenvoyer sur
  Heroku. Une autre solution est d'intégrer ces étapes de déploiement dans une
  intégration continue. Pour cela, commencez par créer un projet `remotemul2`
  sur Gitlab.

* * *

- Pour autoriser votre dépôt Gitlab à déployer sur votre compte Heroku,
  récupérez votre `API KEY` dans la page web de Heroku (page "Account
  settings", section "API KEY", bouton "Reveal").

![](files/gl2-tp-ci-remotemul2-3.png){width="70%"}

* * *

- Sur Gitlab, mettez votre clé dans une variable `HEROKU_API_KEY` (page
  "Settings, CI/CD", section "Variables"). Mettez également une variable
  `APP_NAME` contenant le nom de votre application Heroku.

![](files/gl2-tp-ci-remotemul2-4.png){width="100%"}

* * *

- Clonez votre projet Gitlab, copiez-y vos fichiers puis pushez. Vérifiez que
  le pipeline d'intégration continue est bien réalisé. Regardez les étapes de
  l'integration continue dans le fichier `.gitlab-ci.yml`.

* * *

![](files/gl2-tp-ci-remotemul2-5.png){width="70%"}

* * *

- Vérifiez que votre application Heroku fonctionne pour la page principale mais
  pas pour la route `/mul2/21`.

- Décommentez les lignes de code dans `app/Main.hs` et pushez.

- Vérifiez que le pipeline d'intégration continue est de nouveau réalisé et que
  l'application est automatiquement déployée sur Heroku. Vérifiez que la route
  `/mul2/21` fonctionne désormais.


## S'il vous reste du temps

Mettez en place un projet C++ classique avec intégration continue : compilation
avec CMake, tests unitaires avec Catch2 et documentation avec Doxygen (et
déployée sur l'URL "gitlab.io").

