---
title: GL2 TP1 - Interfacer des langages
date: 2022-07-15
---

TODO

py-fibo1
py-fibo2 : cpp, plot
py-logger : classe

bonus
py-fibo2 : gui
py-logger : date



Un logiciel utilise parfois plusieurs langages. Ceci peut être intéressant pour
réutiliser du code existant ou pour optimiser certaines parties du code. Par
exemple, il arrive souvent qu'on implémente les modules nécessitant beaucoup de
performances en Fortran, C ou C++, et le reste du code dans un langage de plus
haut-niveau comme JavaScript ou Python. 

Un autre exemple classique est de reprendre une base de code C ou C++ et de la
compiler en JavaScript pour l'exécuter dans un navigateur web. C'est ce qu'on
va faire dans ce TP, en utilisant Emscripten. Emscripten permet de générer du
bytecode Asmjs ou Wasm à partir de code C++.

Quelques docs sur JavaScript :

- [MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript)
- [w2schools](https://www.w3schools.com/js/)


# Web

## Premiers scripts

Le projet `tp-ffi/web-fibo1` contient une page web et un fichier JavaScript. 

- Modifiez la page web `test_fibo.html` de façon à afficher la suite de
  Fibonacci en utilisant le script `fibo.js` :

- Écrivez un fichier `repeat.js` contenant une fonction `repeatN`. Cette
  fonction doit prendre un nombre `n` et une fonction `f` et appliquer la
  fonction `f` sur tous les nombres de 1 à `n`.

- Écrivez une page web `test_repeat.html` qui affiche la suite de Fibonacci en
  utilisant votre fonction `repeatN`.

* * *

![](files/gl-scripts-js-web-fibo1.png)


## Afficher un graphique

Le projet `web-fibo2` affiche un graphique de la fonction $\sin$ dans
une page web.

- Réutilisez le script `fibo.js` de l'exercice précédent et modifiez la page
  web de façon à tracer les 10 premiers termes de la suite de fibonacci.


* * *

![](files/gl-scripts-js-web-fibo2.png)


## Interfacer du code C++

[Emscripten](https://emscripten.org/) permet d'utiliser du code C++ dans du
code JS. Pour cela, il faut écrire du code de liaison C++/JavaScript et
compiler tout le code C++ dans un format utilisable en JavaScript (Asmjs ou
Wasm).

* * *

- Regardez le projet `web-fibo3` (`fibo.cpp`, `Makefile` et `index1.html`).
  Comment est fait le lien entre le code C++ et le code JavaScript ?

- Ouvrez un `nix-shell` puis lancez un `make` (pour compiler le code et lancer
  un serveur web). Regardez le résultat à l'url
  <http://localhost:8080/index1.html>. Pensez à quitter le `nix-shell`.

- Ajoutez un fichier `sinus.cpp` implémentant la fonction $f(x) =
  \sin(2\pi(ax + b))$ ainsi que son interface JavaScript.  Modifiez la page
  `index1.html` de façon à tracer également votre fonction `sinus` avec
  $a=2$ et $b=0.25.$

- Modifiez le fichier `index2.html` de façon à tracer également votre
  fonction `sinus`, dont on pourra régler les paramètres.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-js-web-fibo3.mp4" type="video/mp4" />
![](files/gl-scripts-js-web-fibo3.png)

</video>



## Générer une image en C++/WebAssembly

Le projet `web-sinus` implémente une page web avec un canvas calculé
par du code C++/WebAssembly. Pour l'instant, le canvas est rempli en rouge.

- Compilez et testez le projet.

- Ajoutez un paramètre à la méthode `Sinus::update` pour indiquer la quantité
  de rouge voulue.

- Ajoutez un `range` dans la page de façon à pouvoir modifier dynamiquement
  la quantité de rouge.

- Modifiez votre projet de façon à afficher un dégradé selon un sinus dont on
  peut modifier la fréquence et la phase.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-js-web-sinus.mp4" type="video/mp4" />
![](files/gl-scripts-js-web-sinus.png)

</video>


# Electron

[Electron](https://electronjs.org/) est un framework qui permet de développer
des interfaces graphiques. Il est basé sur Chromium et Node.js, et est utilisé
dans des logiciels clients comme Visual Studio Code, Discord... Le cas
d'utilisation typique de Electron est de développer des applications de bureau
en utilisant les technos web classiques (HTML, CSS, JS).

## Application desktop avec Electron

- Regardez le projet `electron-fibo` et comprenez, dans les grandes lignes, le
  fonctionnement d'une application Electron (voir éventuellement le [tutoriel
  Electron](https://electronjs.org/docs/tutorial/first-app)).

- Modifiez le projet de façon à implémenter une application qui affiche un
  graphique de la suite de Fibonacci jusqu'à un nombre réglable via
  l'interface. Vous pouvez reprendre la fonction JavaScript `fiboIterative` des
  premiers exercices.

* * *

![](files/gl-scripts-js-web-electron.png)



# Node.js

[Node.js](https://nodejs.org/en/) est un environnement d'exécution JavaScript
autonome (i.e., non intégré dans un navigateur). Node.js est souvent utilisé
pour développer des serveurs web asynchrones.


## Scripts simples


- Regardez le projet `node-fibo1`, notamment comment définir du code JavaScript
  et comment le réutiliser dans un script Node.js.

- Modifiez le script `test_fibo.js` de façon à afficher la suite de Fibonacci,
  en utilisant le script `fibo1.js` ou `fibo2.js`.

- Reprenez le fichier `repeat.js` des exercices précédents et 
  écrivez un script `test_repeat.js` qui affiche la suite de Fibonacci
  en utilisant la fonction `repeatN`.

* * *

```bash
$ node test_fibo.js 

fiboIterative(0) = 0
fiboIterative(1) = 1
fiboIterative(2) = 1
fiboIterative(3) = 2
fiboIterative(4) = 3
fiboIterative(5) = 5
fiboIterative(6) = 8
fiboIterative(7) = 13
fiboIterative(8) = 21
fiboIterative(9) = 34
```


## Interfacer du code C++

Le projet `node-fibo2` contient un module C++ (`fibo.cpp`), un script utilisant
ce module (`test_fibo.js`) et un `Makefile`.

* * *

- Regardez, compilez et testez le code fourni (dans un `nix-shell`).

- Reprenez le module `sinus.cpp` des exercices précédents, écrivez un script
  `test_sinus.js` et ajoutez une règle de construction/exécution dans
  le `Makefile`.

```bash
[nix-shell]$ make sinus
...
sinus.sinus(1, 0, 0.5) = 1.2246467991473532e-16
sinus.sinus(1, 0, 0.25) = 1
```


## Application web

Le projet `node-fiboweb` contient un serveur Node/Express (API JSON + fichiers
statiques) ainsi que du code C++ interfacé via emscripten (Wasm et Asmjs).

* * *

- Regardez et testez le projet fourni (dans un `nix-shell`).

- Ajoutez un fichier `sinus.cpp` avec la fonction `sinus` habituelle.
  Normalement, la configuration fournie devrait l'intégrer directement (Wasm + Asmjs).

- Modifiez le serveur de façon à fournir également une API pour votre `sinus`.

- Modifiez la page web de façon à afficher des graphiques dynamiques, en
  utilisant le code Wasm.

- S'il vous reste du temps, calculez les graphiques en utilisant l'API, via des
  requêtes XHR.


* * *


<video preload="metadata" controls>
<source src="files/gl-scripts-js-node-fiboweb.mp4" type="video/mp4" />
![](files/gl-scripts-js-node-fiboweb.png)

</video>


# Haskell

## Appeler du code C dans un programme Haskell

Haskell permet d'appeler du code C via une FFI (Foreign Function Interface).
Ceci est généralement utilisé pour implémenter une interface Haskell à une
bibliothèque C existante.

- Regardez le projet `hs-c`, notamment l'utilisation de la FFI de Haskell.

- Compilez et testez.

- Ajoutez une fonction `add3` dans le code C et appelez-là depuis le code
  Haskell.


## Transpiler un programme Haskell vers JavaScript

(ne pas faire : pas à jour)

Le compilateur GHC produit du code natif, exécutable via le système
d'exploitation. Il existe également le compilateur GHCJS, équivalent mais qui
produit du code JavaScript, exécutable via un navigateur web. Attention, le
compilateur GHCJS est moins mature que GHC, ce qui peut parfois nécessiter de
réparer ou de recompiler des paquets.

- Regardez le projet `hs-js1`.

- Lancez un `nix-shell` puis un `cabal build`. Ceci produit du code JavaScript
  et une page web permettant de l'appeler. Ouvrez cette page et regardez la
console JavaScript.

- GHCJS fournit également une FFI pour interagir avec le code JavaScript.
  Regardez le projet `hs-js2`. Compilez et testez.


