---
title: GL2 TP7/8 - Workflow
date: 2022-07-15
---

> Vous disposez de 2 séances pour réaliser ce TP.
> Prenez donc le temps de bien comprendre le code 
> fourni et d'utiliser correctement les méthodes
> et outils demandés.

## Introduction

L'objectif de ce TP est de réaliser un projet de développement logiciel
en utilisant des outils et méthodes classiques : cycle itératif,
git, doxygen, tests unitaires…

Le projet à réaliser est un jeu de tictactoe avec une interface texte et une
interface graphique. Voir le 
[travail de spécification réalisé en TD](post15-td-uml.html#étude-de-cas).

* * *

<video preload="metadata" controls>
<source src="files/gl-workflow-tictactoe.mp4" type="video/mp4" />
![](files/gl-workflow-tictactoe.png)

</video>

## Mise en place du projet

Dépôt git/gitlab :

- Créez un dépôt `tictactoe` sur votre compte gitlab. 

- Copiez et ajoutez les fichiers fournis (`ulco-gl2-etudiant/tp-workflow`) dans
  votre nouveau dépôt.  N'oubliez pas le fichier `.gitignore` mais n'ajoutez
  pas le fichier `.gitlab-ci.yml` pour l'instant. 

- Indiquez vos nom/prénom/groupe dans le fichier `README.md`.

- Commitez/pushez et vérifiez sur l'interface web de gitlab.

* * *

Intégration continue :

- Compilez le projet CMake manuellement et testez les programmes (interface
  texte, tests unitaires).

- Copiez et ajoutez le fichier d'intégration continue `.gitlab-ci.yml` à
  votre dépôt. Regardez et comprenez ce qu'il contient. Commitez/pushez et
  regardez le résultat sur l'interface web de gitlab (onglet « CI/CD »,
  rubriques « Pipelines » et « Jobs »).

- Regardez et comprenez le job « pages » : description dans le
  `.gitlab-ci.yml`, « Job artifacts » dans l'interface web et déploiement sur
  l'URL `gitlab.io`.

* * *

- Modifiez le code C++ de façon à y ajouter une erreur de compilation.
  Commitez/pushez et regardez ce qu'il se passe au niveau de l'intégration
  continue. 

- Corrigez l'erreur de compilation, ajoutez une erreur dans les tests unitaires
  et regardez le résultat de l'intégration continue.

* * *

Documentation de code :

- Générez une documentation de code avec Doxygen. Le fichier de configuration 
  est fourni mais vous devrez reformater les commentaires de code.

- Vérifiez que la documentation est générée par l'intégrationn continue et
  déployer sur l'URL de déploiement (`gitlab.io`).

* * *

Interface graphique (JavaScript) :

- Pour développer l'interface graphique du tictactoe, on propose de réutiliser
  le code C++ en le compilant en WebAssembly. Regardez le code
fourni, notamment l'interfaçage du code C++ et les scripts.

- Vérifiez que l'interface graphique est générée et déployée par l'intégration
  continue.

## Réalisation du projet

Pour réaliser le projet, on propose la planification suivante :

- Milestone 1 : moteur de jeu
    - Issue 1 : affichage de la grille (validation : Tests Unitaires)
    - Issue 2 : jouer des coups, avec détection de case libre mais sans détection de victoire/égalité (validation : TU)
    - Issue 3 : détection de victoire (validation : TU)
    - Issue 4 : détection d'égalité (validation : TU)

* * *

- Milestone 2 : interface texte
    - Issue 5 : afficher un jeu (validation : exécution manuelle)
    - Issue 6 : saisir un coup (validation : exécution manuelle)
    - Issue 7 : dérouler une partie de jeu complète (validation : exécution manuelle)

* * *

- Milestone 3 : interface graphique (JavaScript)
    - Issue 8 : intégration du moteur de jeu (validation : manuelle)
    - Issue 9 : implémentation de l'interface graphique (validation : manuelle)
    - Issue 10 : affichage d'un message de status (validation : manuelle)

* * *

Dans l'interface web de gitlab, ajoutez les milestones et issues prévus, puis
réalisez le projet selon la démarche suivante : 

- choisissez l'issue à traiter
- écrivez les code/tests/doc correspondant à l'issue (pas plus, pas moins) 
- effectuez la validation (tests unitaires, tests manuels…)
- commitez, pushez, vérifiez l'intégration continue et fermez l'issue
- si l'issue termine un milestone, ajoutez un tag
- recommencez avec l'issue suivante

