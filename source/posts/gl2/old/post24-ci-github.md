---
title: GL2 TP2 - Intégration continue avec GitHub
date: 2024-01-12
---

L'intégration continue est un ensemble de pratiques et d'outils permettant de
collaborer sur une base de code et d'automatiser des opérations : intégrer des
modifications de code, vérifier la compilation et les tests, packager et
déployer une nouvelle version de l'application...  Ces services sont
généralement disponibles sur les plateformes de gestion de code : GitLab CI/CD,
GitHub Actions...

* * *

> Pour ce TP, vous utiliserez [GitHub](https://github.com).
> 
> - Créez-vous un compte, si vous n'en avez pas déjà un.
> - Au début de chaque exercice, créez le dépôt demandé sur GitHub.
> - A la fin de chaque exercice, pensez à copier le code de votre dépôt GitHub
>   dans votre dépôt `ulco-gl2-etudiant` (sur GitLab).

* * *

Pour créer un token d'accès, allez dans les menus : `Settings / Developper
settings / Personal access tokens`, cliquez sur `Generate new token`,
indiquez les réglages suivant puis générez le token. Sauvegardez ce token
quelque part et utilisez-le quand git vous demandera un login/password.

* * *

![](files/gl2-ci-github-token1.png){width="50%"}


## GitHub Pages : monsupersite

Le système d'intégration continue de GitHub permet de créer une page web très
facilement ([GitHub
Pages](https://docs.github.com/en/pages/quickstart)).

- Créez un dépôt avec le nom `<login>.github.io`

- Clonez, copiez les deux fichiers fournis dans
  `ulco-gl2-etudiant/tp-ci/monsupersite` puis faites un add/commit/push.

* * *

- Vérifiez que le commit apparait sur GitHub et qu'une action est déclenchée.

![](files/gl2-ci-github-monsupersite1.png){width="70%"}

* * *

- Regardez le contenu des deux fichiers puis ajoutez deux pages "intro" et
  "conclu" à votre site, avec des liens entre les pages.

![](files/gl2-ci-github-monsupersite2.png){width="60%"}

![](files/gl2-ci-github-monsupersite3.png){width="60%"}


## GitHub Actions : masuperlib

L'intégration continue de GitHub permet de vérifier la compilation, les tests
unitaires et de générer et déployer la documentation.

- Créez un dépôt GitHub `masuperlib`.

- Clonez votre dépôt, copiez-y les fichiers du projet
  `ulco-gl2-etudiant/tp-ci/masuperlib` (sauf le dossier `.github`) et faites un
  add/commit/push.

- Ajoutez maintenant le fichier `.github/workflows/main.yml`, faites un
  add/commit/push et vérifiez qu'une action est déclenchée.

* * *

- Modifiez le fichier `.github/workflows/main.yml` de façon à compiler le
  projet. Faites un commit/push et regardez les logs de l'action
  correspondante. Corrigez le problème.

![](files/gl2-ci-github-masuperlib1.png){width="70%"}

* * *

- Ajoutez l'exécution des tests. Regardez les logs et corrigez le problème.

![](files/gl2-ci-github-masuperlib2.png){width="70%"}

* * *

- Ajoutez le fichier `.github/workflows/doxygen.yml` et complétez les deux
  actions manquantes pour contruire et déployer la documentation Doxygen.
  Vérifiez sur GitHub que la documentation est bien générée dans une branche
  `gh-pages`.

* * *

- Modifiez les Settings de votre dépôt de façon à lier les Pages à la branche
  `gh-pages` 

![](files/gl2-ci-github-masuperlib3.png){width="70%"}

* * *

- Vérifiez que votre documentation est accessible à l'URL
  `<login>.github.io/masuperlib`. 

![](files/gl2-ci-github-masuperlib4.png){width="80%"}

* * *

- Ajoutez le tag `v0.1` à votre projet et vérifiez qu'il apparait dans les
  releases/tags de GitHub.

```text
$ git tag v0.1

$ git push --tags
Total 0 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
To https://github.com/juliendehos-test/masuperlib.git
 * [new tag]         v0.1 -> v0.1
```

![](files/gl2-ci-github-masuperlib5.png){width="70%"}


## GitHub Actions + Nix : masuperapp

Nix permet de gérer les dépendances logiciels, par exemple pour intégrer une
bibliothèque dans une application, ce qui simplifie l'intégration continue.

- Créez un dépôt `masuperapp`, clonez-le et copiez-y tous les fichiers de
  `ulco-gl2-etudiant/tp-ci/masuperapp`.

* * *

- Regardez le contenu des fichiers puis, dans un terminal, compilez et testez
  (pensez à supprimer le dossier `build` si VSCode l'a déjà créé) :

```text
$ nix-build 
these 2 derivations will be built:
  /nix/store/9dwyj1hm1ikn4rmjb9nclj3yc3n494d3-masuperlib.drv
  /nix/store/lvi2941yicgjm0jkfsixgn59v8vxypbq-masuperpp.drv
...

$ ./result/bin/masuperapp 
v0.1
42
```

* * *

- Modifiez le fichier `default.nix` de façon à utiliser votre `masuperlib` sur
  GitHub (pour cela, utilisez l'URL de l'archive tar.gz du tag v0.1). Vérifiez
  que la compilation (avec `nix-build`) fonctionne.

- Modifiez le fichier `.github/workflows/main.yml` de façon à compiler votre
  code. Faites un add/commit/push et vérifiez les logs sur GitHub.

* * *

- On veut ajouter une page web de documentation, générée à partir du fichier
  `index.md`. Ajoutez un fichier `_config.yml` similaire à celui du premier
  exercice. Faites un add/commit/push.

- Dans un terminal, créez une branche `gh-pages`, passez sur cette branche et
  pushez-la.

* * *

- Vérifiez qu'une action est déclenchée et vérifiez la page web générée.

![](files/gl2-ci-github-masuperapp1.png){width="70%"}


## GitHub Container Registry : monsuperserveur

GitHub permet de générer et stocker des packages. Par exemple, on peut générer
une image Docker automatiquement, pour ensuite la télécharger ou la déployer
sur un serveur.

- Créez un dépôt `monsuperserveur`, clonez-le, copiez-y les fichiers de
`ulco-gl2-etudiant/tp-ci/monsuperserveur` et faites un add/commit/push.

* * *

- Compilez le projet avec un `nix-build`, lancez le programme généré et
  testez-le en allant à l'URL <http://localhost:3000>.

```text
$ nix-build
this derivation will be built:
  /nix/store/w6aw5zr67idxym0mjsq3bw9jmv3vci02-monsuperserveur-1.0.drv
...

$ ./result/bin/monsuperserveur
Setting phasers to stun... (port 3000) (ctrl-c to quit)
```

* * *

- Ajoutez un workflow `.github/workflow/main.yml` pour vérifier automatiquement
  que `nix-build` fonctionne (cf `masuperapp`).  

* * *

- Le projet fourni permet de construire une image Docker contenant le programme
  implémenté. Testez-le avec les commandes suivantes.

```text
$ nix-build docker.nix
these 11 derivations will be built:
...

$ docker load -i result 
4ee3af161ea5: Loading layer [==============>]  1.812MB/1.812MB
...

$ docker images
REPOSITORY        TAG       IMAGE ID       CREATED        SIZE
monsuperserveur   latest    b407362254f4   52 years ago   44MB

$ docker run --rm -it -p 3000:3000 monsuperserveur:latest
Setting phasers to stun... (port 3000) (ctrl-c to quit)

$ docker rmi -f monsuperserveur
```

* * *

- Regardez le workflow `.github/workflows/docker.yml` puis créez une branche
  permettant de le déclencher.

- Vérifiez sur GitHub que le package est bien créé et accessible.

![](files/gl2-ci-github-monsuperserveur2.png){width="80%"}

* * *

- Testez l'image Docker construite par GitHub, avec un `docker run` sur votre
  machine.

- S'il vous reste du temps, modifiez le code de façon à ajouter une route `add`
  qui retourne la somme de deux nombres donnés en paramètre, et vérifiez que
  l'image Docker construite par GitHub est bien mise à jour.


