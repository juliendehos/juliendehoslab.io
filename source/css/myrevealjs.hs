{-# LANGUAGE OverloadedStrings #-}

import Clay
import Data.Text.Lazy.IO (putStr)
import Prelude hiding (putStr, (**), div)

main :: IO ()
main = putStr $ renderWith compact [] $ do

    (p <> li <> td) |> code ? do
        color "#060"
        backgroundColor inherit

    section ? textAlign (alignSide sideLeft)

    ".reveal" ? (h1 <> h2 <> h3) ? textTransform none

    h1 ? textAlign (alignSide sideLeft)

    h2 ? textAlign (alignSide sideLeft)

    h2 |> code ? fontSize (pt 24)

    h3 |> code ? fontSize (pt 20)

    div # ".sourceCode" ? do
        margin (em 0.5) 0 0 0
        maxHeight (px 600)
        overflow visible

    code ? -- do
        backgroundColor "#ffd"
        -- border solid (px 1) black

    ".reveal" ? pre ? code ? do
        fontSize (pt 18)
        lineHeight (pt 20)
        maxHeight (px 600)

    ".reveal" ? pre ? code ? width (px 1000)
    ".reveal" ? pre ? width (px 1000)

    ".reveal" ? fontSize (pt 24)

    ".reveal" ? section ? img ? border 0 none black

    ".reveal" ? ".slide-number" ? do
        color white
        fontSize (pt 20)
        height (px 30)
        left (px 0)
        position absolute
        textAlign center
        verticalAlign middle
        width (px 80)
        zIndex 100

    ".reveal" ? section ? (table <> th <> td) ? do
        border (px 2) solid black
        borderCollapse collapse

    ".reveal" ? blockquote ? do
        border (px 2) solid black
        backgroundColor "#fdd"

    blockquote |> p ? do
        margin (px 0) (px 10) (px 0) (px 10)
        padding (px 0) (px 10) (px 0) (px 10)

