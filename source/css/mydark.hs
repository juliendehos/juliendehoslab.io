{-# LANGUAGE OverloadedStrings #-}

import Clay
import Data.Text.Lazy.IO (putStr)
import Prelude hiding (putStr, div)

main :: IO ()
main = putStr $ renderWith compact [] $ do
-- main = putStr $ renderWith pretty [] $ do
-- margin: top right bottom left

    -- main styles

    hr ? border nil none white

    -- video <> img ? do
    --     display block
    --     marginLeft auto
    --     marginRight auto

    -- section |> p |> img <> p |> img <> figure |> img ? do
    --     backgroundColor white
    --     marginTop (px 10)

    section |> p |> img <> p |> img <> figure |> img ? do
        border (px 1) solid "#ddd"
        backgroundColor white
        marginTop (px 10)

    html ? do
        padding nil (px 5) nil (px 5)
        margin auto auto auto auto
        maxWidth (px 900)
        width (pct 100)
        background ("#222"::Color)
        color white
        fontFamily ["Helvetica", "sans-serif"] [sansSerif]
        fontSize (pt 14)

    ul ? listStyleType none

    li # ".post" ? do
        display flex
        alignItems center

    ul |> li # before ? do
        content (stringContent "-")
        position absolute
        marginLeft (em (-1))

    a ? do
        color "#6b6"
        textDecoration none

    (h1 <> h2 <> h3) ? a ? do
        color "#ddd"
        textDecoration none

    h1 ? fontSize (em 2)
    h2 ? fontSize (em 1.5)
    h3 ? fontSize (em 1.2)

    (h1 <> h2 <> h3) ? 
        marginTop (px 40)

    (td <> h1 <> h2 <> h3) |> a |> img ?
        display inline

    table <> th <> td ? do
        border (px 1) solid "#ddd"
        borderCollapse collapse
        padding (px 5) (px 5) (px 5) (px 5)

    table ?
        marginBottom (px 20)

    "#mytopic" <> "#mysite" ? do
        color "#ddd"
        fontWeight bold
        textDecoration none

    "#mysite" ?
        fontSize (em 1.5)

    "#mydate" ? 
        textAlign (alignSide sideRight)

    -- search box

{-
    "#searchbox" ? do
        overflow hidden
        margin nil nil nil nil
        padding nil nil nil nil
        maxWidth (px 200)
        height (px 40)
        border none nil white
        verticalAlign vAlignTop
-}

    -- header/footer

    table # ".myfloat" ? do 
        border nil none white
        width (pct 100)

    td # ".myfloat_left" <> td # ".myfloat_center" <> td # ".myfloat_right" ? do
        border nil solid white
        padding (px 5) (px 5) nil (px 5)
        margin (px 5) (px 5) nil (px 5)
        width (pct 33)

    header |> (td # ".myfloat_left" <> td # ".myfloat_center" <> td # ".myfloat_right") ?
        width auto

    ".myfloat_left" ? textAlign (alignSide sideLeft)

    ".myfloat_center" ? textAlign (alignSide sideCenter)

    ".myfloat_right" ? textAlign (alignSide sideRight)

    main_ ? do
        borderTop (px 1) solid white
        borderBottom (px 1) solid white

    main_ <> header <> footer ? 
        padding (px 5) (px 5) (px 5) (px 5)

    "#title" ? do
        fontVariant smallCaps
        textAlign center

    img # ".icon" ? do
        border nil solid black
        margin (px 5) (px 5) nil (px 5) 
        width (px 32)

    img # ".icon_inline" ? do
        border nil solid black
        margin (px 5) nil nil (px 10)
        width (px 24)

    -- sidebar

    "#sidebar_container" ? 
        width (pct 100)

    "#sidebar_content" ? do
        verticalAlign vAlignTop
        width (pct 100)

    "#sidebar_toc" ? do
        verticalAlign vAlignTop
        float floatRight
        maxWidth (px 250)
        backgroundColor "#111"
        margin (px 10) (px 10) (px 10) (px 10)
        padding (px 0) (px 10) (px 0) (px 10)
        border (px 1) solid "#ddd"

    "#sidebar_toc" ? ul ? do
        margin (px 10) nil (px 10) nil
        paddingLeft (px 20)

    -- div |> "#sidebar_toc" # before ? do
    --     content (stringContent "Table of contents")
    --     display block

    -- source code

    pre # ".text" <> pre # ".sourceCode" <> pre |> div # ".sourceCode" ? do
        border (px 1) solid "#ddd"
        backgroundColor "#111"
        padding (px 10) (px 10) (px 10) (px 10)
        overflow auto

    code ? 
        fontSize (pt 12)

    (p <> li) ? code ? 
        color "#dd8"
        -- backgroundColor "#111"

    -- block

    blockquote |> p ? margin (px 0) (px 0) (px 0) (px 0)

    blockquote ? do
        display inlineBlock
        margin (px 20) (px 20) (px 20) (px 20)
        padding (px 10) (px 10) (px 10) (px 10)
        color "#ddd"
        backgroundColor "#422"
        border (px 1) solid "#ddd"

