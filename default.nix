{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "juliendehos.gitlab.io";
  src = ./.;

  buildInputs = with pkgs; [

    (haskellPackages.callCabal2nix "app" ./app {})

    (haskellPackages.ghcWithPackages (ps: with ps; [
      clay
      text
    ]))

    gnumake
    graphviz
    gzip
    librsvg
    pandoc
    pdf2svg
    plantuml
    (texlive.combine {
      inherit (texlive)
      scheme-small
      adjustbox
      collectbox
      framed
      pdfcrop
      wasy
      wasysym;
    })

  ];

  installPhase = ''
    mkdir -p $out
    cp -R public $out/
  '';

  shellHook = ''
    export PATH="$PATH:${pkgs.haskellPackages.wai-app-static}/bin"
  '';

}

