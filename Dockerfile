#FROM nixos/nix
FROM nixos/nix:2.15.2

WORKDIR /root
ADD app /root/app
ADD *.nix /root/

RUN nix-shell release.nix --run "echo ok"
RUN rm -rf /root/app /root/*.nix

# docker build -t juliendehos/cicd:mywebpage3 .
# docker push juliendehos/cicd:mywebpage3

